//
//  VotersListViewController.m
//  OutBeat
//
//  Created by ibuildx on 8/10/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//
#import "VotersListTableViewCell.h"
#import "VotersListViewController.h"
#import "Vote.h"
#import "UIImageView+WebCache.h"
#import "Backendless.h"
#import "UserProfileViewController.h"
#import "Friends.h"
#import "Follower.h"
@interface VotersListViewController ()
{

}
@end

@implementation VotersListViewController

- (void)viewDidLoad {
    self.tableView.dataSource=self;
    self.tableView.delegate=self;
    [super viewDidLoad];
    self.lblTitle.text=self.TitleString;
    
    
    [self checkAndFillTable];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)checkAndFillTable {
    
    [self.tableView reloadData];
}


#pragma mark - tableView delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.selectedList.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"votersListCell";
    VotersListTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (cell == nil) {
        cell = [[VotersListTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                              reuseIdentifier:CellIdentifier];
    }
 
    [cell layoutIfNeeded] ;
    
    if ([self.selectedList[indexPath.row] isKindOfClass:[Friends class]]) {
        /*Friends *friend = self.selectedList[indexPath.row] ;
        NSString *userToFindId ;
        if (![friend.SendingUserId isEqualToString:backendless.userService.currentUser.objectId]) {
            userToFindId = friend.SendingUserId ;
        }
        else {
            userToFindId = friend.FriendUserId ;
        }
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore findID:userToFindId response:^(id user) {
            cell.userName.text=[user valueForKey:@"name"];
            [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:[user getProperty:@"profileImage"]]
                                     placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                              options:SDWebImageRefreshCached];
            [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
            cell.userProfileImage.clipsToBounds=YES;
            
        } error:^(Fault *error) {
            NSLog(@"Error at user fetch %@", error.detail) ;
        }] ;
        */
        NSLog(@"SHOULD BE RUNNING THIS CONDITION") ;

    }
    else if ([self.selectedList[indexPath.row] isKindOfClass:[BackendlessUser class]]) {
        BackendlessUser *user = self.selectedList[indexPath.row] ;
        cell.userName.text=[user valueForKey:@"name"];
        [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:[user getProperty:@"profileImage"]]
                                 placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                          options:SDWebImageRefreshCached];
        [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
        cell.userProfileImage.clipsToBounds=YES;
        [cell.friendOrNotBtn setHidden:YES] ;
    }
    else if ([self.selectedList[indexPath.row] isKindOfClass:[Vote class]])
    {
        
    }
    
    
    
    
    
    [cell.friendOrNotBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    self.selectedVoter =self.selectedList[indexPath.row];
    [self performSegueWithIdentifier:@"voterListToProfile" sender:self];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"voterListToProfile"])
    {
        // Get reference to the destination view controller
        UserProfileViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        vc.selectedUser = self.selectedVoter ;
    }
}
-(void)yourButtonClicked:(UIButton*)sender
{
    
}
- (IBAction)backAction:(UIButton *)sender {
    
    /*
    if ([self.delegate respondsToSelector:@selector(fromProfileViewController:)])
    {
        [self performSegueWithIdentifier:@"userListToProfile" sender:self];
    }
    else  if ([self.delegate respondsToSelector:@selector(fromChallengersViewController:)])
    {
        [self performSegueWithIdentifier:@"listTosharingAndCommentsBack" sender:self];
    }
    else{
    [self performSegueWithIdentifier:@"votersListToVotesBack" sender:self];
    }
     */
    [self performSegueWithIdentifier:@"backToProfile" sender:self];

}
- (IBAction)unwindToUserList:(UIStoryboardSegue*)sender
{
}
@end
