//
//  FriendRequests.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/3/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "FriendRequests.h"
#import "Backendless.h"
//#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "RequestCellTableViewCell.h"
#import "UIImageView+WebCache.h"
#import "Friends.h"
#import "ProfileViewController.h"
#import "AMSmoothAlertView.h"
#import "Utility.h"


@interface FriendRequests (){
    
    
    
    
}

@end

@implementation FriendRequests

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [self fetchUser];
   
    self.tableView.delegate = self ;
    self.tableView.dataSource = self ;
    
    self.FriendsUsersArray = [[NSMutableArray alloc] init];
    
 }
-(void)fetchUser{
    
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"FriendUserId = \'%@\' AND RequestStatus = 'NotAccepted'",backendless.userService.currentUser.objectId];
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[Friends class]];
    [dataStore find:query response:^(BackendlessCollection *allRequests) {
        
        self.FriendsRequestArray =[[NSMutableArray alloc]initWithArray:allRequests.data];
        NSLog(@"%lu",(unsigned long)self.FriendsRequestArray.count);
        [self.tableView reloadData];

    } error:^(Fault *error) {
        NSLog(@"error at requsts fetch %@" , error.detail);
    }];
    

    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.FriendsRequestArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"requestsCell";
    RequestCellTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[RequestCellTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:CellIdentifier];
    }
    
    
    Friends *friend = self.FriendsRequestArray[indexPath.row] ;
    
    
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore findID:friend.SendingUserId response:^(id sendingUser) {
        [self.FriendsUsersArray addObject:sendingUser] ;
        
        cell.userName.text=[sendingUser getProperty:@"name"];
        [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:[sendingUser getProperty:@"profileImage"]]
                             placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                      options:SDWebImageRefreshCached];
        
    } error:^(Fault *error) {
        NSLog(@"error at requsts User fetch %@" , error.detail);
    }];
    [cell.profileImage.layer setCornerRadius:cell.profileImage.frame.size.height/2];
    cell.profileImage.clipsToBounds=YES;
    cell.btnoutlet.tag=indexPath.row;
    
    [cell.btnoutlet addTarget:self action:@selector(yourConfirmClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.ignoreBtn addTarget:self action:@selector(ignoreClicked:) forControlEvents:UIControlEventTouchUpInside];

    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
-(void)ignoreClicked:(UIButton*)sender {
    [SVProgressHUD showWithStatus:@"Responding..."];
    Friends *friend = [_FriendsRequestArray objectAtIndex:sender.tag];
    BackendlessUser *selectedUser = self.FriendsUsersArray[sender.tag] ;
    id<IDataStore> dataStore = [backendless.persistenceService of:[Friends class]];
    [dataStore remove:friend response:^(NSNumber *num) {
        [SVProgressHUD dismiss];
        [self.FriendsUsersArray removeObject:selectedUser] ;
        [_FriendsRequestArray removeObject:friend];
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Friend Request Ignored." andCancelButton:NO forAlertType:AlertSuccess];
        [alert show];
        
        [self.tableView reloadData] ;
        
        

    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}


-(void)yourConfirmClicked:(UIButton*)sender
{
    [SVProgressHUD showWithStatus:@"Accepting Request..."];
    Friends *friend = [_FriendsRequestArray objectAtIndex:sender.tag];
    BackendlessUser *currentUser = backendless.userService.currentUser ;
    friend.RequestStatus = @"Accepted" ;
    
    id<IDataStore> dataStoreFriend = [backendless.persistenceService of:[Friends class]];

    
    [dataStoreFriend save:friend response:^(id savedFriend) {
        NSMutableArray *newFriendsList = [[NSMutableArray alloc] initWithArray:[currentUser getProperty:@"friends"]] ;
        [newFriendsList addObject:friend] ;
        [currentUser setProperty:@"friends" object:newFriendsList] ;
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore save:currentUser response:^(id savedUser) {
            [SVProgressHUD dismiss];
            
            
            
            BackendlessUser *selectedUser = self.FriendsUsersArray[sender.tag] ;
            
            backendless.userService.currentUser = savedUser ;
            NSString *friendRequestMessage = [NSString stringWithFormat:@"%@ has Accepted Your Friend's Request..", backendless.userService.currentUser.name];
            [Utility publishMessageAsPushNotificationAsync:friendRequestMessage forDevice:[selectedUser getProperty:@"deviceId"]] ;
            [self.FriendsUsersArray removeObject:selectedUser] ;
            [_FriendsRequestArray removeObject:friend];
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"Friend Request Accepted." andCancelButton:NO forAlertType:AlertSuccess];
            [alert show];
            
            [self.tableView reloadData] ;
            
        } error:^(Fault *error) {
            [SVProgressHUD dismiss];
            NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }] ;
    } error:^(Fault *error) {
        NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }] ;
    
    
    
    
    //[self updateFriends:friend];
}



-(void)updateFriends:(Friends *)friend
{
    /*
    NSMutableArray *addUser=[[NSMutableArray alloc]init];
    BackendlessUser *friendUser =backendless.userService.currentUser;
    addUser=[friendUser getProperty:@"friends"];
    [addUser addObject:friend.sendingUser];
    
    //2ndlist
    NSMutableArray *addUser1=[[NSMutableArray alloc]init];
    BackendlessUser *friendUser1 =friend.sendingUser;
    addUser1=[friendUser1 getProperty:@"friends"];
    [addUser1 addObject:friend.receivingUser];
    
    
    friend.status=true;
    
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[Friends class]];
    [dataStore save:friend response:^(id status) {
       
        
        id<IDataStore> dataStore1 = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore1 save:friendUser response:^(id saveFriendInUserList) {
            
            id<IDataStore> dataStore2 = [backendless.persistenceService of:[BackendlessUser class]];
            [dataStore2 save:friendUser1 response:^(id saveFriendInUserList1) {
                
                NSLog(@"friends saved.");
                
            } error:^(Fault *error) {
                NSLog(@"error at savin friend in userlist");
            }];

            
        } error:^(Fault *error) {
            NSLog(@"error at savin friend in userlist");
        }];
        
        [self fetchUser];
        [self addFriendToList];
        
        //code
    } error:^(Fault *error) {
        //code
        NSLog(@"Error at Friend request accepted %@" ,error.detail);
        
    }];
     */

}
-(void)addFriendToList{
   
    
    /*
    friendArray =[[NSMutableArray alloc]initWithArray:[backendless.userService.currentUser getProperty:@"friends"]] ;
    
    

    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"receivingUser.objectId = \'%@\' AND status =true",backendless.userService.currentUser.objectId];
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[Friends class]];
    [dataStore find:query response:^(BackendlessCollection *allRequests) {
        
        friendRequestArray =[[NSMutableArray alloc]initWithArray:allRequests.data];
       
        for (int i=0; i<friendRequestArray.count; i++) {
            
            BackendlessUser *friendUser =[backendless.userService.currentUser getProperty:@"friends"];
            friendUser = [friendRequestArray[i] valueForKey:@"sendingUser"];
            
            id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
            [dataStore save:friendUser response:^(id any) {
                
            } error:^(Fault *error) {
                NSLog(@"error at saving");
            }];
            
        }
     
        
            
            
        
     } error:^(Fault *error) {
        NSLog(@"error at ");
    }];
*/
    



}
- (IBAction)backAction:(id)sender {
    [self performSegueWithIdentifier:@"friendRequestToProfile" sender:self];
}
@end
