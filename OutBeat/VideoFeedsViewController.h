//
//  VideoFeedsViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/17/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Battle.h"
#import "Challenge.h"

@interface VideoFeedsViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,UIGestureRecognizerDelegate>
@property (weak, nonatomic) IBOutlet UITableView *battleVideosTableView;
@property (strong) NSMutableArray  *vidsFound;
@property (strong) NSMutableArray  *vidsFoundOriginal;
@property (strong) NSMutableArray  *challengeVidsFound;
@property (strong) NSMutableArray  *challengeVidsFound_2;
@property (strong) NSMutableArray  *challengeVidsFoundOriginal;
@property (strong) Battle  *selectedBattleAtVideoFeed;
@property (strong) Challenge  *selectedChallenge;
@property (strong) NSMutableArray  *videosToShow;
@property (strong) NSMutableArray *friendsArray;
@property (strong) NSMutableArray *followersArray;
@property (strong) NSMutableArray  *searchFilters;
@property (weak, nonatomic) IBOutlet UIButton *searchOkBtn;
@property (strong) NSMutableArray *ChellengeVideosArray;


@property (weak, nonatomic) IBOutlet UIButton *searchBtn;


@property (weak, nonatomic) IBOutlet UIView *tabbarShadowView;

- (IBAction)videoSwitch:(UISwitch *)sender;
@property (weak, nonatomic) IBOutlet UISwitch *videoSwitchBtn;

@property (weak, nonatomic) IBOutlet UIView *swipeDownView;


- (IBAction)searchAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITextField *txtSearch;
- (IBAction)searchBackAction:(UIButton *)sender;
- (IBAction)searchOkAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIView *dropDownView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpace;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableVerticalSpace;

@property (strong, nonatomic) IBOutlet UISwipeGestureRecognizer *swipeGestureRecognizer;
@property (weak, nonatomic) IBOutlet UILabel *lblNoResultsfound;
- (IBAction)menuAction:(id)sender;


@end
