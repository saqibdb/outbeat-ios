//
//  FriendsCell.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/2/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *UserName;
@property (weak, nonatomic) IBOutlet UIButton *addFriendAction;
- (IBAction)addFriend:(id)sender;
- (IBAction)followAction:(id)sender;

@end
