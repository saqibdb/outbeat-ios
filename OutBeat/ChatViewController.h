//
//  ChatViewController.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 7/20/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Inbox.h"
#import "Backendless.h"

@interface ChatViewController : UIViewController<UITableViewDelegate , UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UITableView *inboxTableView;


- (IBAction)backAction:(UIButton *)sender;

@property (strong) Inbox  *selectedInbox;
@property (strong) BackendlessUser  *otherUser;

@property (nonatomic, assign) BOOL comingFromProfile;

@property (weak, nonatomic) IBOutlet UIButton *mailBtn;

@property (weak, nonatomic) IBOutlet UIView *textViewRounded;

@property (weak, nonatomic) IBOutlet UITextField *textBoxInput;



@property (weak, nonatomic) IBOutlet NSLayoutConstraint *msgViewBottomConstraint;



@property (strong) NSMutableArray  *allMessages;


- (IBAction)mailAction:(id)sender;



@end
