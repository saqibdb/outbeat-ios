//
//  SharingAndCommentsViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/15/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//
#import "SearchPopUp.h"
#import "TNRadioButtonGroup.h"
#import "SharingAndCommentsViewController.h"
#import <Social/Social.h>
#import "AMSmoothAlertView.h"
#import "Favourite.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Video.h"
#import "commentsTableViewCell.h"
#import "IQKeyboardManager.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "Utility.h"
#import "VideoPlayViewController.h"
#import "VotersListViewController.h"
#import "UIViewController+CWPopup.h"
#import "Comments.h"
#import "UploadVideoViewController.h"
#import "IQKeyboardReturnKeyHandler.h"
#import "PDKPin.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "EmailPopUp.h"
#import "SendGrid.h"
#import "SendGridEmail.h"
#import "UserProfileViewController.h"

@interface SharingAndCommentsViewController (){

SearchPopUp *callUsPopUpView ;
    NSInteger indexToRemove ;
    NSString *report;
    IQKeyboardReturnKeyHandler *returnKeyHandler;
    Video *selectedVideo ;
    EmailPopUp *emailPopUpView ;

}

@end

@implementation SharingAndCommentsViewController
- (void)fromProfileViewController:(NSObject*)something {
    
}
- (void)fromChallengersViewController:(NSObject*)something {
    
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [self profileImagesUi];
    self.commentsTableView.dataSource=self;
    self.commentsTableView.delegate=self;
    [self.txtCommentField addDoneOnKeyboardWithTarget:self action:@selector(commentSave:) shouldShowPlaceholder:YES];
    
    NSString *viewers =[NSString stringWithFormat:NSLocalizedString(@"viewers", nil)];
    _lblViewers.text=[NSString stringWithFormat:@"%@ %@",self.selectedChallenge.Views,viewers];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.keyboardViewBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    self.keyboardViewBottomConstraint.constant = height;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}
-(void)profileImagesUi{

    [_challenger_1.layer setCornerRadius:_challenger_1.frame.size.height/2];
    _challenger_1.clipsToBounds=YES;
    _challenger_1.layer.borderWidth=1.5;
    _challenger_1.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_challenger_2.layer setCornerRadius:_challenger_2.frame.size.height/2];
    _challenger_2.clipsToBounds=YES;
    _challenger_2.layer.borderWidth=1.5;
    _challenger_2.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_challenger_3.layer setCornerRadius:_challenger_3.frame.size.height/2];
    _challenger_3.clipsToBounds=YES;
    _challenger_3.layer.borderWidth=1.5;
    _challenger_3.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_challenger_4.layer setCornerRadius:_challenger_4.frame.size.height/2];
    _challenger_4.clipsToBounds=YES;
    _challenger_4.layer.borderWidth=1.5;
    _challenger_4.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_challenger_5.layer setCornerRadius:_challenger_5.frame.size.height/2];
    _challenger_5.clipsToBounds=YES;
    _challenger_5.layer.borderWidth=1.5;
    _challenger_5.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_challenger_6.layer setCornerRadius:_challenger_6.frame.size.height/2];
    _challenger_6.clipsToBounds=YES;
    _challenger_6.layer.borderWidth=1.5;
    _challenger_6.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    //[_plusChallengersBtn.layer setCornerRadius:_plusChallengersBtn.frame.size.height/2];
    //_plusChallengersBtn.clipsToBounds=YES;

    
    [_plusUserBtn.layer setCornerRadius:_plusUserBtn.frame.size.height/2];
    _plusUserBtn.clipsToBounds=YES;
    _plusUserBtn.hidden = YES ;

    
    //setting main chllenge views
   [self.tumbnailImage sd_setImageWithURL:[NSURL URLWithString:self.selectedChallenge.MainVideo.ThumbnailURL]];
    
    
    
    [self.mainUserProfile sd_setImageWithURL:[NSURL URLWithString:[self.selectedChallenge.MainVideo.User getProperty:@"profileImage"]]];
    
    
    
    self.mainUserName.text=self.selectedChallenge.MainVideo.User.name;
    self.mainChallengeType.text=self.selectedChallenge.MainVideo.Type;
    [self.categoryBtn setTitle:self.selectedChallenge.MainVideo.Type forState:UIControlStateNormal] ;
    
   [self.videoTypeIcon setImage:[UIImage imageNamed: @"challenge-big"]];
    
    [_mainUserProfile.layer setCornerRadius:_mainUserProfile.frame.size.height/2];
    _mainUserProfile.clipsToBounds=YES;
    _mainUserProfile.layer.borderWidth=1.5;
    _mainUserProfile.layer.borderColor=[[UIColor orangeColor] CGColor];
    _mainUserProfile.contentMode = UIViewContentModeScaleToFill;

    
}
-(void)viewWillAppear:(BOOL)animated{

    self.allChellengersImages = [[NSMutableArray alloc] initWithObjects:_thumbnail_1,
                          
                                 _thumbnail_2,
                                 _thumbnail_3,
                                 _thumbnail_4,
                                 _thumbnail_5,nil] ;
    self.AllViews = [[NSMutableArray alloc] initWithObjects:_view_1,
                                 _view_2,
                                 _view_3,
                                 _view_4,
                                 _view_5,nil] ;
        self.AllUsersImagesArray = [[NSMutableArray alloc] initWithObjects:_challenger_1,
                                    _challenger_2,
                                    _challenger_3,
                                    _challenger_4,
                                    _challenger_5,
                                    _challenger_6,nil] ;
    
    for (UIView *view in _AllUsersImagesArray) {
        view.hidden = YES ;
    }
                                    
            //[self fetchingChellengeVideos];
            //[self fetchChallengerUsers];
    
    
    
    self.challengeTitleLabel.text = self.selectedChallenge.MainVideo.Title ;
    
    self.challengeNameText.text = self.selectedChallenge.MainVideo.Title ;
   
    [self updateChellengers];
    
    [self updateChallengerUsers];
    
    
    [self sortByDate:_selectedChallenge.Comments completion:^(NSArray *arraySorted) {
        NSArray* reversedArray = [[arraySorted reverseObjectEnumerator] allObjects];
        _selectedChallenge.Comments = [reversedArray mutableCopy] ;
        [self.commentsTableView reloadData];
        
    }];
    
    


}

-(void)didReceiveMemoryWarning {

    [super didReceiveMemoryWarning];
    
}
#pragma mark - Challengers Profile Related

-(void)fetchChallengerUsers{
    
    
    [[backendless.persistenceService of:[BackendlessUser class]] find:nil response:^(BackendlessCollection *userColection) {
        _usersArray = [[NSMutableArray alloc] initWithArray:userColection.data];
        self.chllengersList=[[NSArray alloc]initWithArray:_usersArray];
        NSLog(@"%lu",(unsigned long)self.chllengersList.count);
        self.circulerRatingView.value = _usersArray.count;
        
        NSString *challengers = [NSString stringWithFormat:NSLocalizedString(@"Challengers", nil)];
        self.lblChellengersCount.text=[NSString stringWithFormat:@"%lu %@" ,(unsigned long)self.usersArray.count, challengers];
        } error:^(Fault *error) {
            NSLog(@"error at getting users");
        }];


}
-(void)updateChallengerUsers{
    
    [self.view layoutIfNeeded];
    NSString *challengers = [NSString stringWithFormat:NSLocalizedString(@"Challengers", nil)];
    self.lblChellengersCount.text=[NSString stringWithFormat:@"%lu %@" ,(unsigned long)self.selectedChallenge.OtherVideos.count, challengers];


    
    if (self.selectedChallenge.OtherVideos.count > 6) {
        _plusUserBtn.hidden = NO ;
        int plusFriends = (int)self.selectedChallenge.OtherVideos.count - 6;
        NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;

        
        [_plusUserBtn setTitle:btnTitle forState:UIControlStateNormal] ;
    }
    else{
        _plusUserBtn.hidden = YES ;
        _plusChallengersBtn.hidden = YES ;
    }
    
    for (int i = 0 ; i <self.selectedChallenge.OtherVideos.count ; i++) {
        if (i < 6) {
            
            UIImageView *circleImageView = _AllUsersImagesArray[i];
            [circleImageView setHidden:NO] ;
            
            Video *vid = self.selectedChallenge.OtherVideos[i] ;
            
            //[circleImageView setHighlighted:NO] ;
            
            [circleImageView sd_setImageWithURL:[NSURL URLWithString:[vid.User valueForKey:@"profileImage"]]
                               placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                        options:SDWebImageRefreshCached];
            [_plusChallengersBtn setHidden:NO];
            [_plusChallengersBtn setTitle:@"+0" forState:UIControlStateNormal] ;
            [_view_12 setHidden:NO] ;
            
        }
        else{
            int plusFriends = i - 5;
            NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;
            [_plusChallengersBtn setTitle:btnTitle forState:UIControlStateNormal] ;
        }

    }


}
#pragma mark - Challengers videos Related
-(void)fetchingChellengeVideos{

 _ChellengeVideosArray2=[[NSMutableArray alloc] init];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"Category='Challenge'"];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Video class]];
    [dataStore find:query response:^(BackendlessCollection *ChellengeVideoCollection) {
       
       
        self.ChellengeVideosArray =[[NSMutableArray alloc]initWithArray:ChellengeVideoCollection.data];
        for (Video *vid in _ChellengeVideosArray){
        
            [_ChellengeVideosArray2 addObject:vid];
            NSLog(@"%@",vid.Category);
        
        }
        //[self updateChellengers];
        
    } error:^(Fault *error) {
        
        
    }];
}
-(void)updateChellengers{
    
    for (int i = 0 ; i <self.selectedChallenge.OtherVideos.count ; i++) {
        if (i < 5) {
            
            UIImageView *circleImageView = _allChellengersImages[i];
            UIView *view= _AllViews[i];
            [view setHidden:NO];
            [circleImageView setHidden:NO] ;
            
            
            Video *vid = self.selectedChallenge.OtherVideos[i] ;
            
            
            [circleImageView sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL] placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"] options:SDWebImageRefreshCached];
            
            [_plusChallengersBtn setHidden:NO];
            [_plusChallengersBtn setTitle:@"+0" forState:UIControlStateNormal] ;
            
        }
        else{
            int plusFriends = i - 5;
            NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;
            [_plusChallengersBtn setTitle:btnTitle forState:UIControlStateNormal] ;
        }
    }
    
    self.circulerRatingView.value = self.selectedChallenge.OtherVideos.count ;
}

-(void)sortByDate :(NSArray *)comments completion:(void (^)(NSArray *arraySorted))completion __attribute__((nonnull(2)));
{
    NSMutableArray *commentsArray = [[NSMutableArray alloc] initWithArray:comments];
    
    NSArray *sortedLocations = [commentsArray sortedArrayUsingComparator: ^(Comments *a1, Comments *a2) {

        return [a1.created compare:a2.created];
    }];
    completion(sortedLocations);
}


- (void)commentSave:(id)sender {

    NSLog(@"Comment Passed = %@" , self.txtCommentField.text) ;
    if (self.txtCommentField.text.length) {
        [SVProgressHUD showWithStatus:@"Saving Comment ..."];
        
        Comments *comment = [[Comments alloc] init];
        comment.Message = self.txtCommentField.text ;
        comment.User =  backendless.userService.currentUser ;
        comment.created = [NSDate date] ;
        
        
        [_selectedChallenge.Comments addObject:comment] ;
        
        
        [self sortByDate:_selectedChallenge.Comments completion:^(NSArray *arraySorted) {
            
            NSArray* reversedArray = [[arraySorted reverseObjectEnumerator] allObjects];
            
            [SVProgressHUD dismiss];
            _selectedChallenge.Comments = [reversedArray mutableCopy] ;
            [self.commentsTableView reloadData];
            [self.txtCommentField setText:@""];
            [self.txtCommentField resignFirstResponder] ;
            
            [self.commentsTableView scrollsToTop];
            
        }];
        
            id<IDataStore> dataStoreBattle = [backendless.persistenceService of:[Challenge class]];
            [dataStoreBattle save:_selectedChallenge response:^(id savedBattle) {
                NSLog(@"Comment Saved");
            } error:^(Fault *errorBattle) {
                [SVProgressHUD dismiss];
                NSLog(@"Error At Saving Challenge = %@" , errorBattle.detail) ;
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",errorBattle.detail] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [_selectedChallenge.Comments removeObject:comment] ;
                [self.commentsTableView reloadData] ;
            }];
        
    }
    else{
        [self.txtCommentField resignFirstResponder] ;
    }

    
    


}


#pragma mark - tableView delegates
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.selectedChallenge.Comments.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.commentsTableView layoutIfNeeded];
    static NSString *CellIdentifier = @"commentCell";
    commentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[commentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:CellIdentifier];
    }
    
    Comments *comment = [self.selectedChallenge.Comments objectAtIndex:indexPath.row] ;
    
    
    
    [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:[comment.User getProperty:@"profileImage"] ]];
    
    cell.lblcomment.text=comment.Message;
    cell.userName.text=comment.User.name;
    [cell.profileImage.layer setCornerRadius:cell.profileImage.frame.size.height/2];
    cell.profileImage.clipsToBounds=YES;
    cell.profileImage.layer.borderWidth=1.5;
    cell.profileImage.layer.borderColor=[[UIColor orangeColor] CGColor];
     NSString *commentDateString = [Utility getTimeFormatForDate:comment.created] ;
    indexToRemove =indexPath.row;
    cell.commentTime.text=[NSString stringWithFormat:@"%@",commentDateString];
    cell.rightUtilityButtons = [self rightButtonsAndComment:comment];
    cell.delegate = self;
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (NSArray *)rightButtonsAndComment:(Comments*)comment
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:@"Report"];
    
    if ([comment.User.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                    title:@"Delete"];
    }
    
    
    return rightUtilityButtons;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            [self openReportPopUp ];
            break;
        case 1:
        {
            Comments *comment = self.selectedChallenge.Comments [indexToRemove] ;
            NSLog(@"Removing Comment is ==%@" ,comment.Message);

            
            id<IDataStore> dataStore = [backendless.persistenceService of:[Comments class]];
            [dataStore remove:comment response:^(NSNumber *number) {
                
                NSLog(@"comment removed");
                [self.selectedChallenge.Comments removeObject:comment] ;
                [self.commentsTableView reloadData];

                
            } error:^(Fault *error) {
                NSLog(@"error at comment removed ==%@" ,error.detail);
            }];

           
            break;
        }
        default:
            break;
    }
}
-(void)openReportPopUp{
    //asdfsadfadsfsadfef
    
    callUsPopUpView = [[SearchPopUp alloc] initWithNibName:@"SearchPopUp" bundle:nil];
    
    callUsPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width- 50, self.view.frame.size.height * 0.5 );
    
    
    TNImageRadioButtonData *one = [TNImageRadioButtonData new];
    one.labelText = @"its annoying or not interesting";
    one.identifier = @"its annoying or not interesting";
    one.selected = NO;
    one.unselectedImage = [UIImage imageNamed:@"unCheked"];
    one.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *two = [TNImageRadioButtonData new];
    two.labelText = @"i think it should'nt be on outBeat";
    two.identifier = @"i think it should'nt be on outBeat";
    two.selected = NO;
    two.unselectedImage = [UIImage imageNamed:@"unCheked"];
    two.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *three = [TNImageRadioButtonData new];
    three.labelText = @"its spam";
    three.identifier = @"its spam";
    three.selected = NO;
    three.unselectedImage = [UIImage imageNamed:@"unCheked"];
    three.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *four = [TNImageRadioButtonData new];
    four.labelText = @"its rude , vulger or uses bad language";
    four.identifier = @"its rude , vulger or uses bad language";
    four.selected = NO;
    four.unselectedImage = [UIImage imageNamed:@"unCheked"];
    four.selectedImage = [UIImage imageNamed:@"cheked"];
    
    
    
    
    callUsPopUpView.radioButtonGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[one, two,three,four] layout:TNRadioButtonGroupLayoutVertical];
    callUsPopUpView.radioButtonGroup.identifier = @"Temperature group";
    
    [callUsPopUpView.radioButtonGroup create];
    //callUsPopUpView.radioButtonGroup.position = CGPointMake(25, 400);
    [ callUsPopUpView.outerView addSubview : callUsPopUpView.radioButtonGroup];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(temperatureGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:callUsPopUpView.radioButtonGroup];
    
    
    
    [self presentPopupViewController:callUsPopUpView animated:YES completion:^(void) {
        [callUsPopUpView.cancelBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
         [callUsPopUpView.sendBtn addTarget:self action:@selector(sendMail) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    
    
}
-(void)sendMail{
    
    
    Comments *comment = self.selectedChallenge.Comments [indexToRemove] ;

    NSString *subject = @"Report from outBeat";
    NSString *body =[NSString stringWithFormat:@" hy ! we received a report (reason:%@) against your commnet on Battle '%@' and your comment was '%@' .Please remove it ASAP",report,self.selectedChallenge.objectId, comment.Message];
    NSString *recipient = @"asadjavid46@gmail.com";
    NSLog(@"%@",recipient);
    [backendless.messagingService
     sendHTMLEmail:subject body:body to:@[recipient]
     response:^(id result) {
         NSLog(@"ASYNC: HTML email has been sent");
         [self dismissPopup];
     }
     error:^(Fault *fault) {
         NSLog(@"Server reported an error: %@", fault);
     }];
}

- (void)temperatureGroupUpdated:(NSNotification *)notification {
    NSLog(@"selected reason = %@", callUsPopUpView.radioButtonGroup.selectedRadioButton.data.identifier);
    report=callUsPopUpView.radioButtonGroup.selectedRadioButton.data.identifier;
}
- (void)createHorizontalListWithImage {
    
}

- (void)dismissPopup {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            NSLog(@"popup view dismissed");
        }];
    }
}


#pragma mark -actions
- (IBAction)playAction:(id)sender {
    selectedVideo = self.selectedChallenge.MainVideo ;
    [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"challengeToVideoPlay"]) {
        
        
        VideoPlayViewController *vc = segue.destinationViewController;
        
        vc.selectedVideo=selectedVideo;
        vc.previousController = @"SharingAndComment" ;

    }
    if ([segue.identifier isEqualToString:@"challengeToProfilea"]) {
        
        
        VotersListViewController *vc = segue.destinationViewController;
        
        vc.votersList =self.chllengersList;
        NSLog(@"%lu",(unsigned long)self.chllengersList.count);
        //vc.delegate=self;
        vc.TitleString=@"Challengers";
        
    }
    if ([segue.identifier isEqualToString:@"challengeToMakeBattle"]) {
        UploadVideoViewController *uploadDestination = segue.destinationViewController ;
        uploadDestination.isForChallenge = YES ;
        uploadDestination.isForBattle = NO ;
        
        uploadDestination.videoForBattle = self.selectedChallenge ;
        uploadDestination.comingFromVC = @"SharingAndComment" ;
    }
    if ([segue.identifier isEqualToString:@"sharingToUserProfile"]) {
        UserProfileViewController *vc = segue.destinationViewController ;
        vc.selectedUser = self.selectedChallenge.MainVideo.User;
        vc.previousController = @"SharingAndCommentsViewController";
    }
    
    


    
}

- (IBAction)plusVideosAction:(UIButton *)sender {
}

- (IBAction)playVideoAction_10:(id)sender {
    
    if (self.selectedChallenge.OtherVideos[9]) {
        selectedVideo = self.selectedChallenge.OtherVideos[9] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }}

- (IBAction)playVideoAction_11:(id)sender {
    if (self.selectedChallenge.OtherVideos[10]) {
        selectedVideo = self.selectedChallenge.OtherVideos[10] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}

- (IBAction)playVideoAction_1:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[0]) {
        selectedVideo = self.selectedChallenge.OtherVideos[0] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
    
}

- (IBAction)playVideoAction_2:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[1]) {
        selectedVideo = self.selectedChallenge.OtherVideos[1] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}

- (IBAction)playVideoAction_3:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[2]) {
        selectedVideo = self.selectedChallenge.OtherVideos[2] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}

- (IBAction)playVideoAction_4:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[3]) {
        selectedVideo = self.selectedChallenge.OtherVideos[3] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}

- (IBAction)playVideoAction_5:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[4]) {
        selectedVideo = self.selectedChallenge.OtherVideos[4] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}

- (IBAction)playVideoAction_6:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[5]) {
        selectedVideo = self.selectedChallenge.OtherVideos[5] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}

- (IBAction)playVideoAction_7:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[6]) {
        selectedVideo = self.selectedChallenge.OtherVideos[6] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}
- (IBAction)playVideoAction_8:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[7]) {
        selectedVideo = self.selectedChallenge.OtherVideos[7] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}

- (IBAction)playVideoAction_9:(UIButton *)sender {
    if (self.selectedChallenge.OtherVideos[8]) {
        selectedVideo = self.selectedChallenge.OtherVideos[8] ;
        [self performSegueWithIdentifier:@"challengeToVideoPlay" sender:self];
    }
}
#pragma mark - socialmedia sharing
- (IBAction)shareFbAction:(id)sender {
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:self.selectedChallenge.MainVideo.VideoURL];
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil] ;
}

- (IBAction)shareTwitterAction:(id)sender {
   
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        SLComposeViewController *twitterController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [twitterController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        
        [twitterController setInitialText:@"Check out the OutBeat from Appstore Today!"];
        [twitterController addURL:[NSURL URLWithString:self.selectedChallenge.MainVideo.VideoURL]];
        [twitterController setCompletionHandler:completionHandler];
        [self presentViewController:twitterController animated:YES completion:nil];
    }
    
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Cannot Share" andText:[NSString stringWithFormat:@"You need Twitter app installed and have to be logged in to it to be able to Share to Twitter."] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];

        NSLog(@"you are not logged in ");
    }
}

- (IBAction)shareG:(id)sender {
    // Construct the Google+ share URL
    NSURLComponents* urlComponents = [[NSURLComponents alloc]
                                      initWithString:@"https://plus.google.com/share"];
    urlComponents.queryItems = @[[[NSURLQueryItem alloc]
                                  initWithName:@"url"
                                  value:[[NSURL URLWithString:self.selectedChallenge.MainVideo.VideoURL] absoluteString]]];
    NSURL* url = [urlComponents URL];
    
    if ([SFSafariViewController class]) {
        // Open the URL in SFSafariViewController (iOS 9+)
        SFSafariViewController* controller = [[SFSafariViewController alloc]
                                              initWithURL:url];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Open the URL in the device's browser
        [[UIApplication sharedApplication] openURL:url];
    }
    
}

- (IBAction)sharePintrestAction:(id)sender {
    [PDKPin pinWithImageURL:[NSURL URLWithString:@"https://about.pinterest.com/sites/about/files/logo.jpg"]
                       link:[NSURL URLWithString:self.selectedChallenge.MainVideo.VideoURL]
         suggestedBoardName:@"OutBeat"
                       note:@"Check out the OutBeat from Appstore Today!"
         fromViewController:self
                withSuccess:^
     {
         //weakSelf.resultLabel.text = [NSString stringWithFormat:@"successfully pinned pin"];
     }
                 andFailure:^(NSError *error)
     {
         //weakSelf.resultLabel.text = @"pin it failed";
     }];
    
    }

- (IBAction)shareMailAction:(id)sender {
    emailPopUpView = [[EmailPopUp alloc] initWithNibName:@"EmailPopUp" bundle:nil];
    
    emailPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width- 50, self.view.frame.size.height * 0.4 );
    
    
    [self presentPopupViewController:emailPopUpView animated:YES completion:^(void) {
        
        [emailPopUpView.shareBtn addTarget:self action:@selector(dismissPopupWithEmail) forControlEvents:UIControlEventTouchUpInside];
        [emailPopUpView.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        
    }];
    
    }
- (void)dismissPopupWithEmail {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            SendGrid *sendgrid = [SendGrid apiUser:@"ibuildx" apiKey:@"umer7447"];
            SendGridEmail *email = [[SendGridEmail alloc] init];
            email.to = emailPopUpView.phoneTxt.text;
            email.from = @"info@ibuildx.com";
            email.subject = @"OutBeat";
            email.text = [NSString stringWithFormat:@"Checkout the OutBeat from Appstore. %@" , self.selectedChallenge.MainVideo.VideoURL];
            [sendgrid sendWithWeb:email successBlock:^(id responseObject) {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Sent" andText:@"Your Share has been sent." andCancelButton:NO forAlertType:AlertSuccess withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                }];
                alert.cornerRadius = 3.0f;
                [alert show];
            } failureBlock:^(NSError *error) {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Failure!" andText:@"Your Email Cannot be sent." andCancelButton:NO forAlertType:AlertFailure withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                }];
                alert.cornerRadius = 3.0f;
                [alert show];
            }];
            
            NSLog(@"popup view dismissed");
        }];
    }
}

- (IBAction)plusChallengersAction:(id)sender {
    [self performSegueWithIdentifier:@"challengeToProfilea" sender:self];
}

- (IBAction)openChallengersList:(id)sender {
   
    
    
    
    

}

- (IBAction)backAction:(id)sender {
    
    [self performSegueWithIdentifier:@"challengeToVideoFeed" sender:self];
}
- (IBAction)unwindToSharingAndComments:
(UIStoryboardSegue*)sender
{
}
- (IBAction)GoToMakeBattle:(UIButton *)sender {
    
    
    NSLog(@"Need to make Challenge here") ;
    
    if ([self.selectedChallenge.MainVideo.User.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Stop" andText:[NSString stringWithFormat:@"You already Have a Challenge video for this Challenge."] andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        
    }
    else{
        
        for (Video *video in self.selectedChallenge.OtherVideos) {
            if ([video.User.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Cannot Challenge!" andText:[NSString stringWithFormat:@"You already Have a Challenge video for this Challenge."] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                return ;
            }
        }
        
        [self performSegueWithIdentifier:@"challengeToMakeBattle" sender:self];
    }
    
    
    
    
}
- (IBAction)commentStarted:(id)sender {
    NSLog(@"commentStarted") ;
    UIScrollView *mainView = (UIScrollView *)self.view ;
    [mainView setContentOffset:
     CGPointMake(0, -mainView.contentInset.top) animated:YES];}

- (IBAction)userProfileClickAction:(id)sender {
    [self performSegueWithIdentifier:@"sharingToUserProfile" sender:self];
}
@end
         

