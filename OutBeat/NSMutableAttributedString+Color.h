//
//  NSMutableAttributedString+Color.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 8/3/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSMutableAttributedString (Color)
-(void)setColorForText:(NSString*) textToFind withColor:(UIColor*) color;

@end
