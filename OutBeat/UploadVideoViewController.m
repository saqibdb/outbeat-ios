//
//  UploadVideoViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/26/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//
#import "UIViewController+CWPopup.h"
#import "BattleViewController.h"
#import "UploadVideoViewController.h"
#import "Backendless.h"
//#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import <MobileCoreServices/MobileCoreServices.h>
#import "Video.h"
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CoreMedia.h>
#import "challengersPopUp.h"
#import <AVFoundation/AVBase.h>
#import <Foundation/Foundation.h>
#import <CoreMedia/CMTime.h>
#import <CoreGraphics/CoreGraphics.h>
#import "ASJTagsView.h"
#import <Foundation/Foundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AMSmoothAlertView.h"
#import "DropDownTableViewCell.h"
#import "Battle.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "Challenge.h"
#import <Social/Social.h>
#import "PDKPin.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "EmailPopUp.h"
#import "SendGrid.h"
#import "SendGridEmail.h"

@interface UploadVideoViewController () <TLTagsControlDelegate>{
    NSURL *videoURL ;
    UIImage *thumbImageForVideo ;
    NSArray *battleTypesArray ;
    NSArray *categoryArray ;
    NSMutableArray *tagsArray;
    challengersPopUp *challengersPopUpView;
    NSString *challengersAmount;
    EmailPopUp *emailPopUpView ;
    BOOL isAborted;
}

@end

@implementation UploadVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.artBtn.enabled = NO;
    [self setup];
    [self.txtTags addDoneOnKeyboardWithTarget:self action:@selector(addTapped) shouldShowPlaceholder:YES];
    
    if ([self.delegate respondsToSelector:@selector(tellRegisterDelegateSomething:)] || self.isForChallenge ){
        [self.videoCredView setHidden:YES];
        //[self.videoSharingView setHidden:NO];

    }
    if (self.isForBattle || self.isForChallenge) {
        [self.videoSharingView setHidden:NO];
    }
    

    _tagsViewNew.tagPlaceholder = @"Tags";
    _tagsViewNew.tagsDeleteButtonColor = [UIColor blackColor];
    _tagsViewNew.mode = TLTagsControlModeEdit;
    _tagsViewNew.tagsBackgroundColor =  [UIColor colorWithRed:251/255.0 green:102/255.0 blue:68/255.0 alpha:1];
    _tagsViewNew.tagsTextColor = [UIColor blackColor];
    [_tagsViewNew reloadTagSubviews];
    [_tagsViewNew setTapDelegate:self];
    
    
    tagsArray =[[NSMutableArray alloc] init];
    _tagsView.hidden=YES;
    _txtTitel.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Title", nil)];
    _txtArt.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Type of competition", nil)];
    _txtCatagory.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Category", nil)];
    _txtTags.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Tags", nil)];
    _lblTitel.text=[NSString stringWithFormat:NSLocalizedString(@"Upload Video", nil)];
    
    [_addText setTitle: [NSString stringWithFormat:NSLocalizedString(@"Abort", nil)] forState: UIControlStateNormal];
    [_abortText setTitle: [NSString stringWithFormat:NSLocalizedString(@"Add", nil)] forState: UIControlStateNormal];
    
    
    _lblDescription.text=[NSString stringWithFormat:NSLocalizedString(@"DESCRIPTION", nil)];
    [self.dropDownView setHidden:YES];
    [self.view bringSubviewToFront:self.dropDownView];
    
    [self.dropDownTableView setDelegate:self];
    [self.dropDownTableView setDataSource:self];
    
    battleTypesArray = [[NSArray alloc] initWithObjects:@"Battle", @"Challenge" , nil];

    categoryArray = [[NSArray alloc] initWithObjects:@"Sport", @"Musik", @"Aussergewöhnliches",@"Fun",@"Sonstiges", nil];
    
    // Do any additional setup after loading the view.
    
    
    if (self.isForBattle || self.isForChallenge) {
        [self.addText setTitle:@"Back" forState:UIControlStateNormal] ;
    }
    
    [self.view layoutIfNeeded];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.tabbarShadowView.bounds];
    self.tabbarShadowView.layer.masksToBounds = NO;
    self.tabbarShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabbarShadowView.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    self.tabbarShadowView.layer.shadowOpacity = 0.2f;
    self.tabbarShadowView.layer.shadowPath = shadowPath.CGPath;
    
   
}

#pragma mark - TLTagsControlDelegate
- (void)tagsControl:(TLTagsControl *)tagsControl tappedAtIndex:(NSInteger)index {
    NSLog(@"Tag \"%@\" was tapped", tagsControl.tags[index]);
}

#pragma mark - tag view methods
- (void)setup
{
    _tagsView.tagColor = [UIColor orangeColor];
    [self handleTagBlocks];
    
}

#pragma mark - Tag blocks

- (void)handleTagBlocks
{
   
    __weak typeof(self) weakSelf = self;
    [_tagsView setTapBlock:^(NSString *tagText, NSInteger idx)
     {
         
     }];
    
    [_tagsView setDeleteBlock:^(NSString *tagText, NSInteger idx)
     {
         
         
         [weakSelf.tagsView deleteTagAtIndex:idx];
     }];
}
- (void)addTapped
{
    _tagsView.hidden=NO;
    [_tagsView addTag:self.txtTags.text];
    self.txtTags.text = nil;
    [self.txtTags resignFirstResponder];
   
    [tagsArray addObject:self.txtTags.text];
     NSLog(@"%lu",(unsigned long)tagsArray.count);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)abortBtnAction:(id)sender {
    
    isAborted =YES;
    [self.uploadBtn setEnabled:YES] ;

    self.thumbImage.image=nil;
    self.txtTitel.text=@"";
    self.txtArt.text=@"";
    self.txtCatagory.text=@"";
    _tagsViewNew.tags=nil;
    [_tagsViewNew reloadTagSubviews];
    
    if ([self.comingFromVC isEqualToString:@"SharingAndComment"]) {
        [self performSegueWithIdentifier:@"bactToSharing" sender:self];

    }
    else {
        if (self.isForBattle || self.isForChallenge) {
            [self performSegueWithIdentifier:@"backToBattleScreen" sender:self];
        }
    }

    //self.artBtn.enabled = YES;

    [SVProgressHUD dismiss];
    
}
-(void)openPopUp{
    
        challengersPopUpView = [[challengersPopUp
                            alloc] initWithNibName:@"challengersPopUp" bundle:nil];
    
    [self presentPopupViewController:challengersPopUpView animated:YES completion:^(void) {
            [challengersPopUpView.doneBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        }];

    
    
}
- (void)dismissPopup {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
         challengersAmount =  challengersPopUpView.txtChallengeramount.text;
            
            NSLog(@"popup view dismissed");
        }];
    }
}

- (IBAction)addBtnAction:(id)sender {
    
}

- (IBAction)uploadVideoBtnAction:(id)sender {
    
    if (videoURL) {
        //self.artBtn.enabled = YES;
  
        isAborted=NO;
        [self.uploadBtn setEnabled:NO] ;
        [SVProgressHUD showProgress:0.0 status:@"Uploading Video...0%"];
        Video *vid = [Video new];
        vid.Title = self.txtTitel.text ;
        id<IDataStore> dataStore = [backendless.persistenceService of:[Video class]];
        [dataStore save:vid response:^(id obj) {
            Video *nVid = (Video *)obj ;
            
            if (isAborted) {
                
                [dataStore remove:nVid responder:nil];
                
                return ;
            }
        
            [SVProgressHUD showProgress:10.0 status:@"Uploading Video...10%"];

            NSLog(@"vid Saved %@", nVid.objectId);
            NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
            
            [backendless.fileService upload:[NSString stringWithFormat:@"media/%@.mp4", nVid.objectId] content:videoData response:^(BackendlessFile *file) {
                
                if (isAborted) {
                    [backendless.fileService remove:file.fileURL responder:nil];
                    [dataStore remove:nVid responder:nil];
                    return ;
                }
                
                [SVProgressHUD showProgress:60.0 status:@"Uploading Video...60%"];

                
                NSData *imageData = UIImageJPEGRepresentation(thumbImageForVideo, 1.0);
                
                    [backendless.fileService upload:[NSString stringWithFormat:@"media/%@.jpg", nVid.objectId] content:imageData response:^(BackendlessFile *imageFile) {
                      
                        if (isAborted) {
                            [dataStore remove:nVid responder:nil];
                            [backendless.fileService remove:imageFile.fileURL responder:nil];
                            return ;
                        }
                        [SVProgressHUD showProgress:90.0 status:@"Uploading Video...90%"];

                        
                        nVid.VideoURL = file.fileURL ;
                        nVid.User = backendless.userService.currentUser;
                        nVid.ThumbnailURL = imageFile.fileURL ;
                        nVid.Category=self.txtArt.text;
                        nVid.Type=self.txtCatagory.text;
                        nVid.alreadyBattle=YES;
                        
                        
                        NSMutableArray *tags = _tagsViewNew.tags;
                        NSLog(@"%@", tags);
                        
                        NSString *sentence;
                        if (!tags.count) {
                            sentence = @" ";
                        }
                        sentence = [tags componentsJoinedByString:@" "];
                        nVid.Tags=sentence;
                        nVid.ChallengersAmount=challengersAmount;
                        
                    [dataStore save:nVid response:^(id savedObj) {
                        NSLog(@"vid saved %@",savedObj);
                        [self.uploadBtn setEnabled:YES] ;
                        [SVProgressHUD showProgress:95.0 status:@"Uploading Video...95%"];

                        
                        //[self viewDidLoad];
                        /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Successful" andText:@"The Video has been uploaded." andCancelButton:NO forAlertType:AlertSuccess];
                        [alert show];
                        */
                        
                        
                        if ([self.txtArt.text isEqualToString:@"Challenge"]) {
                            Challenge *newChallenge = [Challenge new] ;
                            newChallenge.MainVideo = savedObj ;
                            newChallenge.ChallengeLimit = challengersAmount ;
                            [dataStore save:newChallenge response:^(id newSavedChallenge) {
                                [SVProgressHUD dismiss];
                                NSLog(@"Challenge Created = %@", newSavedChallenge);
                                [self.uploadBtn setEnabled:YES] ;
                                
                                
                                self.thumbImage.image=nil;
                                self.txtTitel.text=@"";
                                self.txtArt.text=@"";
                                self.txtCatagory.text=@"";
                                _tagsViewNew.tags=nil;
                                [_tagsViewNew reloadTagSubviews];
                                
                                [self performSegueWithIdentifier:@"backToBattleScreen" sender:self];
                            } error:^(Fault *errorOnChallenge) {
                                [SVProgressHUD dismiss];
                                [self.uploadBtn setEnabled:YES] ;
                                
                                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:errorOnChallenge.detail andCancelButton:NO forAlertType:AlertFailure];
                                [alert show];
                            }];
                        }
                        else if ([self.txtArt.text isEqualToString:@"Battle"])
                        {
                            Video *video1 = savedObj ;
                            Battle *newBattle = [[Battle alloc]init] ;
                            newBattle.videoOne = video1 ;
                            
                            [dataStore save:newBattle response:^(id newBattleSaved) {
                                [SVProgressHUD dismiss];
                                NSLog(@"Battle Created = %@", newBattleSaved);
                                [self.uploadBtn setEnabled:YES] ;
                                
                                self.thumbImage.image=nil;
                                self.txtTitel.text=@"";
                                self.txtArt.text=@"";
                                self.txtCatagory.text=@"";
                                _tagsViewNew.tags=nil;
                                [_tagsViewNew reloadTagSubviews];
                                
                                [self performSegueWithIdentifier:@"backToBattleScreen" sender:self];
                            } error:^(Fault *error) {
                                [SVProgressHUD dismiss];
                                [self.uploadBtn setEnabled:YES] ;
                                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
                                [alert show];
                            }];

                        }
                        
                        if (self.isForBattle) {
                            Video *video1 = self.videoForBattle ;
                            Video *video2 = savedObj ;
                            Battle *newBattle = [[Battle alloc]init] ;
                            newBattle.videoOne = video1 ;
                            newBattle.videoTwo = video2 ;
                            
                            [dataStore save:newBattle response:^(id newBattleSaved) {
                                [SVProgressHUD dismiss];
                                NSLog(@"Battle Created = %@", newBattleSaved);
                                [self.uploadBtn setEnabled:YES] ;
                                
                                self.thumbImage.image=nil;
                                self.txtTitel.text=@"";
                                self.txtArt.text=@"";
                                self.txtCatagory.text=@"";
                                _tagsViewNew.tags=nil;
                                [_tagsViewNew reloadTagSubviews];
                                
                                [self performSegueWithIdentifier:@"backToBattleScreen" sender:self];
                            } error:^(Fault *error) {
                                [SVProgressHUD dismiss];
                                [self.uploadBtn setEnabled:YES] ;
                                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
                                [alert show];
                            }];
                        }
                        else if (self.isForChallenge){
                            Challenge *challenge = self.videoForBattle ;
                            [challenge.OtherVideos addObject:savedObj];
                            [dataStore save:challenge response:^(id newChallengeSaved) {
                                [SVProgressHUD dismiss];
                                NSLog(@"newChallengeSaved Created = %@", newChallengeSaved);
                                [self.uploadBtn setEnabled:YES] ;
                                
                                self.thumbImage.image=nil;
                                self.txtTitel.text=@"";
                                self.txtArt.text=@"";
                                self.txtCatagory.text=@"";
                                _tagsViewNew.tags=nil;
                                [_tagsViewNew reloadTagSubviews];
                                
                                [self performSegueWithIdentifier:@"backToBattleScreen" sender:self];
                            } error:^(Fault *challengeSavingError) {
                                [SVProgressHUD dismiss];
                                [self.uploadBtn setEnabled:YES] ;
                                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:challengeSavingError.detail andCancelButton:NO forAlertType:AlertFailure];
                                [alert show];
                            }];
                        }
                    } error:^(Fault *error) {
                        NSLog(@"vid Error %@",error.detail);
                        [SVProgressHUD dismiss];
                        [self.uploadBtn setEnabled:YES] ;
                        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
                        [alert show];
                    }];
                } error:^(Fault *error) {
                    [SVProgressHUD dismiss];
                    [self.uploadBtn setEnabled:YES] ;
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
                    [alert show];
                }];
            } error:^(Fault *error) {
                NSLog(@"vid Error %@",error.detail);
                [SVProgressHUD dismiss];
                [self.uploadBtn setEnabled:YES] ;
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }];
        } error:^(Fault *error) {
            NSLog(@"vid Error %@",error.detail);
            [SVProgressHUD dismiss];
            [self.uploadBtn setEnabled:YES] ;
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }];
    }
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Video" andText:@"There is No Video Selected to be Uploaded." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }
  }

- (IBAction)imageBtnAction:(UIButton *)sender {
    if([UIImagePickerController isSourceTypeAvailable:
        UIImagePickerControllerSourceTypePhotoLibrary]) {
        
        UIImagePickerController *picker= [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        picker.mediaTypes = [NSArray arrayWithObjects:(NSString *)kUTTypeMovie, nil];
        
        [self presentViewController:picker animated:YES completion:nil];
    }
    
}
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary*)info{
    
    videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:videoURL options:nil];
    AVAssetImageGenerator * generator = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    generator.appliesPreferredTrackTransform = YES;
    CMTime time = CMTimeMakeWithSeconds(0.0, 600);
    CMTime actualTime;
    NSError * error = nil;
    generator.maximumSize = CGSizeMake(320, 320);

    
    CGImageRef imageRef = [generator copyCGImageAtTime:time actualTime:&actualTime error:&error];
    UIImage * thumb = [[UIImage alloc] initWithCGImage:imageRef];
    CGImageRelease(imageRef);
    
    
    thumbImageForVideo= thumb ;
    self.thumbImage.image = thumb ;

    NSLog(@"Data of the Image is %@", videoURL) ;
    
    
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - tableView Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (tableView.tag == 1) {
        return battleTypesArray.count;
    }
    else{
        return categoryArray.count;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"DropDownTableViewCell";
    DropDownTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[DropDownTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                        reuseIdentifier:CellIdentifier];
    }
    if (tableView.tag == 1) {
        cell.dropDownTxt.text = battleTypesArray[indexPath.row];
       
    }
    else{
        cell.dropDownTxt.text = categoryArray[indexPath.row];

    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (tableView.tag == 1) {
        _txtArt.text = battleTypesArray[indexPath.row];
        //_artBtn.tag = 0 ;
        _categoryBtn.tag =0;
        _artDownIcon.image = [UIImage imageNamed:@"downArrow"] ;
        if([_txtArt.text isEqualToString:@"Challenge"]){
            [self openPopUp];
        }

    }
    else{
        _txtCatagory.text = categoryArray[indexPath.row];
        _catBtn.tag = 0 ;
        _catDownIcon.image = [UIImage imageNamed:@"downArrow"];
    }
    [self.dropDownView setHidden:YES];

}

- (IBAction)typeComtAction:(UIButton *)sender {
    _dropDownTopSpace.constant = 0 ;
    self.dropDownTableView.tag = 1 ;
    if (!sender.tag) {
        [self.dropDownView setHidden:NO];
        _artDownIcon.image = [UIImage imageNamed:@"upArrow"];
        sender.tag = 1 ;
        _catBtn.tag = 0 ;
        _catDownIcon.image = [UIImage imageNamed:@"downArrow"];
        _dropDownHeight.constant = 90 ;
    }
    else{
        [self.dropDownView setHidden:YES];
        _artDownIcon.image = [UIImage imageNamed:@"downArrow"];
        sender.tag = 0 ;
    }
    [self.dropDownTableView reloadData];
}

- (IBAction)catAction:(UIButton *)sender {
    _dropDownTopSpace.constant = 45 ;
    self.dropDownTableView.tag = 2 ;
    if (!sender.tag) {
        [self.dropDownView setHidden:NO];
        _catDownIcon.image = [UIImage imageNamed:@"upArrow"];
        sender.tag = 1 ;
        //_artBtn.tag = 0 ;
        _categoryBtn.tag =0;
        _artDownIcon.image = [UIImage imageNamed:@"downArrow"];
        _dropDownHeight.constant = 128 ;

    }
    else{
        [self.dropDownView setHidden:YES];
        _catDownIcon.image = [UIImage imageNamed:@"downArrow"];
        sender.tag = 0 ;
    }
    [self.dropDownTableView reloadData];  

    
}

- (IBAction)facebookAction:(id)sender {
    NSString *shareUrl ;
    if (self.isForBattle) {
        Video *video1 = self.videoForBattle ;
        shareUrl = video1.VideoURL ;
        }
    else if (self.isForChallenge){
        Challenge *challenge = self.videoForBattle ;
        shareUrl = challenge.MainVideo.VideoURL ;
    }
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:shareUrl];
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil] ;
}

- (IBAction)twitterAction:(id)sender {
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        SLComposeViewController *twitterController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [twitterController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        NSString *shareUrl ;
        if (self.isForBattle) {
            Video *video1 = self.videoForBattle ;
            shareUrl = video1.VideoURL ;
        }
        else if (self.isForChallenge){
            Challenge *challenge = self.videoForBattle ;
            shareUrl = challenge.MainVideo.VideoURL ;
        }
        
        [twitterController setInitialText:@"Check out the OutBeat from Appstore Today!"];
        [twitterController addURL:[NSURL URLWithString:shareUrl]];
        [twitterController setCompletionHandler:completionHandler];
        [self presentViewController:twitterController animated:YES completion:nil];
    }
    
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Cannot Share" andText:[NSString stringWithFormat:@"You need Twitter app installed and have to be logged in to it to be able to Share to Twitter."] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        
        NSLog(@"you are not logged in ");
    }
}

- (IBAction)googleAction:(id)sender {
    NSString *shareUrl ;
    if (self.isForBattle) {
        Video *video1 = self.videoForBattle ;
        shareUrl = video1.VideoURL ;
    }
    else if (self.isForChallenge){
        Challenge *challenge = self.videoForBattle ;
        shareUrl = challenge.MainVideo.VideoURL ;
    }
    
    // Construct the Google+ share URL
    NSURLComponents* urlComponents = [[NSURLComponents alloc]
                                      initWithString:@"https://plus.google.com/share"];
    urlComponents.queryItems = @[[[NSURLQueryItem alloc]
                                  initWithName:@"url"
                                  value:[[NSURL URLWithString:shareUrl] absoluteString]]];
    NSURL* url = [urlComponents URL];
    
    if ([SFSafariViewController class]) {
        // Open the URL in SFSafariViewController (iOS 9+)
        SFSafariViewController* controller = [[SFSafariViewController alloc]
                                              initWithURL:url];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Open the URL in the device's browser
        [[UIApplication sharedApplication] openURL:url];
    }

}

- (IBAction)pinterestAction:(id)sender {
    NSString *shareUrl ;
    if (self.isForBattle) {
        Video *video1 = self.videoForBattle ;
        shareUrl = video1.VideoURL ;
    }
    else if (self.isForChallenge){
        Challenge *challenge = self.videoForBattle ;
        shareUrl = challenge.MainVideo.VideoURL ;
    }
    
    [PDKPin pinWithImageURL:[NSURL URLWithString:@"https://about.pinterest.com/sites/about/files/logo.jpg"]
                       link:[NSURL URLWithString:shareUrl]
         suggestedBoardName:@"OutBeat"
                       note:@"Check out the OutBeat from Appstore Today!"
         fromViewController:self
                withSuccess:^
     {
         //weakSelf.resultLabel.text = [NSString stringWithFormat:@"successfully pinned pin"];
     }
                 andFailure:^(NSError *error)
     {
         //weakSelf.resultLabel.text = @"pin it failed";
     }];

}

- (IBAction)mailAction:(id)sender {
    emailPopUpView = [[EmailPopUp alloc] initWithNibName:@"EmailPopUp" bundle:nil];
    
    emailPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width- 50, self.view.frame.size.height * 0.4 );
    
    
    [self presentPopupViewController:emailPopUpView animated:YES completion:^(void) {
        
        [emailPopUpView.shareBtn addTarget:self action:@selector(dismissPopupWithEmail) forControlEvents:UIControlEventTouchUpInside];
        [emailPopUpView.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
    }];
}

- (IBAction)categoryBtnAction:(UIButton *)sender {
    
    _dropDownTopSpace.constant = 0 ;
    self.dropDownTableView.tag = 1 ;
    if (!sender.tag) {
        [self.dropDownView setHidden:NO];
        _artDownIcon.image = [UIImage imageNamed:@"upArrow"];
        sender.tag = 1 ;
        _catBtn.tag = 0 ;
        _catDownIcon.image = [UIImage imageNamed:@"downArrow"];
        _dropDownHeight.constant = 90 ;
    }
    else{
        [self.dropDownView setHidden:YES];
        _artDownIcon.image = [UIImage imageNamed:@"downArrow"];
        sender.tag = 0 ;
    }
    [self.dropDownTableView reloadData];
}
- (void)dismissPopupWithEmail {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            SendGrid *sendgrid = [SendGrid apiUser:@"ibuildx" apiKey:@"umer7447"];
            SendGridEmail *email = [[SendGridEmail alloc] init];
            email.to = emailPopUpView.phoneTxt.text;
            email.from = @"info@ibuildx.com";
            email.subject = @"OutBeat";
            NSString *shareUrl ;
            if (self.isForBattle) {
                Video *video1 = self.videoForBattle ;
                shareUrl = video1.VideoURL ;
            }
            else if (self.isForChallenge){
                Challenge *challenge = self.videoForBattle ;
                shareUrl = challenge.MainVideo.VideoURL ;
            }
            email.text = [NSString stringWithFormat:@"Checkout the OutBeat from Appstore. %@" , shareUrl];
            [sendgrid sendWithWeb:email successBlock:^(id responseObject) {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Sent" andText:@"Your Share has been sent." andCancelButton:NO forAlertType:AlertSuccess withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                }];
                alert.cornerRadius = 3.0f;
                [alert show];
            } failureBlock:^(NSError *error) {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Failure!" andText:@"Your Email Cannot be sent." andCancelButton:NO forAlertType:AlertFailure withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                }];
                alert.cornerRadius = 3.0f;
                [alert show];
            }];
            
            NSLog(@"popup view dismissed");
        }];
    }
}
@end
