//
//  LoginViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/12/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "LoginViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "UIViewController+CWPopup.h"
#import "ForgetEmailPopUp.h"
#import "NeuerAccountViewController.h"
#import "OutBeat-swift.h"
#import "Utility.h"

@interface LoginViewController (){
    NSString  *isSocial;
    //LiquidLoader *liquidLoader ;
    UIView *grayView ;
}
@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.forgetView.hidden=true;
    // Do any additional setup after loading the view.
    _REGISTERBTN.layer.cornerRadius=17;
    _REGISTERBTN.clipsToBounds=YES;
    
    
    UITapGestureRecognizer *tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissPopup)];
    tapRecognizer.numberOfTapsRequired = 1;
    tapRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapRecognizer];
    self.useBlurForPopup = YES;
    
    self.resetEmailText.delegate = self ;
    self.emailtext.delegate = self ;
    self.emailtext.tag = 1 ;
    
    
}
-(void)viewDidAppear:(BOOL)animated{
    //liquidLoader = [Utility createAndAdjustLiquidLoader:self.view];
    //grayView = [Utility createGrayView] ;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




-(void)userLogin
{

    //[Utility showLoading:self.view andGreyBackground:grayView andLineLoader:liquidLoader];
    
    [SVProgressHUD show] ;
    
    NSLog(@"Email is %@", _emailtext.text) ;

    NSLog(@"password is %@", _passwordtext.text) ;
    [backendless.userService login:_emailtext.text password:_passwordtext.text response:^(BackendlessUser *user) {
        NSLog(@"user = %@", user.name);
        
        UIDevice *device = [UIDevice currentDevice];
        NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
        
       
        [backendless.userService setStayLoggedIn:YES] ;

        
        [user setProperty:@"deviceId" object:currentDeviceId];
        
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore save:user response:^(id user) {
            NSLog(@"Device Registered %@", currentDeviceId);

        } error:^(Fault *error) {
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        }];
        
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        
        [SVProgressHUD dismiss] ;
        
        
        [self performSegueWithIdentifier:@"LoginToTabBar" sender:self];
        
        
        
        
    } error:^(Fault *error) {
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        
        NSString *errorMsg = [NSString stringWithFormat:@"Failed to Login. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];

}


- (IBAction)loginAction:(id)sender {
    [self userLogin];
}

- (IBAction)BackToGetStartedFromLogin:(UIButton *)sender {
    
     [self performSegueWithIdentifier:@"backToGetStartedFromLogin" sender:self];
    

}

- (IBAction)forgetPasswordAction:(UIButton *)sender {
    
    self.resetView.hidden = !self.resetView.hidden ;
    
 }


-(void)userPasswordRecovery:(NSString *)login
{
    Responder *responder = [Responder responder:self
                             selResponseHandler:@selector(responseHandler:)
                                selErrorHandler:@selector(errorHandler:)];
    [backendless.userService restorePassword:login responder:responder];
}

-(id)responseHandler:(id)response;
{
    NSLog(@"password = %@", response);
          return response;
          }
          
          -(void)errorHandler:(Fault *)fault
    {
        NSLog(@"FAULT = %@ <%@>", fault.message, fault.detail);
    }


-(BOOL) NSStringIsValidEmail:(NSString *)checkString
{
    BOOL stricterFilter = NO; // Discussion http://blog.logichigh.com/2010/09/02/validating-an-e-mail-address/
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:checkString];
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];

    if ([self NSStringIsValidEmail:proposedNewString]) {
        if (textField.tag == 1) {
            [self.loginbtn setBackgroundColor:[UIColor colorWithRed:251.0/255.0 green:102.0/255.0 blue:68.0/255.0 alpha:1.0]];
        }
        else{
            [self.resetBtn setBackgroundColor:[UIColor colorWithRed:251.0/255.0 green:102.0/255.0 blue:68.0/255.0 alpha:1.0]];
        }
        
    }
    else{
        if (textField.tag == 1) {
            [self.loginbtn setBackgroundColor:[UIColor lightGrayColor]];
        }
        else{
            [self.resetBtn setBackgroundColor:[UIColor lightGrayColor]];
        }

    }
    return YES;
}

- (IBAction)forgetDoneAction:(id)sender {
    [_resetEmailText resignFirstResponder];
    if (![self NSStringIsValidEmail:_resetEmailText.text]) {
        
        
        
        
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc]
                                    initDropAlertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Valid_Eml_Title", nil)] andText:[NSString stringWithFormat:NSLocalizedString(@"Valid_Eml_Message", nil)] andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return;
    }
    
    [SVProgressHUD show];
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"email = '%@'", _resetEmailText.text];
   [dataStore find:query response:^(BackendlessCollection *collection) {
       if (collection.data.count == 0) {
           [SVProgressHUD dismiss];
           AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Email" andText:@"User with Specified Email does not exist." andCancelButton:NO forAlertType:AlertFailure];
           [alert show];
       }
       else{
           
           BackendlessUser *foundUser = collection.data[0];
           if (![[foundUser getProperty:@"isNotSocial"] isEqualToString:@"-"]) {
               
               [backendless.userService restorePassword:_resetEmailText.text response:^(id res) {
                   [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                   [SVProgressHUD dismiss];
                   
                   
                  
                   
                   
                   AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Pass_Reset_Title", nil)] andText:[NSString stringWithFormat:NSLocalizedString(@"Pass_Reset_Message", nil)] andCancelButton:NO forAlertType:AlertSuccess];
                   
                   [alert show];
                   
                   alert.completionBlock = ^void (AMSmoothAlertView *alertObj, UIButton *button) {
                       if(button == alertObj.defaultButton) {
                           [self  viewDidLoad];
                           _txtNewEmail.text = @"";
                           
                       }
                   };
                   
               } error:^(Fault *error) {
                   [SVProgressHUD dismiss];
                   [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Failed_Login_Title", nil)] andText:[NSString stringWithFormat:NSLocalizedString(@"Failed_Login_Message", nil)] andCancelButton:NO forAlertType:AlertFailure];
                   
                   [alert show];
               }];

               
               
           }
           else{
               [SVProgressHUD dismiss];
               AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Oops", nil)] andText:[NSString stringWithFormat:NSLocalizedString(@"Reset_Social", nil)] andCancelButton:NO forAlertType:AlertFailure];
               
               [alert show];
           }
           
           
       }
       
   } error:^(Fault *error) {
       
       
       
       AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Failed_Login_Title", nil)] andText:[NSString stringWithFormat:NSLocalizedString(@"Failed_Login_Message", nil)] andCancelButton:NO forAlertType:AlertFailure];
       
       [alert show];
   }];
   
    
    
}


- (IBAction)registerAction:(UIButton *)sender {
    if (self.isBackable) {
        [self performSegueWithIdentifier:@"backToRegister" sender:self];
    }
    else{
        [self performSegueWithIdentifier:@"ToRegister" sender:self];
    }
}

- (IBAction)resetAction:(id)sender {
    
    [self forgetDoneAction:sender];
}

#pragma mark - Methods

- (void)dismissPopup {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            NSLog(@"popup view dismissed");
        }];
    }
}
#pragma mark - gesture recognizer delegate functions

// so that tapping popup view doesnt dismiss it
- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    return touch.view == self.view;
}
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToRegister"]) {
        NeuerAccountViewController *vc = segue.destinationViewController ;
        vc.isBackable = YES;
    }
}
- (IBAction)unwindToLogin:(UIStoryboardSegue*)sender
{
}
@end
