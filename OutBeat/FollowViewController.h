//
//  FollowViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/7/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"
@interface FollowViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>


@property (weak, nonatomic) IBOutlet UITableView *uitableview;
- (IBAction)backAction:(id)sender;

@end
