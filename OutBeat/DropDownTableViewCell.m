//
//  DropDownTableViewCell.m
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 6/13/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "DropDownTableViewCell.h"

@implementation DropDownTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
