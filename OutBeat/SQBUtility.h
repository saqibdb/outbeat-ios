//
//  SQBUtility.h
//  outbeat
//
//  Created by iBuildx_Mac_Mini on 12/7/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"

typedef void (^SQBResponseBlock) (id object, BOOL status, Fault *error);

@interface SQBUtility : NSObject

+(void)getAllFrirendsOfUser :(BackendlessUser *)user responseBlock: (SQBResponseBlock)responseBlock ;
+(void)getAllFollowersOfUser :(BackendlessUser *)user responseBlock: (SQBResponseBlock)responseBlock ;
+(void)fetchingUsersInboxHistoryForUser :(BackendlessUser *)user andOtherUser :(BackendlessUser *)otherUser responseBlock: (SQBResponseBlock)responseBlock ;
+(void)refreshCurrentUserWithresponseBlock: (SQBResponseBlock)responseBlock;
@end
