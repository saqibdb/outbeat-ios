//
//  BattleViewController.m
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 6/3/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "BattleViewController.h"
#import "BattleTableViewCell.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Video.h"
#import "UIImageView+WebCache.h"
#import "VideoPlayViewController.h"
#import "Battle.h"
#import "UIViewController+CWPopup.h"
#import "SearchPopUp.h"
#import "UploadVideoViewController.h"
#import "Utility.h"
#import <UIKit/UIKit.h>
#import "OutBeat-swift.h"
#import "Challenge.h"
#import "HexColor.h"
#import <Social/Social.h>


@interface BattleViewController (){

    BOOL isBattle ;
    SearchPopUp *callUsPopUpView;
    NSMutableArray *BattlesArray;
    NSMutableArray *matchedBattlesFound;
    //LiquidLoader *liquidLoader ;
    UIView *grayView ;
    NSString *report;
    NSUInteger indexToReport;
}

@end

@implementation BattleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self fetchBattles];
    self.videoTableView.delegate = self ;
    self.videoTableView.dataSource = self ;
    
    self.searchFilters = [[NSMutableArray alloc] init];
    
    [ self.txtToSearch.layer setCornerRadius:5];
    [[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    [self.tabBarController.tabBar setValue:@(YES) forKeyPath:@"_hidesShadow"];
    [self.view layoutIfNeeded];
    
       
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.tabbarShadowView.bounds];
    self.tabbarShadowView.layer.masksToBounds = NO;
    self.tabbarShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabbarShadowView.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    self.tabbarShadowView.layer.shadowOpacity = 0.2f;
    self.tabbarShadowView.layer.shadowPath = shadowPath.CGPath;
}

-(void)viewDidAppear:(BOOL)animated{
    /*if (!liquidLoader) {
        liquidLoader = [Utility createAndAdjustLiquidLoader:self.view];
        grayView = [Utility createGrayView] ;
    }*/
    self.vidsFound = [[NSMutableArray alloc] init] ;
    [self.videoTableView reloadData];
    [ self fetchingVideosAsync ];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Backendless And Responses Methods
-(void)fetchBattles{

     [[backendless.persistenceService of:[Battle class]] find:nil response:^(BackendlessCollection *BattlesColection) {
        
         BattlesArray=[[NSMutableArray alloc]initWithArray:BattlesColection.data];
         NSLog(@"%lu",(unsigned long)BattlesArray.count);
         
    } error:^(Fault *error) {
        
    }];
    
    
}

-(void)fetchingVideosAsync {
    //[Utility showLoading:self.view andGreyBackground:grayView andLineLoader:liquidLoader];
    [SVProgressHUD show] ;
    
    isBattle =NO;
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"Category = 'Battle' AND alreadyBattle = False"];
    
    
    [[backendless.persistenceService of:[Video class]] find:query response:^(BackendlessCollection *userColection) {
        NSLog(@"Gender And Age Filtered Users Found = %lu",(unsigned long)userColection.data.count);
        self.vidsFound = [[NSMutableArray alloc] initWithArray:userColection.data];
        self.vidsFoundOriginal = [[NSMutableArray alloc] initWithArray:userColection.data];
        
        
        [[backendless.persistenceService of:[Challenge class]] find:nil response:^(BackendlessCollection *challengeCollection) {
            [self.vidsFound addObjectsFromArray:challengeCollection.data] ;
            [self.vidsFoundOriginal addObjectsFromArray:challengeCollection.data] ;
            [self.videoTableView reloadData];

            
        } error:^(Fault *challengeError) {
            NSLog(@"Error Fetching Challenges = %@", challengeError.description) ;
        }] ;
        
        [self.videoTableView reloadData];
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        
        if (_vidsFound.count == 0) {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Videos" andText:@"No Videos Found for current User." andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
        }
        
        
    } error:^(Fault *error) {
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        NSString *errorMsg = [NSString stringWithFormat:@"Failed to Get video Gather. Details = %@", error.detail];
        NSLog(@"Error Found at Video Gather %@",error.detail);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
    
}
#pragma mark - tableView Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.vidsFound.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"BattleTableViewCell";
    BattleTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[BattleTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                             reuseIdentifier:CellIdentifier];
    }
    
    Video *vid ;

    if ([[self.vidsFound objectAtIndex:indexPath.row] isKindOfClass:[Video class]]) {
        
        vid = [self.vidsFound objectAtIndex:indexPath.row] ;

    }
    else{
        Challenge *challenge = [self.vidsFound objectAtIndex:indexPath.row] ;
        vid = challenge.MainVideo ;
    }

    
    [cell.userProfileImage.layer setCornerRadius:cell.userProfileImage.frame.size.height/2];
    cell.userProfileImage.clipsToBounds=YES;
    cell.userProfileImage.layer.borderWidth=1.5;
    cell.userProfileImage.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    
    [cell.videoThumbImage sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
    cell.userNameLabel.text=vid.User.name;
    cell.lblVideoTitle.text=vid.Title;
    [cell.userProfileImage sd_setImageWithURL:[NSURL URLWithString:[vid.User getProperty:@"profileImage"] ]];
    
    cell.playVideoButtion.tag=indexPath.row;
    indexToReport=indexPath.row;
    [cell.playVideoButtion addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    if ([vid.Category isEqualToString:@"Battle"]){
        
        cell.battleOrChallengImage.image = [UIImage imageNamed: @"battle-big"];
        [cell.battleBtn setImage:[UIImage imageNamed: @"battle-big"] forState:UIControlStateNormal];
    }
    else if([vid.Category isEqualToString:@"Challenge"]){
        
        cell.battleOrChallengImage.image = [UIImage imageNamed: @"challenge-big"];
        [cell.battleBtn setImage:[UIImage imageNamed: @"challenge-big"] forState:UIControlStateNormal];
        
    }
    cell.battleBtn.tag = indexPath.row ;
    [cell.battleBtn addTarget:self action:@selector(gotoSelectBattle:) forControlEvents:UIControlEventTouchUpInside];
    cell.rightUtilityButtons = [self rightButtons];
    cell.delegate = self;

      return cell;
}

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
            case 0:
            [self openReportPopUp];
            break;
        case 1:
            [self shareTwitterAction];
            break;
        case 2:
            [self shareFbAction];
            break;
        case 3:
            [self openReportPopUp];
            break;
            
               default:
            break;
    }
}

- (IBAction)shareFbAction {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        
        SLComposeViewController *fbController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [fbController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        
        [fbController setInitialText:@"Look at this awesome website for aspiring iOS Developers!"];
        [fbController addURL:[NSURL URLWithString:@"http://www.ibuildx.com/"]];
        [fbController setCompletionHandler:completionHandler];
        [self presentViewController:fbController animated:YES completion:nil];
    }
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"you are not logged in your facebook account "] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        NSLog(@"you are not logged in ");
    }
    
    
}

- (IBAction)shareTwitterAction {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        SLComposeViewController *twitterController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [twitterController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        
        [twitterController setInitialText:@"Look at this awesome website for aspiring iOS Developers!"];
        [twitterController addURL:[NSURL URLWithString:@"http://www.ibuildx.com/"]];
        [twitterController setCompletionHandler:completionHandler];
        [self presentViewController:twitterController animated:YES completion:nil];
    }
    
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:[NSString stringWithFormat:@"you are not logged in your twitter account"] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        
        NSLog(@"you are not logged in ");
    }
    
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"cell selected");
    
    Video *vid ;
    if ([[self.vidsFound objectAtIndex:indexPath.row] isKindOfClass:[Video class]]) {
        vid = [self.vidsFound objectAtIndex:indexPath.row] ;
    }
    else{
        Challenge *challenge = [self.vidsFound objectAtIndex:indexPath.row] ;
        vid = challenge.MainVideo ;
    }
    self.selectedVideo = vid ;
    [self performSegueWithIdentifier:@"ToVideoPayback" sender:self];
}
#pragma mark - Navigation
- (void)tellRegisterDelegateSomething:(NSObject*)something
{
    
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToVideoPayback"]) {
        VideoPlayViewController *vc = segue.destinationViewController ;
        vc.selectedVideo = self.selectedVideo ;
    }
    
    
    if ([segue.identifier isEqualToString:@"MakeBattle"]) {
        UploadVideoViewController *vc = segue.destinationViewController ;
        
        if ([self.selectedVideo isKindOfClass:[Video class]]) {
            vc.isForBattle = YES ;
            vc.isForChallenge = NO ;

        }
        else if ([self.selectedVideo isKindOfClass:[Challenge class]]){
            vc.isForBattle = NO ;
            vc.isForChallenge = YES ;
        }
        
        vc.videoForBattle = self.selectedVideo ;
        vc.delegate=self;
       
    }
    
    
}
- (IBAction)unwindToBattleViewController:(UIStoryboardSegue*)sender
{
    
}

- (IBAction)searchAction:(UIButton *)sender {
    
    [self.searchView setHidden:NO];
    self.txtToSearch.text=@"";
    [self.txtToSearch becomeFirstResponder];
    
}
#pragma mark - Methods
-(void)gotoSelectBattle:(UIButton*)sender
{
    if ([self.vidsFound[sender.tag] isKindOfClass:[Video class]]) {
        Video *video = self.vidsFound[sender.tag];
        if ([backendless.userService.currentUser.objectId isEqualToString:video.User.objectId]) {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:@"You Cannot Make Battle on your Own Video." andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
            return;
        }
    }
    else if ([self.vidsFound[sender.tag] isKindOfClass:[Challenge class]]){
        Challenge *challenge = self.vidsFound[sender.tag];
        if ([backendless.userService.currentUser.objectId isEqualToString:challenge.MainVideo.User.objectId]) {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:@"You Cannot Make Challenge on your Own Video." andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
            return;
        }
        for (Video *video in challenge.OtherVideos) {
            if ([backendless.userService.currentUser.objectId isEqualToString:video.User.objectId]) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:@"You Have a Video Uploaded for this Challenge." andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                return;
            }
        }
    }
    
    
    
    self.selectedVideo = self.vidsFound[sender.tag] ;
    [self performSegueWithIdentifier:@"MakeBattle" sender:self];
    
    /*
    matchedBattlesFound=[[NSMutableArray alloc]init];
    Video *vid = self.vidsFound[sender.tag] ;
    
    for(Battle *btl in BattlesArray){
        
        if([btl.videoOne.objectId isEqualToString:vid.objectId] || [btl.videoTwo.objectId isEqualToString: vid.objectId]){
            [matchedBattlesFound addObject:btl];
            NSLog(@"%lu",(unsigned long)matchedBattlesFound.count);
        }
    }
    
    NSString *a=vid.ChallengersAmount;
    NSInteger b= [a integerValue];
    if(b == matchedBattlesFound.count){
        NSLog(@"this vid reached the challengers ammont");
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry!" andText:@"This Video reached the maximum Number of Challengers" andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        
    }else{
        NSLog(@"%lu", (unsigned long)matchedBattlesFound.count);
        self.selectedVideo = vid ;
        [self performSegueWithIdentifier:@"MakeBattle" sender:self];
        
    }
     
     
     */
}

-(void)playButtonClicked:(UIButton*)sender
{
    Video *vid ;
    if ([[self.vidsFound objectAtIndex:sender.tag] isKindOfClass:[Video class]]) {
        vid = [self.vidsFound objectAtIndex:sender.tag] ;
    }
    else{
        Challenge *challenge = [self.vidsFound objectAtIndex:sender.tag] ;
        vid = challenge.MainVideo ;
    }
    self.selectedVideo = vid ;
    [self performSegueWithIdentifier:@"ToVideoPayback" sender:self];
    
    
    
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    
    /*[rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithHexString:@"#3b5998"]
                                                title:@"Facebook"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithHexString:@"#00aced"]
                                                title:@"Twitter"];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithHexString:@"#d34836"]
                                                title:@"Google"];
    
    */
    
    NSString *Report = [NSString stringWithFormat:NSLocalizedString(@"Report", nil)];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:Report];
    
    
    return rightUtilityButtons;
}

- (void)dismissPopup {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            NSLog(@"popup view dismissed");
        }];
    }
}
-(void)openReportPopUp{
    //asdfsadfadsfsadfef
    
    callUsPopUpView = [[SearchPopUp alloc] initWithNibName:@"SearchPopUp" bundle:nil];
    
    //callUsPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width- 50, self.view.frame.size.height * 0.5 );
    
    callUsPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width- 50, callUsPopUpView.view.frame.size.height);
    
    
    TNImageRadioButtonData *one = [TNImageRadioButtonData new];
    one.labelText = @"its annoying or not interesting";
    one.identifier = @"its annoying or not interesting";
    one.selected = NO;
    one.unselectedImage = [UIImage imageNamed:@"unCheked"];
    one.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *two = [TNImageRadioButtonData new];
    two.labelText = @"i think it should'nt be on outBeat";
    two.identifier = @"i think it should'nt be on outBeat";
    two.selected = NO;
    two.unselectedImage = [UIImage imageNamed:@"unCheked"];
    two.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *three = [TNImageRadioButtonData new];
    three.labelText = @"its spam";
    three.identifier = @"its spam";
    three.selected = NO;
    three.unselectedImage = [UIImage imageNamed:@"unCheked"];
    three.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *four = [TNImageRadioButtonData new];
    four.labelText = @"its rude , vulger or uses bad language";
    four.identifier = @"its rude , vulger or uses bad language";
    four.selected = NO;
    four.unselectedImage = [UIImage imageNamed:@"unCheked"];
    four.selectedImage = [UIImage imageNamed:@"cheked"];
    
    
    
    
    callUsPopUpView.radioButtonGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[one, two,three,four] layout:TNRadioButtonGroupLayoutVertical];
    callUsPopUpView.radioButtonGroup.identifier = @"Temperature group";
    
    [callUsPopUpView.radioButtonGroup create];
    //callUsPopUpView.radioButtonGroup.position = CGPointMake(25, 400);
    [ callUsPopUpView.outerView addSubview : callUsPopUpView.radioButtonGroup];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(temperatureGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:callUsPopUpView.radioButtonGroup];
    
    
    
    [self presentPopupViewController:callUsPopUpView animated:YES completion:^(void) {
        [callUsPopUpView.cancelBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        [callUsPopUpView.sendBtn addTarget:self action:@selector(sendMail) forControlEvents:UIControlEventTouchUpInside];
    }];
    
    
    
}
-(void)sendMail{
    
    Video *vid = self.vidsFound [indexToReport];
    
    
    NSString *subject = @"Report from outBeat";
    NSString *body =[NSString stringWithFormat:@" hy we received a report against your video with title =%@ saying '%@' .please remove it ASAP",vid.Title, report];
    NSString *recipient = @"asadjavid46@gmail.com";
    NSLog(@"%@",recipient);
    [backendless.messagingService
     sendHTMLEmail:subject body:body to:@[recipient]
     response:^(id result) {
         NSLog(@"ASYNC: HTML email has been sent");
         [self dismissPopup];
     }
     error:^(Fault *fault) {
         NSLog(@"Server reported an error: %@", fault);
     }];
}

- (void)temperatureGroupUpdated:(NSNotification *)notification {
    NSLog(@"selected reason = %@", callUsPopUpView.radioButtonGroup.selectedRadioButton.data.identifier);
    report=callUsPopUpView.radioButtonGroup.selectedRadioButton.data.identifier;
}
- (void)createHorizontalListWithImage {
    
}

- (IBAction)unwindToBattleScreen:(UIStoryboardSegue *)unwindSegue
{
}
- (IBAction)searchBackAction:(UIButton *)sender {
    [self.searchView setHidden:YES];
    [self fetchingVideosAsync];
}

- (IBAction)searchOkAction:(UIButton *)sender {
    [self.lblNoResultsFound setHidden:YES];
    NSString* wordsToSearch=self.txtToSearch.text;
    if([wordsToSearch isEqualToString:@""]){
        NSLog(@"empty string");
    }
    else{
        
        BackendlessDataQuery *query = [BackendlessDataQuery query];
        query.whereClause = [NSString stringWithFormat:@"Tags LIKE \'%%%@%%\' OR Title LIKE \'%%%@%%\'  ", wordsToSearch,wordsToSearch];
        id<IDataStore> dataStore = [backendless.persistenceService of:[Video class]];
        [dataStore find:query response:^(BackendlessCollection *VidsToSearch) {
            _vidsFound=[[NSMutableArray alloc]init];
            
            NSMutableArray *one =[[NSMutableArray alloc]initWithArray:VidsToSearch.data];
            for(Video *vid in one){
                
                [_vidsFound addObject:vid];
            }
            if(_vidsFound.count ==0){
                [self.lblNoResultsFound setHidden:NO];
            }
            [self.videoTableView reloadData];
            
            
        } error:^(Fault *error) {
            
            
        }];
    }

}

- (IBAction)menuAction:(id)sender {
    [self performSegueWithIdentifier:@"ToSettings" sender:self];
}
@end
