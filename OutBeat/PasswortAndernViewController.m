//
//  PasswortAndernViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/11/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "PasswortAndernViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
@interface PasswortAndernViewController ()

@end

@implementation PasswortAndernViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _txtPassword.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Password", nil)];
    _txtNewPassword.placeholder=[NSString stringWithFormat:NSLocalizedString(@"New Password", nil)];
    _txtConfirmPassword.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Confirmation", nil)];
    _lblPasswordChange.text=[NSString stringWithFormat:NSLocalizedString(@"Change Password", nil)];
    
    self.txtPassword.delegate = self;
    self.txtNewPassword.delegate = self;
    self.txtConfirmPassword.delegate = self;
    self.txtPassword.tag = 1;
    self.txtNewPassword.tag = 2;
    self.txtConfirmPassword.tag = 3;

    
}
-(void)viewDidAppear:(BOOL)animated {
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];
    
    if (self.txtPassword.text.length > 0 && self.txtNewPassword.text.length > 0 && self.txtConfirmPassword.text.length > 0) {
        
        if (textField.tag == 2 || textField.tag == 3) {
            if (textField.tag == 3) {
                if ([self.txtNewPassword.text isEqualToString:proposedNewString]) {
                    [self.changeBtn setBackgroundColor:[UIColor colorWithRed:65.0/255.0 green:160.0/255.0 blue:85.0/255.0 alpha:1.0]];
                }
                else{
                    [self.changeBtn setBackgroundColor:[UIColor darkGrayColor]];
                }
            }
            else{
                if ([self.txtConfirmPassword.text isEqualToString:proposedNewString]) {
                    [self.changeBtn setBackgroundColor:[UIColor colorWithRed:65.0/255.0 green:160.0/255.0 blue:85.0/255.0 alpha:1.0]];
                }
                else{
                    [self.changeBtn setBackgroundColor:[UIColor darkGrayColor]];
                }
            }
            
        }
        else{
            if ([self.txtConfirmPassword.text isEqualToString:self.txtNewPassword.text]) {
                [self.changeBtn setBackgroundColor:[UIColor colorWithRed:65.0/255.0 green:160.0/255.0 blue:85.0/255.0 alpha:1.0]];
            }
            else{
                [self.changeBtn setBackgroundColor:[UIColor darkGrayColor]];
            }
        }
        
    }
    else{
        [self.changeBtn setBackgroundColor:[UIColor darkGrayColor]];
    }
    return YES;
}



- (IBAction)backAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"passwortAndernToSettings" sender:self];
}

- (IBAction)changePasswordAction:(UIButton *)sender {
    
    [SVProgressHUD show];
    
    
    BackendlessUser *updatePassword = backendless.userService.currentUser ;
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    
    
    NSString *storedPass =[backendless.userService.currentUser getProperty:@"copyPassword"];
    NSString *password =self.txtPassword.text;
    NSString *newPass = self.txtNewPassword.text;
    NSString *confirmPass=self.txtConfirmPassword.text;
    
    
    if ([newPass isEqualToString:confirmPass] && [password isEqualToString:storedPass]) {
        
        [updatePassword setProperty:@"password" object:self.txtNewPassword.text];
        [updatePassword setProperty:@"copyPassword" object:self.txtNewPassword.text];
    }
   
    
    
    
    [dataStore save:updatePassword response:^(BackendlessUser *updatePassword) {
        
        [SVProgressHUD dismiss];
       AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@" " andText:@"password updated" andCancelButton:NO forAlertType:AlertSuccess];
        
        [alert show];
        
    
    
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        
        NSString *errorMsg = [NSString stringWithFormat:@"Failed to change. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
        
        
    }];


      }
@end
