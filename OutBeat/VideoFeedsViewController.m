//
//  VideoFeedsViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/17/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//
#import "VideoFeedChallengesCell.h"
#import "VideoFeedsViewController.h"
#import "BattleVideosTableViewCell.h"
#import "Backendless.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+WebCache.h"
#import "VideoPlayViewController.h"
#import "BattleVideosTableViewCell.h"
#import "Battle.h"
#import "VoteViewController.h"
#import "UIViewController+CWPopup.h"
#import "SearchPopUp.h"

#import "SharingAndCommentsViewController.h"
#import "Utility.h"
#import <UIKit/UIKit.h>
#import "OutBeat-swift.h"
#import "Challenge.h"
#import "IQKeyboardManager.h"
#import "IQUIView+Hierarchy.h"
#import "IQUIView+IQKeyboardToolbar.h"


@interface VideoFeedsViewController (){
    
    BattleVideosTableViewCell *cell;
    NSMutableArray *videosFound;
    NSMutableArray *battleOneVids;
    NSMutableArray *battleTwoVids;
    NSString *storedView;
    SearchPopUp *callUsPopUpView ;
    NSString *wordsToSearch;
    LiquidLoader *liquidLoader ;
    UIView *grayView ;
    VideoFeedChallengesCell *cell1;
}

@end

@implementation VideoFeedsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view layoutIfNeeded];
    self.verticalSpace.constant= -self.dropDownView.frame.size.
    height;
    [self.dropDownView layoutIfNeeded];

    [self.txtSearch.layer setCornerRadius:5];
    
    self.battleVideosTableView.delegate = self ;
    self.battleVideosTableView.dataSource = self ;
    self.txtSearch.delegate=self;
    self.searchFilters = [[NSMutableArray alloc] init];
    [[UITabBar appearance] setBackgroundImage:[[UIImage alloc] init]];
    [[UITabBar appearance] setShadowImage:[[UIImage alloc] init]];
    
    [self.tabBarController.tabBar setValue:@(YES) forKeyPath:@"_hidesShadow"];
    
    self.vidsFound=[[NSMutableArray alloc]init];
    self.challengeVidsFound=[[NSMutableArray alloc] init];
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.tabbarShadowView.bounds];
    self.tabbarShadowView.layer.masksToBounds = NO;
    self.tabbarShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabbarShadowView.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    self.tabbarShadowView.layer.shadowOpacity = 0.2f;
    self.tabbarShadowView.layer.shadowPath = shadowPath.CGPath;
    
    //gesture rcognizers
    UISwipeGestureRecognizer *DownRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    DownRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [DownRecognizer setNumberOfTouchesRequired:1];
    [self.swipeDownView addGestureRecognizer:DownRecognizer];
    
    UISwipeGestureRecognizer *upRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(didSwipe:)];
    upRecognizer.direction = UISwipeGestureRecognizerDirectionUp;
    [upRecognizer setNumberOfTouchesRequired:1];
    [self.dropDownView addGestureRecognizer:upRecognizer];
    
    DownRecognizer.delegate=self;
    upRecognizer.delegate=self;
    
    [self.txtSearch addDoneOnKeyboardWithTarget:self action:@selector(doneAction:)];
    [IQKeyboardManager sharedManager].toolbarDoneBarButtonItemText = @"Search";

}
-(void)doneAction:(UIBarButtonItem*)barButton
{
    //doneAction
    [self.txtSearch resignFirstResponder] ;
    [self searchOkAction:nil] ;
}

- (void) didSwipe:(UISwipeGestureRecognizer *)recognizer{
    
    [UIView animateWithDuration:1.0 animations:^{
        
        self.verticalSpace.constant=0;
        
        [self.dropDownView layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        [self performSelector:@selector(swipeUpAction) withObject:nil afterDelay:1.5];//5sec
    }];
    
}
- (void)swipeUpAction{
    self.verticalSpace.constant=-self.dropDownView.frame.size.height;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        
        
    }];
    
}

-(void)viewDidAppear:(BOOL)animated{
    if (!liquidLoader) {
        liquidLoader = [Utility createAndAdjustLiquidLoader:self.view];
        grayView = [Utility createGrayView] ;
    }
    
    //[self fetchingChellengeVideos ];
    
    [self.lblNoResultsfound setHidden:YES];
}

- (void)viewWillAppear:(BOOL)animated {
    self.vidsFound=[[NSMutableArray alloc]init];
    self.challengeVidsFound=[[NSMutableArray alloc] init];
    [self.battleVideosTableView reloadData];
    
    NSUserDefaults *preferences = [NSUserDefaults standardUserDefaults];

    BOOL firstTimeChecker = [preferences boolForKey:@"firstTimeChecker"];
    
    if (!firstTimeChecker) {
        [_videoSwitchBtn setOn:NO] ;
        firstTimeChecker = YES ;
        [preferences setBool:firstTimeChecker forKey:@"firstTimeChecker"];
        [preferences synchronize];
    }
    
    [ self fetchingVideosAsync ];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)fetchingVideosAsync {
    
    //[Utility showLoading:self.view andGreyBackground:grayView andLineLoader:liquidLoader];
    //[SVProgressHUD show] ;
    
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[Battle class]];
    [dataStore find:nil response:^(BackendlessCollection *videosCollection) {
        _videosToShow =[[NSMutableArray alloc]initWithArray:videosCollection.data];
        _vidsFound =[[NSMutableArray alloc] init ];
        _vidsFoundOriginal =[[NSMutableArray alloc] init ];
        _friendsArray =[[NSMutableArray alloc] init ];
        _followersArray =[[NSMutableArray alloc] init ];
        _friendsArray =[backendless.userService.currentUser getProperty:@"friends"] ;
        _followersArray =[backendless.userService.currentUser getProperty:@"follower"] ;
        for (Battle *btl in _videosToShow) {
            if(_videoSwitchBtn.isOn){
                if ([btl.videoOne.User.objectId isEqualToString:backendless.userService.currentUser.objectId] || [btl.videoTwo.User.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
                    [_vidsFound addObject:btl];
                    [_vidsFoundOriginal addObject:btl];
                }
                for(int i=0 ;i<_friendsArray.count; i++)
                {
                    if([btl.videoOne.User.objectId isEqualToString:[_friendsArray[i] valueForKey:@"objectId"]] || [btl.videoTwo.User.objectId isEqualToString:[_friendsArray[i] valueForKey:@"objectId"]] )
                    {
                        if (![_vidsFoundOriginal containsObject:btl]) {
                            [_vidsFound addObject:btl] ;
                            [_vidsFoundOriginal addObject:btl];
                            
                        }
                    }
                }
            }
            else{
                
                [_vidsFound addObject:btl];
                [_vidsFoundOriginal addObject:btl];
                
            }
        }
        [self.battleVideosTableView reloadData];
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        
        
        
        [[backendless.persistenceService of:[Challenge class]] find:nil response:^(BackendlessCollection *challengeCollection) {
            [self.vidsFound addObjectsFromArray:challengeCollection.data] ;
            [self.vidsFoundOriginal addObjectsFromArray:challengeCollection.data] ;
            [self.battleVideosTableView reloadData];
            
            
        } error:^(Fault *challengeError) {
            NSLog(@"Error Fetching Challenges = %@", challengeError.description) ;
        }] ;
        
        
        
        
        
        [SVProgressHUD dismiss] ;

        
        if (_vidsFound.count == 0) {
            
            NSString *noBattlesMsg = [NSString stringWithFormat:NSLocalizedString(@"noBattlesMsg", nil)] ;
            
            
            
            
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Battles" andText:noBattlesMsg andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
        }
        
    } error:^(Fault *error) {
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        NSString *errorMsg = [NSString stringWithFormat:@"Failed to Get video feed. Details = %@", error.detail];
        NSLog(@"error at video feed = %@", error.description);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        
    }];
}
-(void)fetchingChellengeVideos{
    
    //[Utility showLoading:self.view andGreyBackground:grayView andLineLoader:liquidLoader];
    
    [SVProgressHUD show] ;
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"Category='Challenge'"];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Video class]];
    [dataStore find:query response:^(BackendlessCollection *ChellengeVideoCollection) {
        _friendsArray =[backendless.userService.currentUser getProperty:@"friends"] ;
        self.challengeVidsFound =[[NSMutableArray alloc]init];
        self.challengeVidsFoundOriginal =[[NSMutableArray alloc]init];
        self.ChellengeVideosArray =[[NSMutableArray alloc]initWithArray:ChellengeVideoCollection.data];
        for(Video *vid in self.ChellengeVideosArray){
            if(_videoSwitchBtn.isOn){
                if ([vid.User.objectId isEqualToString:backendless.userService.currentUser.objectId] || [vid.User.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
                    [_challengeVidsFound addObject:vid];
                    [_challengeVidsFoundOriginal addObject:vid];
                }
                for(int i=0 ;i<_friendsArray.count; i++)
                {
                    if([vid.User.objectId isEqualToString:[_friendsArray[i] valueForKey:@"objectId"]] || [vid.User.objectId isEqualToString:[_friendsArray[i] valueForKey:@"objectId"]] )
                    {
                        if (![_challengeVidsFoundOriginal containsObject:vid]) {
                            [_challengeVidsFound addObject:vid] ;
                            [_challengeVidsFoundOriginal addObject:vid];
                            
                        }
                    }
                }
            }
            
            else{
                
                [_challengeVidsFound addObject:vid];
                [_challengeVidsFoundOriginal addObject:vid];
                
            }
            
        }
        [self.battleVideosTableView reloadData];
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        
    } error:^(Fault *error) {
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        NSLog(@"error at vidoe gather");
        
    }];
}
#pragma mark - tableView delegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _vidsFound.count;

    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if([[_vidsFound objectAtIndex:indexPath.row] isKindOfClass:[Battle class]]){
        static NSString *CellIdentifier = @"BattleVideosTableViewCell";
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
        if (cell == nil) {
            cell = [[BattleVideosTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                    reuseIdentifier:CellIdentifier];
        }
        
        Battle  *battle = [_vidsFound objectAtIndex:indexPath.row] ;
        
        [cell.thumbail_first sd_setImageWithURL:[NSURL URLWithString:battle.videoOne.ThumbnailURL]];
        [cell.thumbnail_second sd_setImageWithURL:[NSURL URLWithString:battle.videoTwo.ThumbnailURL]];
        [cell.profileImage_first sd_setImageWithURL:[NSURL URLWithString:[battle.videoOne.User getProperty:@"profileImage"] ]];
        [cell.profileImage_second sd_setImageWithURL:[NSURL URLWithString:[battle.videoTwo.User getProperty:@"profileImage"]]];
        
        cell.userName_first.text= [battle.videoOne.User getProperty:@"name"];
        cell.userName_second.text= [battle.videoTwo.User getProperty:@"name"];
        NSString *wins = [NSString stringWithFormat:NSLocalizedString(@"wins", nil)];
        cell.totalWins_first.text =[NSString stringWithFormat:@"%@ %@" ,battle.videoOneWins,wins] ;
        cell.totalWins_second.text =[NSString stringWithFormat:@"%@ %@" ,battle.videoTwoWins,wins] ;
        
        
        cell.videoOneTitle.text=battle.videoOne.Title;
        cell.videoTwoTitle.text=battle.videoTwo.Title;
        
        cell.votes_first.text=battle.videoOneVotes;
        cell.votes_second.text=battle.videoTwoVotes;
        
        [cell layoutIfNeeded];
        
        [cell.profileImage_first.layer setCornerRadius:cell.profileImage_first.frame.size.height/2];
        cell.profileImage_first.clipsToBounds=YES;
        cell.profileImage_first.layer.borderWidth=1.5;
        cell.profileImage_first.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        [cell.profileImage_second.layer setCornerRadius:cell.profileImage_second.frame.size.height/2];
        cell.profileImage_second.clipsToBounds=YES;
        cell.profileImage_second.layer.borderWidth=1.5;
        cell.profileImage_second.layer.borderColor=[[UIColor whiteColor] CGColor];
        
        [cell.VS_spriteView.layer setCornerRadius:cell.VS_spriteView.frame.size.height/2];
        cell.VS_spriteView.clipsToBounds=YES;
        
        cell.backgroundColor = [UIColor clearColor];
        [cell.vsIcon.layer setCornerRadius:cell.vsIcon.frame.size.width];

        /*
        UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:cell.vsIcon.bounds];
        cell.vsIcon.layer.masksToBounds = NO;
        cell.vsIcon.layer.shadowColor = [UIColor blackColor].CGColor;
        cell.vsIcon.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
        cell.vsIcon.layer.shadowOpacity = 0.3f;
        cell.vsIcon.layer.shadowPath = shadowPath.CGPath;
        */
        return cell;
    }
    
    //section 2
    else{
        static NSString *CellIdentifier1 = @"challengeCell";
        cell1 = [tableView dequeueReusableCellWithIdentifier:CellIdentifier1];
        if (cell1 == nil) {
            cell1 = [[VideoFeedChallengesCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                   reuseIdentifier:CellIdentifier1];
        }
        Challenge *challenge = [_vidsFound objectAtIndex:indexPath.row];
        
        [cell1.videoThumnail sd_setImageWithURL:[NSURL URLWithString:challenge.MainVideo.ThumbnailURL]];
        [cell1.userProfileImage sd_setImageWithURL:[NSURL URLWithString:[challenge.MainVideo.User getProperty:@"profileImage"]]];
        cell1.userName.text=challenge.MainVideo.User.name;
        cell1.playVideoBtn.tag=indexPath.row;
        cell1.lblVidTitle.text=challenge.MainVideo.Title;
        [cell1.btnType setImage:[UIImage imageNamed: @"challenge-big"] forState:UIControlStateNormal];
        
        [cell1.userProfileImage.layer setCornerRadius:cell.profileImage_first.frame.size.height/2];
        cell1.userProfileImage.clipsToBounds=YES;
        cell1.userProfileImage.layer.borderWidth=1.5;
        cell1.userProfileImage.layer.borderColor=[[UIColor whiteColor] CGColor];
        //        [cell1.playVideoButtion addTarget:self action:@selector(yourButtonClicked:)       forControlEvents:UIControlEventTouchUpInside];
        for(int i=0 ; i<challenge.OtherVideos.count ;i++)
        {
            Video *nvid =challenge.OtherVideos[i];
            UIImageView *img =cell1.TileViewImages[i];
            UIView *view=cell1.TileViews[i];
            [view setHidden:NO];
            [img sd_setImageWithURL:[NSURL URLWithString:nvid.ThumbnailURL]
                   placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                            options:SDWebImageRefreshCached];
            
            
        }
        cell1.backgroundColor = [UIColor clearColor];
        return cell1;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    //self.selectedBattle=[_vidsFound objectAtIndex:indexPath.row] ;
    
    if ([[_vidsFound objectAtIndex:indexPath.row] isKindOfClass:[Battle class]]){
        Battle *battle = [_vidsFound objectAtIndex:indexPath.row] ;
        storedView= battle.Views;
        
        NSInteger num =[storedView integerValue];
        num= num + 1;
        battle.Views= [NSString stringWithFormat:@"%ld" ,(long)num] ;
        id<IDataStore> dataStore = [backendless.persistenceService of:[Battle class]];
        [dataStore save:battle response:^(id status) {
            NSLog(@"viewer incresed");
            
        } error:^(Fault *error) {
            //code
            NSLog(@"Error at viewer increment %@" ,error.detail);
        }];
        
        [self performSegueWithIdentifier:@"VideoFeedsToVotesViewController" sender:self];
    }
    else{
        self.selectedChallenge = _vidsFound[indexPath.row];
        NSInteger num =[self.selectedChallenge.Views integerValue];
        num= num + 1;
        self.selectedChallenge.Views= [NSString stringWithFormat:@"%ld" ,(long)num] ;
        id<IDataStore> dataStore = [backendless.persistenceService of:[Challenge class]];
        [dataStore save:self.selectedChallenge response:^(id status) {
            NSLog(@"viewer incresed");
        } error:^(Fault *error) {
            //code
            NSLog(@"Error at viewer increment %@" ,error.detail);
        }];
        [self performSegueWithIdentifier:@"videoFeedToChallenge" sender:self];
    }
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if(scrollView.contentOffset.y < -80)
    {
        NSLog(@"Did Scroll");
        self.verticalSpace.constant=0;

        [UIView animateWithDuration:0.5 animations:^{
            

            [self.view layoutIfNeeded];
        } completion:^(BOOL finished) {
            
            [self performSelector:@selector(swipeUpAction) withObject:nil afterDelay:2.0];//5sec
        }];
        
    }
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"VideoFeedsToVotesViewController"]) {
        NSIndexPath *indexPath = [self.battleVideosTableView indexPathForSelectedRow];
        
        VoteViewController *destViewController = segue.destinationViewController;
        
        
        self.selectedBattleAtVideoFeed=[_vidsFound objectAtIndex:indexPath.row] ;
        
        
        destViewController.selectedBattle=self.selectedBattleAtVideoFeed;
        
    }
    if ([segue.identifier isEqualToString:@"videoFeedToChallenge"]) {
        
        
        SharingAndCommentsViewController *vc = segue.destinationViewController;
        
        vc.selectedChallenge=self.selectedChallenge;
    }
}

- (IBAction)videoSwitch:(UISwitch *)sender {
    
    [self fetchingVideosAsync ];
    [self fetchingChellengeVideos];
}

#pragma mark - Methods
- (IBAction)searchAction:(UIButton *)sender {
    
    [self.searchView setHidden:NO];
    self.txtSearch.text=@"";
    [self.txtSearch becomeFirstResponder];
    
}
- (IBAction)unwindToVideoFeed:(UIStoryboardSegue*)sender
{
    NSLog(@"UNWINDED HERE ");
}

- (IBAction)searchBackAction:(UIButton *)sender {
    [self.lblNoResultsfound setHidden:YES];
    [self.searchView setHidden:YES];
    [self fetchingVideosAsync];
}
- (IBAction)changePasswordAction:(UIButton *)sender {
    
}

- (IBAction)searchOkAction:(UIButton *)sender {
    [self.lblNoResultsfound setHidden:YES];
    wordsToSearch=self.txtSearch.text;
    wordsToSearch = [wordsToSearch lowercaseString] ;
    if([wordsToSearch isEqualToString:@""]){
        NSLog(@"empty string");
        _vidsFound = [[NSMutableArray alloc] initWithArray:_vidsFoundOriginal];
    }
    else{
        //section one search querey
        _vidsFound = [[NSMutableArray alloc] init];
        for (int i = 0 ; i < _vidsFoundOriginal.count; i++) {
            if([[_vidsFoundOriginal objectAtIndex:i] isKindOfClass:[Battle class]]){
                Battle *battleToBeSearched = [_vidsFoundOriginal objectAtIndex:i] ;
                if ([[battleToBeSearched.videoOne.Title lowercaseString] containsString:wordsToSearch] || [[battleToBeSearched.videoTwo.Title lowercaseString] containsString:wordsToSearch]) {
                    [_vidsFound addObject:battleToBeSearched] ;
                }
            }
            else{
                Challenge *challengeToBeSearched = [_vidsFoundOriginal objectAtIndex:i] ;
                if ([[challengeToBeSearched.MainVideo.Title lowercaseString] containsString:wordsToSearch]) {
                    [_vidsFound addObject:challengeToBeSearched] ;
                }
            }
        }
    }
    [self.battleVideosTableView reloadData] ;
    [self.txtSearch resignFirstResponder] ;
}

- (IBAction)menuAction:(id)sender {
    [self performSegueWithIdentifier:@"ToSettings" sender:self];
}
@end
