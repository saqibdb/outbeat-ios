//
//  EinstellungenViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/18/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "EinstellungenViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"

@interface EinstellungenViewController ()

@end

@implementation EinstellungenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _lblchangeProfile.text=[NSString stringWithFormat:NSLocalizedString(@"Change Profile", nil)];
    _lblChangePassword.text=[NSString stringWithFormat:NSLocalizedString(@"Change Password", nil)];
    _lblDeleteAccount.text=[NSString stringWithFormat:NSLocalizedString(@"Delete Account", nil)];
    _lblPrivacyPolicy.text=[NSString stringWithFormat:NSLocalizedString(@"Privacy Policy", nil)];
    _lblUserConditions.text=[NSString stringWithFormat:NSLocalizedString(@"User Conditions", nil)];
    _lblUserTerms.text=[NSString stringWithFormat:NSLocalizedString(@"User terms", nil)];
    _lblTitle.text=[NSString stringWithFormat:NSLocalizedString(@"Settings", nil)];
    
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backToSettings:(UIButton *)sender {
    [self performSegueWithIdentifier:@"backToprofile" sender:self];
}

- (IBAction)logOutAction:(UIButton *)sender {
    
    
    
    [backendless.userService logout:^(id user) {
        NSLog(@"logged out");
        [self performSegueWithIdentifier:@"logOutToGetStarted" sender:self];
        
    } error:^(Fault *error) {
        
        NSLog(@"error at log out");
    }
     
     ];

    
    
}

- (IBAction)privacyPolicyBtnAction:(id)sender {
    /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Under construction" andText:@"The Page is Currently under construction" andCancelButton:NO forAlertType:AlertInfo];
    
    [alert show];*/

}

- (IBAction)userConditionBtnAction:(id)sender {
    /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Under construction" andText:@"The Page is Currently under construction" andCancelButton:NO forAlertType:AlertInfo];
    
    [alert show];*/

}

- (IBAction)impressumBtnAction:(id)sender {
    /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Under construction" andText:@"The Page is Currently under construction" andCancelButton:NO forAlertType:AlertInfo];
    
    [alert show];*/

}

- (IBAction)passwortAndernBtnAction:(id)sender {
    
    
    NSString *mail =[backendless.userService.currentUser getProperty:@"email"];
    NSString *isSocial =[backendless.userService.currentUser getProperty:@"isNotSocial"];

    
    
    if (![mail isEqualToString:isSocial]) {
       AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Sorry" andText:@"Changing Password for Facebook Login is not allowed" andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];

        
            }
    else
    {
        [self performSegueWithIdentifier:@"settingsToPasswortChange" sender:self];
    }
}
- (IBAction)unwindToSettings:(UIStoryboardSegue*)sender
{
    
}
@end
