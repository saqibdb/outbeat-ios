//
//  ChatViewController.m
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 7/20/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "ChatViewController.h"
#import "MessagesTableViewCell.h"
#import "Inbox.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "UIImageView+WebCache.h"
#import "IQUIView+IQKeyboardToolbar.h"


@interface ChatViewController ()

@end

@implementation ChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.inboxTableView setDelegate:self];
    [self.inboxTableView setDataSource:self];
    self.allMessages = [[NSMutableArray alloc] init];
    
    
    // border radius
    [_textViewRounded.layer setCornerRadius:5.0f];
    
    // border
    UIColor *borderColor = [UIColor colorWithRed:233.0/255.0 green:235.0/255.0 blue:234.0/255.0 alpha:1.0] ;
    [_textViewRounded.layer setBorderColor:borderColor.CGColor];
    [_textViewRounded.layer setBorderWidth:1.0f];
    
    // drop shadow
    [_textViewRounded.layer setShadowColor:[UIColor grayColor].CGColor];
    [_textViewRounded.layer setShadowOpacity:0.7];
    [_textViewRounded.layer setShadowRadius:2.0];
    [_textViewRounded.layer setShadowOffset:CGSizeMake(1.0, 2.0)];
    
    
    
    [self.view layoutIfNeeded] ;
    
  /*  // Build a triangular path
    UIBezierPath *path = [UIBezierPath new];
    [path moveToPoint:(CGPoint){_textViewRounded.frame.origin.x, _textViewRounded.frame.origin.y}];
    
    
    [path addLineToPoint:(CGPoint){_textViewRounded.frame.origin.x + _textViewRounded.frame.size.width, _textViewRounded.frame.origin.y}];
    
    [path addLineToPoint:(CGPoint){_textViewRounded.frame.origin.x + _textViewRounded.frame.size.width - 20, _textViewRounded.frame.origin.y + 20}];
    
    
    //[path addLineToPoint:(CGPoint){_textViewRounded.frame.origin.x + _textViewRounded.frame.size.width - 20, _textViewRounded.frame.origin.y + _textViewRounded.frame.size.height}];
    
    
    
    //[path addLineToPoint:(CGPoint){_textViewRounded.frame.origin.x , _textViewRounded.frame.origin.y + _textViewRounded.frame.size.height}];
    
    
    [path addLineToPoint:(CGPoint){_textViewRounded.frame.origin.x , _textViewRounded.frame.origin.y}];
    
    
    // Create a CAShapeLayer with this triangular path
    // Same size as the original imageView
    CAShapeLayer *mask = [CAShapeLayer new];
    mask.frame = _textViewRounded.bounds;
    mask.path = path.CGPath;
    
    // Mask the imageView's layer with this shape
    _textViewRounded.layer.mask = mask;
    
    
    
    */
    
    self.textBoxInput.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Berühren um zu schreiben", nil)];
    [self.textBoxInput addRightButtonOnKeyboardWithText:@"Send" target:self action:@selector(sendAction) shouldShowPlaceholder:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveNotification:)
                                                 name:@"NewMessage"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [self fetchingUsersInboxAsync];
}

- (void) receiveNotification:(NSNotification *) notification{
    NSDictionary *userInfo = notification.userInfo;
    
    NSString *message = nil;
    
    id alert = [userInfo objectForKey:@"alert"];
    if ([alert isKindOfClass:[NSString class]]) {
        message = alert;
    } else if ([alert isKindOfClass:[NSDictionary class]]) {
        message = [alert objectForKey:@"body"];
    }
    if (message)
    {
        Inbox *newMessage = [[Inbox alloc] init] ;
        newMessage.Sender = self.otherUser ;
        newMessage.Reciever = backendless.userService.currentUser ;
        newMessage.messageBody = message ;
        if (!self.allMessages) {
            self.allMessages = [[NSMutableArray alloc] init] ;
        }
        [self.allMessages addObject:newMessage];
        [self.inboxTableView reloadData];
        [self.inboxTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.allMessages.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];

    }
    [self fetchingUsersInboxAsync];
        

}







-(void)sendAction {
    [self.textBoxInput resignFirstResponder];
    if (self.textBoxInput.text.length) {
        Inbox *newInbox = [[Inbox alloc] init] ;
        newInbox.messageBody = self.textBoxInput.text ;
        newInbox.Sender = backendless.userService.currentUser ;
        newInbox.Reciever = self.otherUser ;
        
        newInbox.isSeen = @"NO" ;
        newInbox.isBlocked = @"NO" ;

        
        id<IDataStore> dataStore = [backendless.persistenceService of:[Inbox class]];
        [dataStore save:newInbox response:^(id savedInbox) {
            [self publishMessageAsPushNotificationAsync:self.textBoxInput.text forDevice:[self.otherUser getProperty:@"deviceId"]];
            
            self.textBoxInput.text = @"" ;
            if (!self.allMessages) {
                self.allMessages = [[NSMutableArray alloc] init];
            }
            [self.allMessages addObject:savedInbox] ;
            [self.inboxTableView reloadData] ;
            [self.inboxTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.allMessages.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];

            
        } error:^(Fault *error) {
            NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
            
            [alert show];
        }];
    }
}
-(void)publishMessageAsPushNotificationAsync:(NSString *)message forDevice:(NSString *)deviceId
{
    NSLog(@"PUSH MSG_________%@_________",message);
    DeliveryOptions *deliveryOptions = [DeliveryOptions new];
    deliveryOptions.pushSinglecast = [@[deviceId] mutableCopy];
    
    PublishOptions *publishOptionsPush = [PublishOptions new];
    //publishOptions.headers = @{@"android-content-title":@"Notification title for Android",
    //                          @"android-content-text":@"Notification text for Android"};
    
    
    [backendless.messagingService
     publish:@"default" message:message publishOptions:publishOptionsPush deliveryOptions:deliveryOptions
     response:^(MessageStatus *messageStatus) {
         if (messageStatus) {
             NSLog(@"MessageStatus = %@ <%@>", messageStatus.messageId, messageStatus.status);
         }
         
     }
     error:^(Fault *fault) {
         NSLog(@"FAULT at push publish = %@", fault);
     }
     ];
}



-(void)fetchingUsersInboxAsync {
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    
    QueryOptions *queryOption = [QueryOptions new];
    queryOption.sortBy = @[@"created DESC"];
    
    NSString *senderId ;
    NSString *recieverId ;
    
    if (self.selectedInbox) {
        senderId = self.selectedInbox.Sender.objectId ;
        recieverId = self.selectedInbox.Reciever.objectId ;
    }
    else{
        senderId = backendless.userService.currentUser.objectId ;
        recieverId = self.otherUser.objectId ;
    }

    NSLog(@"%@",[NSString stringWithFormat:@"((Reciever.objectId = '%@' AND Sender.objectId = '%@') OR (Sender.objectId = '%@' AND Reciever.objectId = '%@'))", recieverId, senderId, senderId, recieverId]);
    
    query.whereClause = [NSString stringWithFormat:@"((Reciever.objectId = '%@' AND Sender.objectId = '%@') OR (Reciever.objectId = '%@' AND Sender.objectId = '%@'))", recieverId, senderId, senderId, recieverId];
    query.queryOptions = queryOption;
    [backendless.persistenceService find:[Inbox class] dataQuery:query response:^(BackendlessCollection *collection1)  {
        NSLog(@"Messgaes Found %i", [[collection1 getTotalObjects] intValue]);
        self.allMessages = [[NSMutableArray alloc] initWithArray:collection1.data] ;
        self.allMessages=[[[self.allMessages reverseObjectEnumerator] allObjects] mutableCopy];
        [self.inboxTableView reloadData];
        if (self.allMessages.count) {
            [self.inboxTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.allMessages.count-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    } error:^(Fault *error) {
        NSLog(@"Inbox Found %@",error.description);
    }];
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.msgViewBottomConstraint.constant = 5;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    self.msgViewBottomConstraint.constant = height+5;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}
#pragma mark - tableView Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.allMessages.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"MessagesTableViewCell";
    MessagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MessagesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:CellIdentifier];
    }
    [cell layoutIfNeeded] ;
    Inbox *inbox = [self.allMessages objectAtIndex:indexPath.row];
    
    
    cell.userName.text = inbox.Sender.name;
    [cell.userAvatar sd_setImageWithURL:[NSURL URLWithString:[inbox.Sender getProperty:@"profileImage"]]
                       placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                options:SDWebImageRefreshCached];
    
    [cell.userAvatar.layer setCornerRadius:cell.userAvatar.frame.size.height/2];
    cell.userAvatar.clipsToBounds=YES;
    cell.userAvatar.layer.borderWidth = 1.0;
    cell.userAvatar.layer.borderColor = [UIColor colorWithRed:251.0f/255.0f
                                                        green:102.0f/255.0f
                                                         blue:68.0f/255.0f
                                                        alpha:1.0f].CGColor;
    cell.userMessage.text = inbox.messageBody ;
    
    
    
    
    NSLog(@"message is %@",inbox.messageBody);
    
    
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)backAction:(UIButton *)sender {
    if (self.comingFromProfile) {
        [self performSegueWithIdentifier:@"backToProfile" sender:self];
    }
    else{
        [self performSegueWithIdentifier:@"backToInbox" sender:self];

    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (IBAction)mailAction:(id)sender {
}
@end
