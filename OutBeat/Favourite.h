//
//  Favourite.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/15/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Favourite : NSObject


@property (nonatomic, strong) NSString *video;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *thumbnail;

@end
