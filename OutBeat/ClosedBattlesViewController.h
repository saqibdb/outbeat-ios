//
//  ClosedBattlesViewController.h
//  outbeat
//
//  Created by iBuildx_Mac_Mini on 12/22/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Battle.h"

@interface ClosedBattlesViewController : UIViewController<UITableViewDataSource , UITableViewDelegate, UICollectionViewDelegate , UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *batlleTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *battleCollectionView;



@property (strong) NSMutableArray  *videos;

- (IBAction)backAction:(id)sender;

@end
