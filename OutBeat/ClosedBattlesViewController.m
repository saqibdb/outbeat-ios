//
//  ClosedBattlesViewController.m
//  outbeat
//
//  Created by iBuildx_Mac_Mini on 12/22/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "ClosedBattlesViewController.h"
#import "ClosedBattleVideosTableViewCell.h"
#import "Video.h"
#import "UIImageView+WebCache.h"
#import "VideoPlayViewController.h"
#import "ClosedBattlesCollectionViewCell.h"

@interface ClosedBattlesViewController (){
    Video *selectedVideo;
}

@end

@implementation ClosedBattlesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.batlleTableView.delegate = self;
    self.batlleTableView.dataSource = self;
    self.battleCollectionView.delegate = self;
    self.battleCollectionView.dataSource = self;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.videos.count;
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    // Adjust cell size for orientation
    [self.view layoutIfNeeded];
    return CGSizeMake((self.view.frame.size.width / 2) - 10, (self.view.frame.size.width / 2) - 10);
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ClosedBattlesCollectionViewCell *cell = (ClosedBattlesCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"ClosedBattlesCollectionViewCell" forIndexPath:indexPath];
    
    [cell layoutIfNeeded];
    Video *video = [self.videos objectAtIndex:indexPath.row];
    cell.videoTitle.text = video.Title;
    cell.videoUserName.text = video.User.name;
    [cell.videoImageView sd_setImageWithURL:[NSURL URLWithString:video.ThumbnailURL]
                           placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                    options:SDWebImageRefreshCached];
    [cell.videoUserProfile sd_setImageWithURL:[NSURL URLWithString:[video.User getProperty:@"profileImage"]]
                             placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                      options:SDWebImageRefreshCached];
    [cell.videoUserProfile.layer setCornerRadius:cell.videoUserProfile.frame.size.height/2];
    cell.videoUserProfile.clipsToBounds=YES;
    cell.videoUserProfile.layer.borderWidth = 1.0;
    cell.videoUserProfile.layer.borderColor = [UIColor colorWithRed:251.0f/255.0f
                                                              green:102.0f/255.0f
                                                               blue:68.0f/255.0f
                                                              alpha:1.0f].CGColor;
    cell.palyBtn.tag = indexPath.row;
    [cell.palyBtn addTarget:self
                     action:@selector(playAction:)
           forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
    
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.videos.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"ClosedBattleVideosTableViewCell";
    ClosedBattleVideosTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[ClosedBattleVideosTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                            reuseIdentifier:CellIdentifier];
    }
    [cell layoutIfNeeded];
    Video *video = [self.videos objectAtIndex:indexPath.row];
    cell.videoTitle.text = video.Title;
    cell.videoUserName.text = video.User.name;
    [cell.videoImageView sd_setImageWithURL:[NSURL URLWithString:video.ThumbnailURL]
                       placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                options:SDWebImageRefreshCached];
    [cell.videoUserProfile sd_setImageWithURL:[NSURL URLWithString:[video.User getProperty:@"profileImage"]]
                       placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                options:SDWebImageRefreshCached];
    [cell.videoUserProfile.layer setCornerRadius:cell.videoUserProfile.frame.size.height/2];
    cell.videoUserProfile.clipsToBounds=YES;
    cell.videoUserProfile.layer.borderWidth = 1.0;
    cell.videoUserProfile.layer.borderColor = [UIColor colorWithRed:251.0f/255.0f
                                                        green:102.0f/255.0f
                                                         blue:68.0f/255.0f
                                                        alpha:1.0f].CGColor;
    cell.palyBtn.tag = indexPath.row;
    [cell.palyBtn addTarget:self
                                  action:@selector(playAction:)
       forControlEvents:UIControlEventTouchUpInside];

    return cell;
}


- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    selectedVideo = self.videos[indexPath.row];
    [self performSegueWithIdentifier:@"ToVideoPaybackFromClosed" sender:self];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    selectedVideo = self.videos[indexPath.row];
    [self performSegueWithIdentifier:@"ToVideoPaybackFromClosed" sender:self];

}
-(void)playAction :(UIButton *)btn {
    selectedVideo = self.videos[btn.tag];
    [self performSegueWithIdentifier:@"ToVideoPaybackFromClosed" sender:self];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToVideoPaybackFromClosed"]) {
        VideoPlayViewController *vc = segue.destinationViewController ;
        vc.selectedVideo = selectedVideo ;
        vc.previousController = @"ClosedBattle";
    }
}


- (IBAction)backAction:(id)sender {
    [self performSegueWithIdentifier:@"BackToProfile" sender:self];
    
}
- (IBAction)unwindToClosed:(UIStoryboardSegue*)sender
{
}
@end
