//
//  profileInformation.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/14/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
#import "NeuerAccountViewController.h"
@interface profileInformation : NSObject


@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;
//-(void)saveNewContact;
@end
