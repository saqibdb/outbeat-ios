//
//  Friends.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/2/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"

@interface Friends : NSObject
@property (nonatomic, strong) NSString *objectId;

@property (nonatomic, strong) NSString *FriendUserId;
@property (nonatomic, strong) NSString *RequestStatus;
@property (nonatomic, strong) NSString *SendingUserId;




@end
