//
//  JoinedVideoViewController.h
//  VidEditor
//
//  Created by ibuildx on 11/1/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import "Video.h"

@interface JoinedVideoViewController : UIViewController <AVPlayerViewControllerDelegate>
- (IBAction)backAction:(UIButton *)sender;

@property (strong,nonatomic) NSString *previousController;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoTitle;
@property (strong) Video *selectedVideo;
@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (nonatomic) AVPlayerViewController *playerViewController;

@end
