//
//  MessagesTableViewCell.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 7/18/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessagesTableViewCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;
@property (weak, nonatomic) IBOutlet UILabel *userName;

@property (weak, nonatomic) IBOutlet UILabel *userMessage;

@property (weak, nonatomic) IBOutlet UIButton *chatBtn;




@end
