//
//  UserProfileViewController.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 8/9/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"
#import "Inbox.h"

@protocol UserProfileViewDelegate <NSObject>
- (void)tellUserProfileDelegateSomething:(NSObject*)something;
@end


@interface UserProfileViewController : UIViewController

@property (weak, nonatomic) id <UserProfileViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView *circulerProfile;
@property (weak, nonatomic) IBOutlet UIImageView *blurImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeAddressLabel;

//friends
@property (weak, nonatomic) IBOutlet UIImageView *image_1;
@property (weak, nonatomic) IBOutlet UIImageView *image_2;
@property (weak, nonatomic) IBOutlet UIImageView *image_3;
@property (weak, nonatomic) IBOutlet UIImageView *image_4;
@property (weak, nonatomic) IBOutlet UIImageView *image_5;
@property (weak, nonatomic) IBOutlet UIImageView *image_6;
//followers
@property (weak, nonatomic) IBOutlet UIImageView *image_7;
@property (weak, nonatomic) IBOutlet UIImageView *image_8;
@property (weak, nonatomic) IBOutlet UIImageView *image_9;
@property (weak, nonatomic) IBOutlet UIImageView *image_10;
@property (weak, nonatomic) IBOutlet UIImageView *image_11;
@property (weak, nonatomic) IBOutlet UIImageView *image_12;

@property (weak, nonatomic) IBOutlet UIButton *PlusFriends;
@property (weak, nonatomic) IBOutlet UIButton *plusFollowerAction;

@property (weak, nonatomic) IBOutlet UIView *WBV_1;
@property (weak, nonatomic) IBOutlet UIView *WBV_2;
@property (weak, nonatomic) IBOutlet UIView *WBV_3;
@property (weak, nonatomic) IBOutlet UIView *WBV_4;
@property (weak, nonatomic) IBOutlet UIView *WBV_5;

@property (weak, nonatomic) IBOutlet UIView *LBV_1;
@property (weak, nonatomic) IBOutlet UIView *LBV_2;
@property (weak, nonatomic) IBOutlet UIView *LBV_3;
@property (weak, nonatomic) IBOutlet UIView *LBV_4;
@property (weak, nonatomic) IBOutlet UIView *LBV_5;

@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_1;
@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_2;
@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_3;
@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_4;
@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_5;
@property (weak, nonatomic) IBOutlet UIView *btn_WBV;
@property (weak, nonatomic) IBOutlet UIButton *plus_btn_WBV;
- (IBAction)playVideoAction:(id)sender;
- (IBAction)playVideoAction_2:(id)sender;
- (IBAction)playVideoAction_3:(id)sender;
- (IBAction)playVideoAction_4:(id)sender;
- (IBAction)playVideoAction_5:(id)sender;

- (IBAction)playVideoAction_6:(id)sender;
- (IBAction)playVideoAction_7:(id)sender;
- (IBAction)playVideoAction_8:(id)sender;
- (IBAction)playVideoAction_9:(id)sender;
- (IBAction)playVideoAction_10:(id)sender;






@property (strong) NSMutableArray *_usersArray;
@property (weak, nonatomic) IBOutlet UIButton *playVideoOutLet;

@property (strong) NSMutableArray *follwersArray;
@property (strong) NSMutableArray *winningVideosArray;
@property (strong) NSMutableArray *allFreindsCircles;
@property (strong) NSMutableArray *allFollwersCircles;
@property (strong) NSMutableArray *allVideoBox;



@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_1;
@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_2;
@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_3;
@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_4;
@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_5;
@property (weak, nonatomic) IBOutlet UIView *btn_LBV;

- (IBAction)PlusFriendsAction:(id)sender;
- (IBAction)plusFollowerAction:(id)sender;

- (IBAction)addFollowersAction:(id)sender;
- (IBAction)addFriendsAction:(id)sender;

- (IBAction)MessageBtnAction:(UIButton *)sender;
- (IBAction)menueBtnAction:(UIButton *)sender;

@property (strong) NSMutableArray  *vidsFound;
@property (strong) NSMutableArray  *BattleVidsFound;

@property (strong) NSMutableArray  *lostVidsFound;
@property (strong) NSMutableArray  *wonVidsFound;

@property (strong) Video *selectedVideo;



@property (weak, nonatomic) IBOutlet UILabel *winNumberTxt;
@property (weak, nonatomic) IBOutlet UILabel *lostNumberTxt;
@property (weak, nonatomic) IBOutlet UILabel *battlesNumberTxt;

@property (weak, nonatomic) IBOutlet UITextView *aboutMeText;

@property (weak, nonatomic) IBOutlet UIButton *messageBtn;



@property (weak, nonatomic) IBOutlet UIView *tabbarShadowView;

@property (strong) BackendlessUser  *selectedUser ;

@property (strong) NSString  *previousController ;

@property (weak, nonatomic) IBOutlet UILabel *lblWins;
@property (weak, nonatomic) IBOutlet UILabel *lblLost;
@property (weak, nonatomic) IBOutlet UILabel *lblBattles;
@property (weak, nonatomic) IBOutlet UILabel *lblAbout;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowers;
@property (weak, nonatomic) IBOutlet UILabel *lblWinningbattles;
@property (weak, nonatomic) IBOutlet UILabel *lblLostBattles;


@property (weak, nonatomic) IBOutlet UILabel *lblFriends;
@property (weak, nonatomic) IBOutlet UIButton *followerBtn;

@property (weak, nonatomic) IBOutlet UIButton *friendBtn;
@property (strong) Inbox  *selectedInbox;


@end
