//
//  AccountLoschenViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/11/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "AccountLoschenViewController.h"
#import "Backendless.h"
//#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"

@interface AccountLoschenViewController ()

@end

@implementation AccountLoschenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    _lblTitle.text=[NSString stringWithFormat:NSLocalizedString(@"Delete Account", nil)];
    _viewText.text=[NSString stringWithFormat:NSLocalizedString(@"We regret that you want to delete your account . We hope to you soon bye.", nil)];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated {
    
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"AccountLoschenToSettings" sender:self];
}

- (IBAction)deleteAccountAction:(UIButton *)sender {
    
    
    
    [SVProgressHUD show];
    
    BackendlessUser *updateUser = backendless.userService.currentUser ;
    
    
       id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore remove:updateUser response:^(NSNumber *number) {
        
        NSLog(@"Account removed");
        [self performSegueWithIdentifier:@"deleteToGetStarted" sender:self];
        
    } error:^(Fault *error) {
        NSLog(@"error at Account removed");
    }];

    
   }

#pragma mark - responder
@end
