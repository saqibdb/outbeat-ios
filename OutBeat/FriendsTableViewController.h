//
//  FriendsTableViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/2/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendsTableViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

@property (strong, nonatomic) IBOutlet UITableView *friendsTableView;
- (IBAction)backAction:(id)sender;

@property (strong) NSMutableArray *friendsArrayIds;

@end
