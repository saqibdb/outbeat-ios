//
//  EinstellungenViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/18/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EinstellungenViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblchangeProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePassword;
@property (weak, nonatomic) IBOutlet UILabel *lblDeleteAccount;
@property (weak, nonatomic) IBOutlet UILabel *lblPrivacyPolicy;
@property (weak, nonatomic) IBOutlet UILabel *lblUserConditions;
@property (weak, nonatomic) IBOutlet UILabel *lblUserTerms;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

- (IBAction)backToSettings:(UIButton *)sender;
- (IBAction)logOutAction:(UIButton *)sender;
- (IBAction)privacyPolicyBtnAction:(id)sender;
- (IBAction)userConditionBtnAction:(id)sender;
- (IBAction)impressumBtnAction:(id)sender;
- (IBAction)passwortAndernBtnAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *listView;


@end
