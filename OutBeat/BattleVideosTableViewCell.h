//
//  BattleVideosTableViewCell.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/17/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BattleVideosTableViewCell : UITableViewCell

// leftView
@property (weak, nonatomic) IBOutlet UIImageView *thumbail_first;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage_first;
@property (weak, nonatomic) IBOutlet UILabel *userName_first;
@property (weak, nonatomic) IBOutlet UILabel *totalWins_first;
@property (weak, nonatomic) IBOutlet UILabel *votes_first;
@property (weak, nonatomic) IBOutlet UIButton *playVideo_first;
@property (weak, nonatomic) IBOutlet UILabel *videoOneTitle;

// rightView
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_second;
@property (weak, nonatomic) IBOutlet UIImageView *profileImage_second;
@property (weak, nonatomic) IBOutlet UILabel *userName_second;
@property (weak, nonatomic) IBOutlet UILabel *totalWins_second;
@property (weak, nonatomic) IBOutlet UILabel *votes_second;
@property (weak, nonatomic) IBOutlet UIButton *playVideoAction_second;
@property (weak, nonatomic) IBOutlet UILabel *videoTwoTitle;


@property (weak, nonatomic) IBOutlet UIView *VS_spriteView;

@property (weak, nonatomic) IBOutlet UIImageView *vsIcon;


@end
