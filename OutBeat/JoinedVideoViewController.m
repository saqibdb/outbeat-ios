//
//  JoinedVideoViewController.m
//  VidEditor
//
//  Created by ibuildx on 11/1/16.
//  Copyright © 2016 Saqibdb. All rights reserved.
//

#import "JoinedVideoViewController.h"
#import <AVKit/AVKit.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "AMSmoothAlertView.h"
#import <FBSDKShareKit/FBSDKShareKit.h>

@interface JoinedVideoViewController (){
    UIButton           *playBTN;
    NSURL *newAssetURL ;
    BOOL isComingFromFacebookShare ;
    BOOL isComingFromFInstaShare ;
    BOOL isVideoSaved ;
}

@end

@implementation JoinedVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
     [self.view layoutIfNeeded] ;
    
    self.lblVideoTitle.text = _selectedVideo.Title;
    
    _playerViewController = [[AVPlayerViewController alloc] init];
    _playerViewController.delegate = self;
    _playerViewController.view.frame = CGRectMake(0, 0, self.playerView.frame.size.width, self.playerView.frame.size.height);
    
    _playerViewController.showsPlaybackControls = YES;
    
    
    // First create an AVPlayerItem
    NSURL *URL = [NSURL URLWithString:self.selectedVideo.VideoURL];
    AVPlayerItem* playerItem = [AVPlayerItem playerItemWithURL:URL];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(itemDidFinishPlaying:) name:AVPlayerItemDidPlayToEndTimeNotification object:playerItem];
    
    
    _playerViewController.player = [AVPlayer playerWithPlayerItem:playerItem];
    _playerViewController.player.actionAtItemEnd = AVPlayerActionAtItemEndPause;
    
    
    [self.playerView addSubview:_playerViewController.view];
    UIImage *normImage = [UIImage imageNamed:@"preview"];
    playBTN = [UIButton buttonWithType:UIButtonTypeCustom];
    [playBTN addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    [playBTN setImage:normImage forState:UIControlStateNormal];
    playBTN.frame = CGRectMake(0, 0, normImage.size.width, normImage.size.height);
    playBTN.center = CGPointMake(_playerViewController.view.frame.size.width/2, _playerViewController.view.frame.size.height/2);
    [_playerViewController.view addSubview:playBTN];
    //[_playerViewController.player play];
    
    
    playBTN.hidden = NO ;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Process

- (void)playVideo:(id)sender
{
    
   playBTN.hidden = YES;
    [[_playerViewController player] play];
}
#pragma mark - Video Player Delegate

-(void)itemDidFinishPlaying:(NSNotification *) notification {
    
    playBTN.hidden = NO;
    AVPlayerItem *p = [notification object];
    [p seekToTime:kCMTimeZero];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)backAction:(UIButton *)sender {
    
    if ([_previousController isEqualToString:@"SharingAndComment"]) {
        [self performSegueWithIdentifier:@"backToSharingAndComment" sender:self];
    }
    else if ([_previousController isEqualToString:@"ClosedBattle"]){
        [self performSegueWithIdentifier:@"backToClosed" sender:self];
    }
    else if ([_previousController isEqualToString:@"VoteViewController"]) {
        
        [self performSegueWithIdentifier:@"videoPlayerToVotesVC" sender:self];
    }
    else {
        [self performSegueWithIdentifier:@"videoPlayerToBattleVC" sender:self];
    }
    
    
}

@end
