//
//  UploadVideoViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/26/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"
#import "ASJTagsView.h"
#import "Challenge.h"
#import "TLTagsControl.h"


@protocol UploadVideoViewDelegate <NSObject>
- (void)tellRegisterDelegateSomething:(NSObject*)something;
@end
@interface UploadVideoViewController : UIViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITableViewDelegate , UITableViewDataSource , SFSafariViewControllerDelegate>

@property (weak, nonatomic) id <UploadVideoViewDelegate> delegate;

- (IBAction)abortBtnAction:(id)sender;
- (IBAction)addBtnAction:(id)sender;
- (IBAction)uploadVideoBtnAction:(id)sender;
- (IBAction)imageBtnAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIButton *imageBtn;

@property (weak, nonatomic) IBOutlet UITextField *txtTitel;
@property (weak, nonatomic) IBOutlet UITextField *txtTags;
@property (weak, nonatomic) IBOutlet UITextField *txtCatagory;
@property (weak, nonatomic) IBOutlet UITextField *txtArt;
@property (weak, nonatomic) IBOutlet UIButton *abortText;
@property (weak, nonatomic) IBOutlet UIButton *addText;
@property (weak, nonatomic) IBOutlet UILabel *lblTitel;
@property (weak, nonatomic) IBOutlet UILabel *lblDescription;
@property (weak, nonatomic) IBOutlet UIImageView *catDownIcon;
@property (weak, nonatomic) IBOutlet ASJTagsView *tagsView;
@property (nonatomic, strong) IBOutlet TLTagsControl *tagsViewNew;

@property (weak, nonatomic) IBOutlet UIImageView *thumbImage;
@property (weak, nonatomic) IBOutlet UIImageView *artDownIcon;

@property (weak, nonatomic) IBOutlet UIView *dropDownView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropDownTopSpace;
@property (weak, nonatomic) IBOutlet UITableView *dropDownTableView;

@property (weak, nonatomic) IBOutlet UIButton *artBtn;
@property (weak, nonatomic) IBOutlet UIButton *catBtn;

@property (weak, nonatomic) IBOutlet UIButton *categoryBtn;


@property (weak, nonatomic) IBOutlet UIView *uploadVideoView;
@property (weak, nonatomic) IBOutlet UIView *videoCredView;
@property (copy, nonatomic) NSArray *tags;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *dropDownHeight;


@property (weak, nonatomic) id videoForBattle;
@property BOOL isForBattle;
@property BOOL isForChallenge;
@property (weak, nonatomic) NSString *comingFromVC;

@property (weak, nonatomic) IBOutlet UIButton *uploadBtn;

@property (weak, nonatomic) IBOutlet UIView *tabbarShadowView;
@property (weak, nonatomic) IBOutlet UIView *videoSharingView;

- (IBAction)typeComtAction:(UIButton *)sender;
- (IBAction)catAction:(UIButton *)sender;
- (IBAction)facebookAction:(id)sender;
- (IBAction)twitterAction:(id)sender;

- (IBAction)googleAction:(id)sender;
- (IBAction)pinterestAction:(id)sender;

- (IBAction)mailAction:(id)sender;
- (IBAction)categoryBtnAction:(id)sender;

@end
