//
//  ChallengersViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/30/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.

#import "Stars.h"
#import "ChallengersViewController.h"
#import "challengersTableViewCell.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "UIImageView+UIActivityIndicatorForSDWebImage.h"
#import "UserProfileViewController.h"
#import "Utility.h"
#import <UIKit/UIKit.h>
#import "OutBeat-swift.h"

@interface ChallengersViewController ()
{
    BackendlessUser *User;
    
    //LiquidLoader *liquidLoader ;
    UIView *grayView ;
}
@end

@implementation ChallengersViewController

- (void)viewDidLoad {
    
    
   
    
    self.challengersTableView.delegate =self;
    self.challengersTableView.dataSource =self;
    [super viewDidLoad];
    
    [self.view layoutIfNeeded];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.tabbarShadowView.bounds];
    self.tabbarShadowView.layer.masksToBounds = NO;
    self.tabbarShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabbarShadowView.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    self.tabbarShadowView.layer.shadowOpacity = 0.2f;
    self.tabbarShadowView.layer.shadowPath = shadowPath.CGPath;
     _lblTopChallengers.text=[NSString stringWithFormat:NSLocalizedString(@"top Challengers", nil)];
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:currentDate]; // Get necessary date components
    
    [components month]; //gives you month
    [components day]; //gives you day
    [components year]; // gives you year
    
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *someDate = [cal dateByAddingUnit:NSCalendarUnitMonth value:1 toDate:[NSDate date] options:0];
    NSDateComponents* componentsEnd = [cal components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:someDate]; // Get necessary date components

    
    
    NSString *start = [NSString stringWithFormat:@"%ld-%ld-%ld", (long)[components year] , (long)[components month] , (long)[components day]] ;//@"2010-09-01";
    
    
    
    NSString *end = [NSString stringWithFormat:@"%ld-%ld-01", (long)[componentsEnd year] , (long)[componentsEnd month] ] ;//@"2010-12-01";
    
    NSDateFormatter *f = [[NSDateFormatter alloc] init];
    [f setDateFormat:@"yyyy-MM-dd"];
    NSDate *startDate = [f dateFromString:start];
    NSDate *endDate = [f dateFromString:end];
    
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *componentsDifference = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:startDate
                                                          toDate:endDate
                                                         options:0];
    
    int hoursRemaining = [componentsDifference hour] % 24 ;
    
    
    NSString *nextRankingStr = [NSString stringWithFormat:NSLocalizedString(@"nextRankingIn", nil)]; ;
    
    
    self.nextRankingText.text = [NSString stringWithFormat:@"%@ %li days %i hours", nextRankingStr, (long)[componentsDifference day], hoursRemaining] ;
    
}

-(void)viewDidAppear:(BOOL)animated{
    /*if (!liquidLoader) {
        liquidLoader = [Utility createAndAdjustLiquidLoader:self.view];
        grayView = [Utility createGrayView] ;
    }*/
    [self fetchingUsersAsync];
}

-(void)viewWillAppear:(BOOL)animated {
    _usersArray = [[NSMutableArray alloc] init];
    [self.challengersTableView reloadData] ;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)tellUserProfileDelegateSomething:(NSObject*)something
{
    
}
-(void)fetchingUsersAsync {
    //[Utility showLoading:self.view andGreyBackground:grayView andLineLoader:liquidLoader];
    [SVProgressHUD show] ;
    self.usersArray=[[NSMutableArray alloc] init];
    [[backendless.persistenceService of:[BackendlessUser class]] find:nil response:^(BackendlessCollection *userColection) {
        NSLog(@" Users Found = %lu",(unsigned long)userColection.data.count);
        
        NSMutableArray *unsortedArray =[[NSMutableArray alloc] initWithArray:userColection.data];
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"starsThisMonth"
                                                     ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [unsortedArray sortedArrayUsingDescriptors:sortDescriptors];
        _usersArray=[sortedArray mutableCopy];
        
        
        
        [self.challengersTableView reloadData];
        
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        if (_usersArray.count == 0) {
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"No Users" andText:@"No Users Found.." andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
        }
    } error:^(Fault *error) {
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        NSString *errorMsg = [NSString stringWithFormat:@"Failed to Get Top Challengers. Details = %@", error.description];
        NSLog(@"Error Found at Top Challengers %@",error.detail);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _usersArray.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    static NSString *CellIdentifier = @"challengersCell";
    challengersTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[challengersTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:CellIdentifier];
    }
    [cell layoutIfNeeded] ;
    
    cell.likesTotal.text  = [NSString stringWithFormat:NSLocalizedString(@"Total", nil)];

    switch (indexPath.row) {
        case 0:
            cell.likesView.backgroundColor = [UIColor colorWithRed:253.0f/255.0f
                                                             green:173.0f/255.0f
                                                              blue:9.0f/255.0f
                                                             alpha:1.0f];
            cell.lineView.backgroundColor=[UIColor colorWithRed:245/255.0f
                                                          green:169/255.0f
                                                           blue:11.0f/255.0f
                                                          alpha:1.0f];
            cell.lblTotal.textColor=[UIColor whiteColor];
            cell.lblThisMonth.textColor=[UIColor whiteColor];
            cell.likesTotal.textColor=[UIColor whiteColor];
            cell.likesThisMonth.textColor=[UIColor whiteColor];
           
           
            break;
        case 1:
            cell.likesView.backgroundColor = [UIColor colorWithRed:139.0f/255.0f
                                                             green:139.0f/255.0f
                                                              blue:139.0f/255.0f
                                                             alpha:1.0f];
            cell.lineView.backgroundColor=[UIColor colorWithRed:138/255.0f
                                                          green:138/255.0f
                                                           blue:138/255.0f
                                                          alpha:1.0f];
            cell.lblTotal.textColor=[UIColor whiteColor];
            cell.lblThisMonth.textColor=[UIColor whiteColor];
            cell.likesTotal.textColor=[UIColor whiteColor];
            cell.likesThisMonth.textColor=[UIColor whiteColor];
            
            break ;
        case 2:
            cell.likesView.backgroundColor = [UIColor colorWithRed:184.0f/255.0f
                                                             green:96.0f/255.0f
                                                              blue:38.0f/255.0f
                                                             alpha:1.0f];
            cell.lineView.backgroundColor=[UIColor colorWithRed:175/255.0f
                                                         green:92/255.0f
                                                          blue:37/255.0f
                                                         alpha:1.0f];
            cell.lblTotal.textColor=[UIColor whiteColor];
            cell.lblThisMonth.textColor=[UIColor whiteColor];
            cell.likesTotal.textColor=[UIColor whiteColor];
            cell.likesThisMonth.textColor=[UIColor whiteColor];
            break;
            
        default:
            cell.likesView.backgroundColor = [UIColor colorWithRed:221.0f/255.0f
                                                             green:221.0f/255.0f
                                                              blue:221.0f/255.0f
                                                             alpha:1.0f];
            cell.lineView.backgroundColor=[UIColor colorWithRed:221.0f/255.0f
                                                          green:221.0f/255.0f
                                                           blue:221.0f/255.0f
                                                          alpha:1.0f];
            cell.lblTotal.textColor=[UIColor blackColor];
            cell.lblThisMonth.textColor=[UIColor blackColor];
            cell.likesTotal.textColor=[UIColor blackColor];
            cell.likesThisMonth.textColor=[UIColor blackColor];
    }
    
    
    
    User =[_usersArray objectAtIndex:indexPath.row];
    cell.labelUserName.text=User.name;
    cell.lableSerialNo.text=[NSString stringWithFormat:@"%ld",(long)indexPath.row + 1] ;
    
    //[cell.profileImage sd_setImageWithURL:[NSURL URLWithString:[User getProperty:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"] options:SDWebImageRefreshCached];
    
    
    //[cell.profileImage setImageWithURL:[NSURL URLWithString:[User getProperty:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"userAvatar"] options:SDWebImageRefreshCached usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    
    [cell.profileImage setImageWithURL:[NSURL URLWithString:[User getProperty:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"userAvatar"] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        if (!error) {
            dispatch_async(dispatch_get_main_queue(), ^{
                //Your main thread code goes in here
                cell.profileImage.image = image ;
            });
        }
    } usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
     
     
    cell.likesTotal.text=[NSString stringWithFormat:@"%@",[User getProperty:@"Stars"]];
    if (![User getProperty:@"Stars"]) {
        cell.likesTotal.text=[NSString stringWithFormat:@"%@",@"0"];
    }
    cell.likesThisMonth.text=[NSString stringWithFormat:@"%@",[User getProperty:@"starsThisMonth"]];
    if (![User getProperty:@"starsThisMonth"]) {
        cell.likesThisMonth.text=[NSString stringWithFormat:@"%@",@"0"];
    }
    cell.likesView.layer.cornerRadius=5;
    [cell.profileImage.layer setCornerRadius:cell.profileImage.frame.size.height/2];
    cell.profileImage.clipsToBounds=YES;
    cell.profileImage.layer.borderWidth=1.5;
    cell.profileImage.layer.borderColor=[[UIColor orangeColor] CGColor];
    

    
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    self.selectedUser=self.usersArray[indexPath.row];
    [self performSegueWithIdentifier:@"challengerToProfile" sender:self];
    
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"challengerToProfile"])
    {
        // Get reference to the destination view controller
        UserProfileViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        vc.selectedUser = self.selectedUser;
        vc.delegate=self;
        
        
    }
}
- (IBAction)unwindToChallengers:(UIStoryboardSegue*)sender
{
    
    
}
@end
