//
//  Friend_Requests.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/22/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
@interface Friend_Requests : NSObject

@property (nonatomic, strong) NSString *sender;
@property (nonatomic, strong) NSString *receiver;
@property(nonatomic) BOOL status;

@end






