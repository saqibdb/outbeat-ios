//
//  RequestCellTableViewCell.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/3/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RequestCellTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;

- (IBAction)confirmRequestAction:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *btnoutlet;
@property (weak, nonatomic) IBOutlet UIButton *ignoreBtn;


@end
