//
//  NeuerAccountViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/12/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"

#import "TTTAttributedLabel.h"

@interface NeuerAccountViewController : UIViewController  <UINavigationControllerDelegate, UIImagePickerControllerDelegate, UITextFieldDelegate, TTTAttributedLabelDelegate>

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *password;


@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;


@property (weak, nonatomic) IBOutlet UITextView *txtViewPrivacyPolicy;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIButton *profileImageBtn;

@property BOOL isBackable;

- (IBAction)registerAction:(id)sender;
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)imagePickerAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblNeuerAccount;

@property (weak, nonatomic) IBOutlet UIView *personalCredView;
@property (weak, nonatomic) IBOutlet UIView *credView;
@property (weak, nonatomic) IBOutlet UIButton *LOGINBTN;

@property (weak, nonatomic) IBOutlet UIView *avatarBGView;

@property (weak, nonatomic) IBOutlet UIButton *registrattionBtn;

@property (weak, nonatomic) IBOutlet UITextField *repasswordText;

- (IBAction)loginAction:(UIButton *)sender;


@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;

@property (weak, nonatomic) IBOutlet TTTAttributedLabel *agreementText;



- (IBAction)userConditionsAction:(id)sender;
- (IBAction)PrivacyPolicyAction:(id)sender;


@end

