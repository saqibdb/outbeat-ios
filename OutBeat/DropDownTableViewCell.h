//
//  DropDownTableViewCell.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 6/13/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DropDownTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *dropDownTxt;

@end
