//
//  FollowTableViewCell.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/7/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FollowTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIButton *followAction;
- (IBAction)followBtnAction:(id)sender;

@end
