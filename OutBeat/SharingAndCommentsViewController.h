//
//  SharingAndCommentsViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/15/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"
#import "MBCircularProgressBarView.h"
#import "VotersListViewController.h"
#import "SWTableViewCell.h"
#import "Challenge.h"
@interface SharingAndCommentsViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,SWTableViewCellDelegate , SFSafariViewControllerDelegate>
//
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *circulerRatingView;

//
@property (weak, nonatomic) IBOutlet UILabel *lblChellengersCount;


@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@property (weak, nonatomic) IBOutlet UITextField *txtCommentField;


@property (weak, nonatomic) IBOutlet UIImageView *tumbnailImage;



- (IBAction)playAction:(id)sender;

//
@property (weak, nonatomic) IBOutlet UIImageView *videoTypeIcon;



@property (weak, nonatomic) IBOutlet UIImageView *mainUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *mainUserName;


//
@property (weak, nonatomic) IBOutlet UILabel *mainChallengeType;
@property (strong,nonatomic) NSMutableArray *AllViews;
- (IBAction)GoToMakeBattle:(UIButton *)sender;



//videos view outlets
@property (weak, nonatomic) IBOutlet UIView *view_1;
@property (weak, nonatomic) IBOutlet UIView *view_2;
@property (weak, nonatomic) IBOutlet UIView *view_3;
@property (weak, nonatomic) IBOutlet UIView *view_4;
@property (weak, nonatomic) IBOutlet UIView *view_5;
@property (weak, nonatomic) IBOutlet UIView *view_6;
@property (weak, nonatomic) IBOutlet UIView *view_7;
@property (weak, nonatomic) IBOutlet UIView *view_8;
@property (weak, nonatomic) IBOutlet UIView *view_9;
@property (weak, nonatomic) IBOutlet UIView *view_10;
@property (weak, nonatomic) IBOutlet UIView *view_11;
@property (weak, nonatomic) IBOutlet UIView *view_12;

@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_1;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_2;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_3;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_4;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_5;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_6;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_7;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_8;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_9;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_10;
@property (weak, nonatomic) IBOutlet UIImageView *thumbnail_11;
@property (strong) Challenge  *selectedChallenge;

@property (weak, nonatomic) IBOutlet UILabel *challengeTitleLabel;




@property (weak, nonatomic) IBOutlet UILabel *lblViewers;



- (IBAction)playVideoAction_1:(UIButton *)sender;
- (IBAction)playVideoAction_2:(UIButton *)sender;

- (IBAction)playVideoAction_3:(UIButton *)sender;
- (IBAction)playVideoAction_4:(UIButton *)sender;

- (IBAction)playVideoAction_5:(UIButton *)sender;
- (IBAction)playVideoAction_6:(UIButton *)sender;
- (IBAction)playVideoAction_7:(UIButton *)sender;

- (IBAction)playVideoAction_8:(UIButton *)sender;
- (IBAction)playVideoAction_9:(UIButton *)sender;

- (IBAction)plusVideosAction:(UIButton *)sender;
- (IBAction)playVideoAction_10:(id)sender;
- (IBAction)playVideoAction_11:(id)sender;




- (IBAction)shareFbAction:(id)sender;
- (IBAction)shareTwitterAction:(id)sender;
- (IBAction)shareG:(id)sender;
- (IBAction)sharePintrestAction:(id)sender;
- (IBAction)shareMailAction:(id)sender;

//chellenger profil outlets
@property (weak, nonatomic) IBOutlet UIView *challengersProfileView;

@property (weak, nonatomic) IBOutlet UIImageView *challenger_1;
@property (weak, nonatomic) IBOutlet UIImageView *challenger_2;
@property (weak, nonatomic) IBOutlet UIImageView *challenger_3;
@property (weak, nonatomic) IBOutlet UIImageView *challenger_4;
@property (weak, nonatomic) IBOutlet UIImageView *challenger_5;

@property (weak, nonatomic) IBOutlet UIImageView *challenger_6;
@property (weak, nonatomic) IBOutlet UIButton *plusChallengersBtn;
@property (weak, nonatomic) IBOutlet UIButton *categoryBtn;







- (IBAction)plusChallengersAction:(id)sender;
- (IBAction)openChallengersList:(id)sender;
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *challengeNameText;

@property (strong,nonatomic) NSMutableArray *videosArray;
@property (strong,nonatomic) NSMutableArray *ChellengeVideosArray;
@property (strong,nonatomic) NSMutableArray *ChellengeVideosArray2;
@property (strong,nonatomic) NSMutableArray *allChellengersImages;
@property (strong,nonatomic) NSMutableArray *usersArray;
@property (strong,nonatomic) NSMutableArray *AllUsersImagesArray;
@property (strong,nonatomic) NSMutableArray *CommentsFound;
@property (strong,nonatomic) NSArray *chllengersList;
@property (weak, nonatomic) IBOutlet UIButton *plusUserBtn;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keyboardViewBottomConstraint;

- (IBAction)commentStarted:(id)sender;


- (IBAction)userProfileClickAction:(id)sender;


@end
