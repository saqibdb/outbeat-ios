//
//  ChallengeCommentsTableViewCell.h
//  OutBeat
//
//  Created by ibuildx on 8/19/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface ChallengeCommentsTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *userProfile;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *CommentBody;
@property (weak, nonatomic) IBOutlet UILabel *commentTime;

@end
