//
//  ProfileViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/11/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Video.h"
#import "VotersListViewController.h"
@interface ProfileViewController : UIViewController


@property (weak, nonatomic) IBOutlet UIImageView *circulerProfile;
@property (weak, nonatomic) IBOutlet UIImageView *blurImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeAddressLabel;

//friends
@property (weak, nonatomic) IBOutlet UIImageView *image_1;
@property (weak, nonatomic) IBOutlet UIImageView *image_2;
@property (weak, nonatomic) IBOutlet UIImageView *image_3;
@property (weak, nonatomic) IBOutlet UIImageView *image_4;
@property (weak, nonatomic) IBOutlet UIImageView *image_5;
@property (weak, nonatomic) IBOutlet UIImageView *image_6;
//followers
@property (weak, nonatomic) IBOutlet UIImageView *image_7;
@property (weak, nonatomic) IBOutlet UIImageView *image_8;
@property (weak, nonatomic) IBOutlet UIImageView *image_9;
@property (weak, nonatomic) IBOutlet UIImageView *image_10;
@property (weak, nonatomic) IBOutlet UIImageView *image_11;
@property (weak, nonatomic) IBOutlet UIImageView *image_12;

@property (weak, nonatomic) IBOutlet UIButton *PlusFriends;
@property (weak, nonatomic) IBOutlet UIButton *plusFollowerAction;

@property (weak, nonatomic) IBOutlet UIView *WBV_1;
@property (weak, nonatomic) IBOutlet UIView *WBV_2;
@property (weak, nonatomic) IBOutlet UIView *WBV_3;
@property (weak, nonatomic) IBOutlet UIView *WBV_4;
@property (weak, nonatomic) IBOutlet UIView *WBV_5;

@property (weak, nonatomic) IBOutlet UIView *LBV_1;
@property (weak, nonatomic) IBOutlet UIView *LBV_2;
@property (weak, nonatomic) IBOutlet UIView *LBV_3;
@property (weak, nonatomic) IBOutlet UIView *LBV_4;
@property (weak, nonatomic) IBOutlet UIView *LBV_5;

@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_1;
@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_2;
@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_3;
@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_4;
@property (weak, nonatomic) IBOutlet UIImageView *image_WBV_5;
@property (weak, nonatomic) IBOutlet UIView *btn_WBV;
@property (weak, nonatomic) IBOutlet UIButton *plus_btn_WBV;


@property (weak, nonatomic) IBOutlet UIButton *friendsRedirectsBtn;

@property (weak, nonatomic) IBOutlet UIButton *followersRedirectsBtn;







- (IBAction)playVideoAction:(id)sender;
- (IBAction)playVideoAction_2:(id)sender;
- (IBAction)playVideoAction_3:(id)sender;
- (IBAction)playVideoAction_4:(id)sender;
- (IBAction)playVideoAction_5:(id)sender;

- (IBAction)playVideoAction_6:(id)sender;
- (IBAction)playVideoAction_7:(id)sender;
- (IBAction)playVideoAction_8:(id)sender;
- (IBAction)playVideoAction_9:(id)sender;
- (IBAction)playVideoAction_10:(id)sender;






@property (strong) NSMutableArray *_usersArray;
@property (weak, nonatomic) IBOutlet UIButton *playVideoOutLet;

@property (strong) NSMutableArray *follwersArray;
@property (strong) NSMutableArray *winningVideosArray;
@property (strong) NSMutableArray *allFreindsCircles;
@property (strong) NSMutableArray *allFollwersCircles;
@property (strong) NSMutableArray *allVideoBox;



@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_1;
@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_2;
@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_3;
@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_4;
@property (weak, nonatomic) IBOutlet UIImageView *image_LBV_5;
@property (weak, nonatomic) IBOutlet UIView *btn_LBV;

- (IBAction)PlusFriendsAction:(id)sender;
- (IBAction)plusFollowerAction:(id)sender;

- (IBAction)addFollowersAction:(id)sender;
- (IBAction)addFriendsAction:(id)sender;

- (IBAction)MessageBtnAction:(UIButton *)sender;
- (IBAction)menueBtnAction:(UIButton *)sender;

@property (strong) NSMutableArray  *vidsFound;
@property (strong) NSMutableArray  *BattleVidsFound;

@property (strong) NSMutableArray  *lostVidsFound;
@property (strong) NSMutableArray  *wonVidsFound;
@property (strong) NSMutableArray  *AllVideosArray;
@property (strong) NSMutableArray  *ChellengeVideosArray;
@property (strong) NSMutableArray  *BattleVideosArray;
@property (strong) NSMutableArray *followrsArray;
@property (strong) NSMutableArray *friendArrayIds;

@property (strong) NSMutableArray *friendsArray;



@property (strong) Video *selectedVideo;



@property (weak, nonatomic) IBOutlet UILabel *winNumberTxt;
@property (weak, nonatomic) IBOutlet UILabel *lostNumberTxt;
@property (weak, nonatomic) IBOutlet UILabel *battlesNumberTxt;

@property (weak, nonatomic) IBOutlet UITextView *aboutMeText;

@property (weak, nonatomic) IBOutlet UIButton *messageBtn;



@property (weak, nonatomic) IBOutlet UIView *tabbarShadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblWins;
@property (weak, nonatomic) IBOutlet UILabel *lblLost;
@property (weak, nonatomic) IBOutlet UILabel *lblBattles;
@property (weak, nonatomic) IBOutlet UILabel *lblAbout;
@property (weak, nonatomic) IBOutlet UILabel *lblFriends;
@property (weak, nonatomic) IBOutlet UILabel *lblFollowers;
@property (weak, nonatomic) IBOutlet UILabel *lblWinningBattles;

@property (weak, nonatomic) IBOutlet UILabel *lblLostBattles;

- (IBAction)friendsRedirects:(id)sender;
- (IBAction)followersRedirects:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *followerBtn;

@property (weak, nonatomic) IBOutlet UIButton *winningBattleAllBtn;
@property (weak, nonatomic) IBOutlet UIButton *losingBattleAllBtn;

- (IBAction)plusWinningBattlesAction:(id)sender;

- (IBAction)plusLosingBattlesAction:(id)sender;



@end
