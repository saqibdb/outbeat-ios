//
//  NeuerAccountViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/12/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "NeuerAccountViewController.h"
#import "Backendless.h"
#import "profileInformation.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "ActionSheetPicker.h"
#import "LoginViewController.h"
#import "OutBeat-swift.h"
#import "Utility.h"
#import "NSMutableAttributedString+Color.h"
#import "TTTAttributedLabel.h"


@interface NeuerAccountViewController (){
    UIImage *selectedImage;
    //LiquidLoader *liquidLoader ;
    UIView *grayView ;
}

@end

@implementation NeuerAccountViewController

- (void)viewDidLoad {
    
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _lblNeuerAccount.text=[NSString stringWithFormat:NSLocalizedString(@"New Account", nil)];
    _txtFirstName.placeholder=[NSString stringWithFormat:NSLocalizedString(@"First Name", nil)];
    _txtLastName.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Last Name", nil)];
    _txtUserName.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Username", nil)];
    _txtEmail.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Email", nil)];
    _txtPassword.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Password", nil)];
    _txtViewPrivacyPolicy.text=[NSString stringWithFormat:NSLocalizedString(@"By registering you agree to our User Conditions and Privacy Policy", nil)];
     [_txtViewPrivacyPolicy  setFont: [UIFont fontWithName:@"Helvetica" size:9.0]];
    
    
    _LOGINBTN.layer.cornerRadius=17;
    _LOGINBTN.clipsToBounds=YES;
    
    
    _avatarBGView.layer.cornerRadius=60;
    _avatarBGView.clipsToBounds=YES;
    
    self.repasswordText.delegate = self;
    
    
    [self.view layoutIfNeeded] ;
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"Mit der Registrierung  akzeptierst du unsere Nutzerbedingungen und Datenschutzerklärung"];
    
    
    //[_agreementText addLinkToURL:[NSURL URLWithString:@"http://www.stackoverflow.com"] withRange:[_agreementText.text rangeOfString:@"Nutzerbedingungen"]];

    
    [string setColorForText:@"Nutzerbedingungen" withColor:[Utility getOrangeColor]];
    [string setColorForText:@"Datenschutzerklärung" withColor:[Utility getOrangeColor]];
    _agreementText.attributedText = string;
    
    //[_agreementText addLinkToURL:[NSURL URLWithString:@"http://www.stackoverflow.com"] withRange:[_agreementText.text rangeOfString:@"Datenschutzerklärung"]];

   // _agreementText.delegate = self ;
}

#pragma mark - TTTAttributedLabelDelegate

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url {
    // for handling the URL but we just call our action
    [self userHasClickedTextInLabel];
}

-(void)userHasClickedTextInLabel {
    NSLog(@"Clicked here") ;
}

-(void)viewDidAppear:(BOOL)animated{
    /*if (!liquidLoader) {
        liquidLoader = [Utility createAndAdjustLiquidLoader:self.view];
        grayView = [Utility createGrayView] ;
    }*/
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)saveNewContact
{
    if (![_txtPassword.text isEqualToString:_repasswordText.text]) {
        NSString *error = @"Password should be match";
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
    }
    else
    {
        [SVProgressHUD showWithStatus:@"Registering User"];
        BackendlessUser *user = [BackendlessUser new];

        [user setProperty:@"name" object:_txtUserName.text];
        [user setProperty:@"email" object:_txtEmail.text];
        [user setProperty:@"isNotSocial" object:_txtEmail.text];
        [user setProperty:@"password" object:_txtPassword.text];
        
        [user setProperty:@"firstName" object:_txtFirstName.text];
        [user setProperty:@"lastName" object:_txtLastName.text];
        [user setProperty:@"copyPassword" object:_txtPassword.text];
        
        [backendless.userService registering:user response:^(BackendlessUser *userRegistered) {
            NSLog(@"Object Id Will be = %@", userRegistered.objectId);
            
            [self uploadAsyncWithImageName:userRegistered.objectId andNewRegistereddUser:userRegistered];
            
        } error:^(Fault *error) {
            NSLog(@"Error At Register %@", error.description);
            NSString *errorMsg = [NSString stringWithFormat:@"Error At Register. Details = %@", error.description];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
            
            [alert show];
            
            [SVProgressHUD dismiss];
            
        }];
    }
}
-(void)loginWithUser :(BackendlessUser *)loginUser
{
    [loginUser setProperty:@"password" object:_txtPassword.text];
    
    [backendless.userService login:loginUser.email password:loginUser.password response:^(BackendlessUser *userLogged) {
        UIDevice *device = [UIDevice currentDevice];
        NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
        [backendless.userService setStayLoggedIn:YES] ;

        
        [userLogged setProperty:@"deviceId" object:currentDeviceId];
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore save:userLogged response:^(id result) {
            
        } error:^(Fault *error) {
            NSLog(@"Error at deviceId %@",error.description);
        }];
        
        
        
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"NewAccountToTabBar" sender:self];
    } error:^(Fault *error) {
        
        [SVProgressHUD dismiss];
        
        /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",error] andCancelButton:NO forAlertType:AlertInfo];
        [alert show];*/
    }];
}


-(void)updateImageUrl:(BackendlessUser *)imageUpdateUser :(NSString *)newUrl
{

    [imageUpdateUser setProperty:@"profileImage" object:[NSString stringWithFormat:@"%@", newUrl]];
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore save:imageUpdateUser response:^(id result) {
        [SVProgressHUD dismiss];
        [self loginWithUser:imageUpdateUser];

    } error:^(Fault *error) {
        NSLog(@"Error At imageUrl Update %@", error.description);
        NSString *errorMsg = [NSString stringWithFormat:@"Error At imageUrl Update. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];

        
        [SVProgressHUD dismiss];

    }];
}

-(void)uploadAsyncWithImageName :(NSString *)imageName andNewRegistereddUser :(BackendlessUser *)newRegisteredUser {
    
    NSLog(@"\n============ Uploading file  with the ASYNC API ============");
    
    
    int REQUIRED_SIZE = 100;
    if (selectedImage == nil) {
        selectedImage = [UIImage imageNamed:@"userAvatar"];
    }
    selectedImage = [self getScaledToRequiredSizedImage:REQUIRED_SIZE :selectedImage];
    
    NSData *imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
    
    [backendless.fileService upload:[NSString stringWithFormat:@"myfiles/%@.jpg", imageName]  content:imageData
                           response:^(BackendlessFile *uploadedFile) {
                               NSLog(@"File has been uploaded. File URL is - %@", uploadedFile.fileURL);
                               
                               [self updateImageUrl:newRegisteredUser :uploadedFile.fileURL];
                               
                           }
                              error:^(Fault *fault) {
                                  NSLog(@"Server reported an error: %@", fault);
                                  
                                  NSString *errorMsg = [NSString stringWithFormat:@"Server reported an error. Details = %@", fault.description];
                                  NSLog(@"FAULT = %@ <%@>", fault.message, fault.detail);
                                  AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
                                  
                                  [alert show];

                                  [SVProgressHUD dismiss];

                              }];
}

-(UIImage *)getScaledToRequiredSizedImage :(int)requiredSize :(UIImage *)sentImage{
    int scale = 1;
    
    while (sentImage.size.width / scale / 2 >= requiredSize
           && sentImage.size.height / scale / 2 >= requiredSize)
        scale *= 2;
    
    return [self imageWithImage:selectedImage scaledToSize:CGSizeMake(selectedImage.size.width/scale, selectedImage.size.height/scale)];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}
#pragma mark - responder
-(id)responseHandler:(id)response
{
    NSLog(@"%@", response);
    return response;
}
-(id)errorHandler:(Fault *)fault
{
    NSLog(@"%@", fault.detail);
    return fault;
}


- (IBAction)registerAction:(id)sender {
    
    [self saveNewContact];
}

- (IBAction)backAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"backToGetStarted" sender:self];
}

- (IBAction)imagePickerAction:(UIButton *)sender {
    NSArray *colors = [NSArray arrayWithObjects:@"Gallery" , @"Camera", nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Image Source."
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                          
                                           if (selectedIndex == 1) {
                                               [self showImagePicker:UIImagePickerControllerSourceTypeCamera sender:nil];

                                           }
                                           else{
                                               [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary sender:nil];

                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
    
}


#pragma mark Image Picker And Delegates

- (void)showImagePicker:(UIImagePickerControllerSourceType)sourceType sender:(id)sender {
    
    UIImagePickerController *controler = [[UIImagePickerController alloc] init];
    if (![UIImagePickerController isSourceTypeAvailable:sourceType]) {
        /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Permission Error" andText:@"Permission not granted"  andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];*/
        
        //loder remove
        return;
    }
    controler.sourceType = sourceType;
    controler.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        
        [self presentViewController:controler animated:YES completion:nil];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Picker returned successfully.");
    
    NSURL *mediaUrl;
    mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    if (mediaUrl == nil) {
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil) {
            selectedImage= (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
            NSLog(@"Original image picked.");
            selectedImage = [self scaleAndRotateImage:selectedImage];
            //[self.profileImageBtn setImage:selectedImage forState:UIControlStateNormal];
            
            [self.avatarImage setImage:selectedImage];
        }
        else {
            NSLog(@"Edited image picked.");
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image url is : %@",[mediaUrl absoluteString]);
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


- (IBAction)loginAction:(UIButton *)sender {
    if (self.isBackable) {
        [self performSegueWithIdentifier:@"BackToLogin" sender:self] ;
    }
    else{
        [self performSegueWithIdentifier:@"ToLogin" sender:self] ;
    }
}

- (IBAction)userConditionsAction:(id)sender {
    [self performSegueWithIdentifier:@"toPrivacyAndUserConditions" sender:self];
}

- (IBAction)PrivacyPolicyAction:(id)sender {
    [self performSegueWithIdentifier:@"toPrivacyAndUserConditions" sender:self];
}
- (IBAction)unwindToNeuerAccount:(UIStoryboardSegue*)sender
{
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToLogin"]) {
        LoginViewController *vc = segue.destinationViewController ;
        vc.isBackable = YES;
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString * proposedNewString = [[textField text] stringByReplacingCharactersInRange:range withString:string];

    if ([self.txtPassword.text isEqualToString:proposedNewString]) {
        [self.registrattionBtn setBackgroundColor:[UIColor colorWithRed:251.0/255.0 green:102.0/255.0 blue:68.0/255.0 alpha:1.0]];
    }
    else{
        [self.registrattionBtn setBackgroundColor:[UIColor lightGrayColor]];
        
    }
    
    
    return YES;
}

@end
