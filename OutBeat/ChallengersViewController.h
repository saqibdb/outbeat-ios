//
//  ChallengersViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/30/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"
#import "UserProfileViewController.h"
@interface ChallengersViewController : UIViewController <UITableViewDelegate , UITableViewDataSource,UserProfileViewDelegate>


@property (strong) NSMutableArray *usersArray;
@property (weak, nonatomic) IBOutlet UITableView *challengersTableView;


@property (weak, nonatomic) IBOutlet UIView *tabbarShadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblTopChallengers;
@property (weak, nonatomic) IBOutlet BackendlessUser *selectedUser;


@property (weak, nonatomic) IBOutlet UILabel *nextRankingText;


@end
