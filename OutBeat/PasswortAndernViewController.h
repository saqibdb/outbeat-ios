//
//  PasswortAndernViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/11/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PasswortAndernViewController : UIViewController<UITextFieldDelegate>
- (IBAction)backAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtConfirmPassword;

- (IBAction)changePasswordAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblPasswordChange;
@property (weak, nonatomic) IBOutlet UIView *passwordFieldsView;
@property (weak, nonatomic) IBOutlet UIButton *changeBtn;

@end
