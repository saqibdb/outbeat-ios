//
//  CallUsPopUpView.h
//  Basiligo
//
//  Created by ibuildx on 1/29/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ForgetEmailPopUp : UIViewController


@property (weak, nonatomic) IBOutlet UIView *borderView;

@property (weak, nonatomic) IBOutlet UITextField *phoneTxt;
@property (weak, nonatomic) IBOutlet UIButton *shareBtn;

- (IBAction)shareAction:(UIButton *)sender;



@end
