//
//  SQBUtility.m
//  outbeat
//
//  Created by iBuildx_Mac_Mini on 12/7/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "SQBUtility.h"
#import "Friends.h"
#import "Follower.h"
#import "Inbox.h"

@implementation SQBUtility

+(void)getAllFrirendsOfUser :(BackendlessUser *)user responseBlock: (SQBResponseBlock)responseBlock {
    NSMutableArray *allFriends = [user getProperty:@"friends"] ;
    if (!allFriends.count) {
        responseBlock(nil, NO, nil);
        return ;
    }
    NSMutableArray *friendArrayIds =[[NSMutableArray alloc]init] ;
    for (Friends *friend in allFriends) {
        if ([friend.RequestStatus isEqualToString:@"Accepted"]) {
            if (![user.objectId isEqualToString:friend.FriendUserId]) {
                NSString *idStr = [NSString stringWithFormat:@"'%@'",friend.FriendUserId];
                [friendArrayIds addObject:idStr] ;
            }
            else{
                NSString *idStr = [NSString stringWithFormat:@"'%@'",friend.SendingUserId];
                [friendArrayIds addObject:idStr] ;
            }
        }
    }
    
    if (!friendArrayIds.count) {
        NSLog(@"No Friend in your friends list") ;
    }
    else {
        NSString *friendsIdStr = [friendArrayIds componentsJoinedByString:@","];
        BackendlessDataQuery *query = [BackendlessDataQuery query];
        query.whereClause = [NSString stringWithFormat:@"objectId IN (%@)",friendsIdStr];
        [backendless.persistenceService find:[BackendlessUser class] dataQuery:query response:^(BackendlessCollection *friendsUsers) {
            NSMutableArray *friendsArray = [[NSMutableArray alloc] initWithArray:friendsUsers.data] ;
            responseBlock(friendsArray, YES, nil);
        } error:^(Fault *error) {
            NSLog(@"Error at friends fetch %@" , error.detail ) ;
            responseBlock(nil, NO, error);
        }] ;
    }
}
+(void)getAllFollowersOfUser :(BackendlessUser *)user responseBlock: (SQBResponseBlock)responseBlock {
    NSMutableArray *allFollowers = [user getProperty:@"followers"] ;
    if (!allFollowers.count) {
        responseBlock(nil, NO, nil);
        return ;
    }
    NSMutableArray *followersArrayIds =[[NSMutableArray alloc]init] ;
    for (Follower *follower in allFollowers) {
        if (![user.objectId isEqualToString:follower.FollowerUserId]) {
            NSString *idStr = [NSString stringWithFormat:@"'%@'",follower.FollowerUserId];
            [followersArrayIds addObject:idStr] ;
        }
        else{
            NSString *idStr = [NSString stringWithFormat:@"'%@'",follower.SendingUserId];
            [followersArrayIds addObject:idStr] ;
        }
    }
    
    if (!followersArrayIds.count) {
        NSLog(@"No follower in your followers list") ;
    }
    else {
        NSString *followersIdStr = [followersArrayIds componentsJoinedByString:@","];
        BackendlessDataQuery *query = [BackendlessDataQuery query];
        query.whereClause = [NSString stringWithFormat:@"objectId IN (%@)",followersIdStr];
        [backendless.persistenceService find:[BackendlessUser class] dataQuery:query response:^(BackendlessCollection *followersUsers) {
            NSMutableArray *followersArray = [[NSMutableArray alloc] initWithArray:followersUsers.data] ;
            responseBlock(followersArray, YES, nil);
        } error:^(Fault *error) {
            NSLog(@"Error at followers fetch %@" , error.detail ) ;
            responseBlock(nil, NO, error);
        }] ;
    }
}
+(void)fetchingUsersInboxHistoryForUser :(BackendlessUser *)user andOtherUser :(BackendlessUser *)otherUser responseBlock: (SQBResponseBlock)responseBlock {
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"(Reciever.objectId = '%@' AND Sender.objectId = '%@') OR (Reciever.objectId = '%@' AND Sender.objectId = '%@')", user.objectId, otherUser.objectId , otherUser.objectId , user.objectId];
    [backendless.persistenceService find:[Inbox class] dataQuery:query response:^(BackendlessCollection *collection1)  {
        NSLog(@"Inbox Found %i", [[collection1 getTotalObjects] intValue]);
        if (collection1.data.count) {
            responseBlock(collection1.data[0] , YES , nil) ;
        }
        else{
            responseBlock(nil , NO , nil) ;
        }
    } error:^(Fault *error) {
        responseBlock(nil , NO , error) ;
    }];
    
}

+(void)refreshCurrentUserWithresponseBlock: (SQBResponseBlock)responseBlock {
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore findID:backendless.userService.currentUser.objectId response:^(id obj) {
        
        BackendlessUser *updatedUser = obj;
        
        NSString *profileImage = [backendless.userService.currentUser getProperty:@"profileImage"];
        NSString *firstName = [backendless.userService.currentUser getProperty:@"firstName"];
        NSString *homeAddress = [backendless.userService.currentUser getProperty:@"homeAddress"];
        NSString *aboutMe = [backendless.userService.currentUser getProperty:@"aboutMe"];
        NSMutableArray *allFriends = [backendless.userService.currentUser getProperty:@"friends"] ;
        NSMutableArray *allFollowers = [backendless.userService.currentUser getProperty:@"followers"] ;
        
        NSString *updatedprofileImage = [updatedUser getProperty:@"profileImage"];
        NSString *updatedfirstName = [updatedUser getProperty:@"firstName"];
        NSString *updatedhomeAddress = [updatedUser getProperty:@"homeAddress"];
        NSString *updatedaboutMe = [updatedUser getProperty:@"aboutMe"];
        NSMutableArray *updatedallFriends = [updatedUser getProperty:@"friends"] ;
        NSMutableArray *updatedallFollowers = [updatedUser getProperty:@"followers"] ;
        
        
        if (![profileImage isEqualToString:updatedprofileImage] || ![firstName isEqualToString:updatedfirstName] || ![homeAddress isEqualToString:updatedhomeAddress] || ![aboutMe isEqualToString:updatedaboutMe] || (allFriends.count !=updatedallFriends.count) || (allFollowers.count !=updatedallFollowers.count))
        {
            backendless.userService.currentUser = obj;
            responseBlock(obj , YES , nil) ;
        }
        responseBlock(obj , NO , nil) ;
        
    } error:^(Fault *error) {
        responseBlock(nil , NO , error) ;
    }];
}


@end
