//
//  ClosedBattlesCollectionViewCell.h
//  outbeat
//
//  Created by iBuildx_Mac_Mini on 12/28/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ClosedBattlesCollectionViewCell : UICollectionViewCell


@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;
@property (weak, nonatomic) IBOutlet UILabel *videoUserName;
@property (weak, nonatomic) IBOutlet UIImageView *videoUserProfile;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UIButton *palyBtn;



@end
