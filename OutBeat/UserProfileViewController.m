//
//  UserProfileViewController.m
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 8/9/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "UserProfileViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Friends.h"
#import "UIImageView+WebCache.h"
#import "VideoPlayViewController.h"
#import "Follower.h"
#import "RKNotificationHub.h"
#import "Inbox.h"
#import "Utility.h"
#import "Follower.h"
#import "Friends.h"
#import "SQBUtility.h"
#import "ChatViewController.h"
#import "SQBUtility.h"


@interface UserProfileViewController (){
    NSMutableArray *followrsArray;
    NSMutableArray *friendArray;
    NSMutableArray *UsersArray;
    NSMutableArray *allUsersArray;
    NSMutableArray *friendRequestArray;
    UIVisualEffectView *visualEffectView;
    RKNotificationHub *hub ;
    Follower *selectedFollower ;
    Friends *selectedFriend ;
}

@end

@implementation UserProfileViewController

-(void) checkFollower {
    for (Follower *follower in [backendless.userService.currentUser getProperty:@"following"]) {
        if ([follower.FollowerUserId isEqualToString:self.selectedUser.objectId]) {
            [self.followerBtn setBackgroundImage:[UIImage imageNamed:@"UnfollowIcon"] forState:UIControlStateNormal] ;
            selectedFollower = follower ;
            break ;
        }
        else{
            [self.followerBtn setBackgroundImage:[UIImage imageNamed:@"followIcon"] forState:UIControlStateNormal] ;
        }
    }
}
-(void) checkFriends {
    NSMutableArray *friends = [backendless.userService.currentUser getProperty:@"friends"];
    if (!friends.count) {
        [self.friendBtn setBackgroundImage:[UIImage imageNamed:@"add_friend"] forState:UIControlStateNormal] ;
        self.messageBtn.hidden = YES;
    }
    for (Friends *friend in friends) {
        if (([friend.FriendUserId isEqualToString:self.selectedUser.objectId] && [friend.SendingUserId isEqualToString:backendless.userService.currentUser.objectId]) || ([friend.SendingUserId isEqualToString:self.selectedUser.objectId] && [friend.FriendUserId isEqualToString:backendless.userService.currentUser.objectId])) {
 
            if ([friend.RequestStatus isEqualToString:@"Accepted"]) {
                [self.friendBtn setBackgroundImage:[UIImage imageNamed:@"remove_friend_Yellow"] forState:UIControlStateNormal] ;
                self.messageBtn.hidden = NO;
            }
            else{
                [self.friendBtn setBackgroundImage:[UIImage imageNamed:@"add_friend_Yellow"] forState:UIControlStateNormal] ;
                self.messageBtn.hidden = YES;

            }
            selectedFriend = friend ;
            break ;
        }
        else{
            [self.friendBtn setBackgroundImage:[UIImage imageNamed:@"add_friend"] forState:UIControlStateNormal] ;
            self.messageBtn.hidden = YES;
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    [_circulerProfile.layer setCornerRadius:_circulerProfile.frame.size.height/2];
    _circulerProfile.clipsToBounds=YES;
    _circulerProfile.layer.borderWidth=2.0;
    _circulerProfile.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    //friends circuler images
    
    [_PlusFriends.layer setCornerRadius:_PlusFriends.frame.size.height/2];
    _PlusFriends.clipsToBounds=YES;
    
    [_image_1.layer setCornerRadius:_image_1.frame.size.height/2];
    _image_1.clipsToBounds=YES;
    _image_1.layer.borderWidth=1.5;
    _image_1.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_2.layer setCornerRadius:_image_2.frame.size.height/2];
    _image_2.clipsToBounds=YES;
    _image_2.layer.borderWidth=1.5;
    _image_2.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_3.layer setCornerRadius:_image_3.frame.size.height/2];
    _image_3.clipsToBounds=YES;
    _image_3.layer.borderWidth=1.5;
    _image_3.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_4.layer setCornerRadius:_image_4.frame.size.height/2];
    _image_4.clipsToBounds=YES;
    _image_4.layer.borderWidth=1.5;
    _image_4.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_5.layer setCornerRadius:_image_5.frame.size.height/2];
    _image_5.clipsToBounds=YES;
    _image_5.layer.borderWidth=1.5;
    _image_5.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_6.layer setCornerRadius:_image_6.frame.size.height/2];
    _image_6.clipsToBounds=YES;
    _image_6.layer.borderWidth=1.5;
    _image_6.layer.borderColor=[[UIColor orangeColor] CGColor];
    // End of  friends circuler images
    
    //followers circuler images
    
    [_image_7.layer setCornerRadius:_image_7.frame.size.height/2];
    _image_7.clipsToBounds=YES;
    _image_7.layer.borderWidth=1.5;
    _image_7.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_8.layer setCornerRadius:_image_8.frame.size.height/2];
    _image_8.clipsToBounds=YES;
    _image_8.layer.borderWidth=1.5;
    _image_8.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_9.layer setCornerRadius:_image_9.frame.size.height/2];
    _image_9.clipsToBounds=YES;
    _image_9.layer.borderWidth=1.5;
    _image_9.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_10.layer setCornerRadius:_image_10.frame.size.height/2];
    _image_10.clipsToBounds=YES;
    _image_10.layer.borderWidth=1.5;
    _image_10.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_11.layer setCornerRadius:_image_11.frame.size.height/2];
    _image_11.clipsToBounds=YES;
    _image_11.layer.borderWidth=1.5;
    _image_11.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_12.layer setCornerRadius:_image_12.frame.size.height/2];
    _image_12.clipsToBounds=YES;
    _image_12.layer.borderWidth=1.5;
    _image_12.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_plusFollowerAction.layer setCornerRadius:_PlusFriends.frame.size.height/2];
    _PlusFriends.clipsToBounds=YES;
    
    
    [_followerBtn.layer setCornerRadius:_followerBtn.frame.size.height/2];
    _followerBtn.clipsToBounds=YES;
    [_followerBtn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]] ;
    
    
    [_friendBtn.layer setCornerRadius:_friendBtn.frame.size.height/2];
    _friendBtn.clipsToBounds=YES;
    
    
    // End of  followers circuler images
    
    
    [_circulerProfile sd_setImageWithURL:[NSURL URLWithString:[self.selectedUser getProperty:@"profileImage"]]
                        placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                 options:SDWebImageRefreshCached];
    
    
    
    [_blurImageView sd_setImageWithURL:[NSURL URLWithString:[self.selectedUser getProperty:@"profileImage"]]
                      placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                               options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   
                                   UIVisualEffect *blurEffect;
                                   blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
                                   [visualEffectView removeFromSuperview];
                                   visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                                   
                                   visualEffectView.frame = _blurImageView.bounds;
                                   [_blurImageView addSubview:visualEffectView];
                               }];
    
    
    
    self.userNameLabel.text = [self.selectedUser getProperty:@"firstName"];
    
    self.homeAddressLabel.text = [self.selectedUser getProperty:@"homeAddress"];
    self.aboutMeText.text = [self.selectedUser getProperty:@"aboutMe"];
    
    
    friendArray =[[NSMutableArray alloc]initWithArray:[self.selectedUser getProperty:@"friends"]] ;
    followrsArray =[[NSMutableArray alloc]initWithArray:[self.selectedUser getProperty:@"follower"]] ;
    
    // localization strings
    _lblAbout.text=[NSString stringWithFormat:NSLocalizedString(@"about", nil)];
    _lblWins.text=[NSString stringWithFormat:NSLocalizedString(@"wins", nil)];
    _lblLost.text=[NSString stringWithFormat:NSLocalizedString(@"Lost", nil)];
    _lblBattles.text=[NSString stringWithFormat:NSLocalizedString(@"Battles", nil)];
    _lblFriends.text=[NSString stringWithFormat:NSLocalizedString(@"Friends", nil)];
    _lblFollowers.text=[NSString stringWithFormat:NSLocalizedString(@"followers", nil)];
    _lblWinningbattles.text=[NSString stringWithFormat:NSLocalizedString(@"winning battles", nil)];
    _lblLostBattles.text=[NSString stringWithFormat:NSLocalizedString(@"lost Battles", nil)];
    
    if ([self.selectedUser.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
        [self.followerBtn setHidden:YES];
        [self.friendBtn setHidden:YES];
        [self.messageBtn setHidden:YES];
    }
    else{
        [self.followerBtn setHidden:NO];
        [self.friendBtn setHidden:NO];
        [self.messageBtn setHidden:NO];
    }
   
    
}

-(void)viewWillAppear:(BOOL)animated{
    _allFreindsCircles = [[NSMutableArray alloc] initWithObjects:_image_1,
                          _image_2,
                          _image_3,
                          _image_4,
                          _image_5,
                          _image_6, nil] ;
    
    _allFollwersCircles = [[NSMutableArray alloc] initWithObjects:_image_7,
                           _image_8,
                           _image_9,
                           _image_10,
                           _image_11,
                           _image_12, nil] ;
    _allVideoBox = [[NSMutableArray alloc] initWithObjects:
                    _image_WBV_1,
                    _image_WBV_2,
                    _image_WBV_3,
                    _image_WBV_4,
                    _image_WBV_5, nil] ;
    
    [self hideObjects];
    [self fetchVideos];
    //[self updateFriendsCircle];
    //[self updateFollwersCircle];
    
    
    [self.view layoutIfNeeded];
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.tabbarShadowView.bounds];
    self.tabbarShadowView.layer.masksToBounds = NO;
    self.tabbarShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabbarShadowView.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    self.tabbarShadowView.layer.shadowOpacity = 0.2f;
    self.tabbarShadowView.layer.shadowPath = shadowPath.CGPath;
    
    
    
    [self setupFunctionsCall];
    
    [SQBUtility refreshCurrentUserWithresponseBlock:^(id object, BOOL status, Fault *error) {
        if (status) {
            
            [self setupFunctionsCall];
        }
        else if(error){
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Try Again Later." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
    }];
}

-(void)setupFunctionsCall {
    
    [self checkFollower] ;
    [self checkFriends] ;
    [self updateFriendsCircle];
    [self updateFollwersCircle];
}

-(void)viewDidAppear:(BOOL)animated {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - Actions
- (IBAction)menueBtnAction:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(tellUserProfileDelegateSomething:)])
    {
        [self performSegueWithIdentifier:@"profileToChallengersBack" sender:self];
    }

    else{
        if ([self.previousController isEqualToString:@"SharingAndCommentsViewController"]) {
            [self performSegueWithIdentifier:@"backToSharing" sender:self];
        }
        else if([self.previousController isEqualToString:@"VoteViewController"]){
            [self performSegueWithIdentifier:@"backToVotes" sender:self];
        }
        else{
            [self performSegueWithIdentifier:@"userProfileToList" sender:self];
        }
    }
    
    
    
}

- (IBAction)PlusFriendsAction:(id)sender {
    
}

- (IBAction)plusFollowerAction:(id)sender {
    
}



- (IBAction)addFriendsAction:(id)sender {
    
    if (selectedFriend) {
        if ([selectedFriend.RequestStatus isEqualToString:@"Accepted"]) {
            
            

            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Confirmation" andText:@"Are you sure, you want to Remove this Friend?" andCancelButton:YES forAlertType:AlertInfo andColor:[UIColor purpleColor] withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                if(btton == alertView.defaultButton) {
                    NSLog(@"Default");
                    
                    [alert dismissAlertView];
                   
                    [SVProgressHUD showWithStatus:@"Removing Friend ..."];

                    [backendless.persistenceService remove:[Friends class] sid:selectedFriend.objectId response:^(NSNumber *num) {
                        [SVProgressHUD dismiss];
                        
                        NSMutableArray *friendsArray = [backendless.userService.currentUser getProperty:@"friends"];
                        [friendsArray removeObject:selectedFriend];
                        [backendless.userService.currentUser setProperty:@"friends" object:friendsArray];
                        [backendless.persistenceService save:backendless.userService.currentUser response:^(id obj) {
                            backendless.userService.currentUser = obj;
                        } error:^(Fault *error) {
                            
                        }];
                        
                        
                        selectedFriend = nil;
                        [self.friendBtn setBackgroundImage:[UIImage imageNamed:@"add_friend"] forState:UIControlStateNormal] ;

                        self.messageBtn.hidden = YES;
                        
                    } error:^(Fault *error) {
                        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Error Removing Friend." andCancelButton:NO forAlertType:AlertFailure];
                        [alert show];
                        [SVProgressHUD dismiss];
                    }];

                    
                }
                else {
                    NSLog(@"Others");
                    
                }
            }];
            
            [alert show];
            
            

            /*
             [self performSegueWithIdentifier:@"OtherUserToChat" sender:self];
            [SQBUtility fetchingUsersInboxHistoryForUser:backendless.userService.currentUser andOtherUser:self.selectedUser responseBlock:^(id object, BOOL status, Fault *error) {
                if (status) {
                    self.selectedInbox = (Inbox *)object ;
                    
                    
                    
                }
                else{
                    if (error) {
                        NSLog(@"Error at fetching Inbox");
                    }
                    else{
                        NSLog(@"Empty Inbox");
                    }
                }
            }];
            */
            
            
        }
        else{
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Friend Request" andText:@"Your Friend Request has not yet been Accepted by Other User." andCancelButton:NO forAlertType:AlertInfo];
            [alert show];

        }
    }
    else{
        [SVProgressHUD showWithStatus:@"Sending Request..."];
        BackendlessUser *selectedUser = self.selectedUser ;
        Friends *newFriendRequest = [[Friends alloc] init];
        newFriendRequest.FriendUserId = [selectedUser getProperty:@"objectId"] ;
        newFriendRequest.RequestStatus = @"NotAccepted" ;
        newFriendRequest.SendingUserId = [backendless.userService.currentUser getProperty:@"objectId"] ;
        
        BackendlessUser *currentUser = backendless.userService.currentUser ;
        NSMutableArray *newFriendsList = [[NSMutableArray alloc] initWithArray:[currentUser getProperty:@"friends"]] ;
        [newFriendsList addObject:newFriendRequest] ;
        [currentUser setProperty:@"friends" object:newFriendsList] ;
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore save:currentUser response:^(id savedUser) {
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Friend request sent" andText:@"Friend Request has been Successfuly sent." andCancelButton:NO forAlertType:AlertSuccess];
            [alert show];
            
            
            backendless.userService.currentUser = savedUser ;
            [self checkFriends] ;
            NSString *friendRequestMessage = [NSString stringWithFormat:@"%@ sent you a Friend Request and Waiting for you Response.", backendless.userService.currentUser.name];
            [Utility publishMessageAsPushNotificationAsync:friendRequestMessage forDevice:[selectedUser getProperty:@"deviceId"]] ;
        } error:^(Fault *error) {
            [SVProgressHUD dismiss];
            NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
            
            [alert show];
        }] ;
    }
    
    
    
    //[self performSegueWithIdentifier:@"profileToFriends" sender:self];
}
- (IBAction)addFollowersAction:(id)sender {
    if (selectedFollower) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Confirm!!!" andText:@"Do You want to Unfollow?" andCancelButton:YES forAlertType:AlertInfo];
        [alert.defaultButton setTitle:@"Yes" forState:UIControlStateNormal] ;
        [alert show];
        alert.completionBlock = ^void (AMSmoothAlertView *alertObj, UIButton *button) {
            if(button == alertObj.defaultButton) {
                NSLog(@"Default");
                id<IDataStore> dataStore = [backendless.persistenceService of:[Follower class]];
                
                [dataStore remove:selectedFollower response:^(NSNumber *num) {
                    [self.followerBtn setBackgroundImage:[UIImage imageNamed:@"followIcon"] forState:UIControlStateNormal] ;
                    
                    NSMutableArray *followings = [backendless.userService.currentUser getProperty:@"following"] ;
                    [followings removeObject:selectedFollower] ;
                    [backendless.userService.currentUser setProperty:@"following" object:followings] ;
                    
                    selectedFollower = nil ;
                    
                    
                    
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"You are now UnFollowing this User." andCancelButton:NO forAlertType:AlertSuccess];
                    [alert show];
                } error:^(Fault *error) {
                    NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.detail];
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
                    [alert show];
                }];
            } else {
                NSLog(@"Others");
            }
        };
    }
    else{
        [SVProgressHUD showWithStatus:@"Saving Follower..."];
        BackendlessUser *currentUser = backendless.userService.currentUser  ;
        Follower *newFollowing = [[Follower alloc] init];
        newFollowing.FollowerUserId = [self.selectedUser getProperty:@"objectId"] ;
        newFollowing.SendingUserId = [currentUser getProperty:@"objectId"] ;
        NSMutableArray *newFollowingList = [[NSMutableArray alloc] initWithArray:[currentUser getProperty:@"following"]] ;
        [newFollowingList addObject:newFollowing] ;
        [currentUser setProperty:@"following" object:newFollowingList] ;
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore save:currentUser response:^(id savedUser) {
            //[self.followerBtn setBackgroundImage:[UIImage imageNamed:@"UnfollowIcon"] forState:UIControlStateNormal] ;

            
            [SVProgressHUD dismiss];
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Following" andText:@"You are now Following this User." andCancelButton:NO forAlertType:AlertSuccess];
            [alert show];
            backendless.userService.currentUser = savedUser ;
            [self checkFollower];
            NSString *friendRequestMessage = [NSString stringWithFormat:@"%@ is now Following You.", backendless.userService.currentUser.name];
            [Utility publishMessageAsPushNotificationAsync:friendRequestMessage forDevice:[_selectedUser getProperty:@"deviceId"]] ;
            Follower *newFollower ;
            NSMutableArray *followingList = [[NSMutableArray alloc] initWithArray:[savedUser getProperty:@"following"]] ;
            for (Follower *follower in followingList) {
                if ([follower.FollowerUserId isEqualToString:newFollowing.FollowerUserId]) {
                    newFollower = follower ;
                }
            }
            if (newFollower) {
                NSMutableArray *newFollowersList = [[NSMutableArray alloc] initWithArray:[_selectedUser getProperty:@"followers"]] ;
                [newFollowersList addObject:newFollower] ;
                [_selectedUser setProperty:@"followers" object:newFollowersList] ;
                [dataStore save:_selectedUser response:^(id savedSelectedUser) {
                    NSLog(@"Follower Saved");
                } error:^(Fault *error) {
                    NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
                }];
            }
            else{
                NSLog(@"issue at object nil") ;
            }
        } error:^(Fault *error) {
            [SVProgressHUD dismiss];
            NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }] ;
    }
}


- (IBAction)MessageBtnAction:(UIButton *)sender {
    
    NSLog(@"Can message from here") ;
    [self performSegueWithIdentifier:@"OtherUserToChat" sender:self];

    
    /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Under construction" andText:@"The Page is Currently under construction" andCancelButton:NO forAlertType:AlertInfo];
     
     [alert show];*/
    //[self performSegueWithIdentifier:@"ToMessages" sender:self];
}
- (IBAction)unwindToProfile:(UIStoryboardSegue*)sender
{
    
}

-(void)updateFriendsCircle {
    
    
    [SQBUtility getAllFrirendsOfUser:self.selectedUser responseBlock:^(id object, BOOL status, Fault *error) {
        if (status) {
            //self.friendsRedirectsBtn.hidden = NO ;
            friendArray = [[NSMutableArray alloc] initWithArray:(NSArray *)object] ;
            for (int i = 0 ; i < friendArray.count; i++) {
                if (i < 6) {
                    UIImageView *circleImageView = _allFreindsCircles[i];
                    [circleImageView setHidden:NO] ;
                    [circleImageView sd_setImageWithURL:[NSURL URLWithString:[friendArray[i] valueForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"] options:SDWebImageRefreshCached];
                }
                else{
                    int plusFriends = i - 5;
                    NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;
                    [_PlusFriends setTitle:btnTitle forState:UIControlStateNormal] ;
                    [_PlusFriends setHidden:NO];
                }
            }
        }
        else{
            if (error) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Error Fetching Friends" andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
            else{
                //self.friendsRedirectsBtn.hidden = YES ;
            }
        }
    }];
    
   /*
    for (int i = 0 ; i < friendArray.count; i++) {
        if (i < 6) {
            
            UIImageView *circleImageView = _allFreindsCircles[i];
            [circleImageView setHidden:NO] ;
            
            [circleImageView sd_setImageWithURL:[NSURL URLWithString:[friendArray[i] valueForKey:@"profileImage"]]
                               placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                        options:SDWebImageRefreshCached];
            [_PlusFriends setHidden:NO];
            [_PlusFriends setTitle:@"+0" forState:UIControlStateNormal] ;
            
        }
        else{
            int plusFriends = i - 5;
            NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;
            [_PlusFriends setTitle:btnTitle forState:UIControlStateNormal] ;
        }
    }
    */
}

-(void)updateFollwersCircle {
    
    
    [SQBUtility getAllFollowersOfUser:self.selectedUser responseBlock:^(id object, BOOL status, Fault *error) {
        if (status) {
            //self.followersRedirectsBtn.hidden = NO ;
            followrsArray = [[NSMutableArray alloc] initWithArray:(NSArray *)object] ;
            for (int i = 0 ; i < followrsArray.count; i++) {
                if (i < 6) {
                    UIImageView *circleImageView = _allFollwersCircles[i];
                    [circleImageView setHidden:NO] ;
                    [circleImageView sd_setImageWithURL:[NSURL URLWithString:[followrsArray[i] valueForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"] options:SDWebImageRefreshCached];
                    [_plusFollowerAction setHidden:YES];
                    [_plusFollowerAction setTitle:@"+0" forState:UIControlStateNormal] ;
                }
                else{
                    [_plusFollowerAction setHidden:NO];
                    int plusFriends = i - 5;
                    NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;
                    [_plusFollowerAction setTitle:btnTitle forState:UIControlStateNormal] ;
                }
            }
        }
        else{
            if (error) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Error Fetching Followers" andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
            else{
                //self.followersRedirectsBtn.hidden = YES ;
            }
            
        }
    }] ;
    
    
    
    /*
    
    for (int i = 0 ; i < followrsArray.count; i++) {
        if (i < 6) {
            
            UIImageView *circleImageView = _allFollwersCircles[i];
            [circleImageView setHidden:NO] ;
            
            [circleImageView sd_setImageWithURL:[NSURL URLWithString:[followrsArray[i] valueForKey:@"profileImage"]]
                               placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                        options:SDWebImageRefreshCached];
            [_plusFollowerAction setHidden:NO];
            [_plusFollowerAction setTitle:@"+0" forState:UIControlStateNormal] ;
            
        }
        else{
            int plusFriends = i - 5;
            NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;
            [_plusFollowerAction setTitle:btnTitle forState:UIControlStateNormal] ;
        }
    }
    */
}
-(void)hideObjects{
    
    
    [_image_1 setHidden:YES];
    [_image_2 setHidden:YES];
    [_image_3 setHidden:YES];
    [_image_4 setHidden:YES];
    [_image_5 setHidden:YES];
    [_image_6 setHidden:YES];
    [_PlusFriends setHidden:YES];
    [_image_7 setHidden:YES];
    [_image_8 setHidden:YES];
    [_image_9 setHidden:YES];
    [_image_10 setHidden:YES];
    [_image_11 setHidden:YES];
    [_image_12 setHidden:YES];
    [_plusFollowerAction setHidden:YES];
    
    [_LBV_1 setHidden:YES];
    [_LBV_2 setHidden:YES];
    [_LBV_3 setHidden:YES];
    [_LBV_4 setHidden:YES];
    [_LBV_5 setHidden:YES];
    [_btn_LBV setHidden:YES];
    
    
}
-(void)fetchVideos{
    
    
    [_WBV_1 setHidden:YES];
    [_WBV_2 setHidden:YES];
    [_WBV_3 setHidden:YES];
    [_WBV_4 setHidden:YES];
    [_WBV_5 setHidden:YES];
    [_btn_WBV setHidden:YES];
    
    
    
    BackendlessDataQuery *query1 = [BackendlessDataQuery query];
    query1.whereClause = [NSString stringWithFormat:@"User.objectId = '%@' ",self.selectedUser.objectId];
    
    id<IDataStore> dataStore2 = [backendless.persistenceService of:[Video class]];
    [dataStore2 find:query1 response:^(BackendlessCollection *allWinningBattleVideos) {
        NSLog(@"winning battles found = %lu",(unsigned long)allWinningBattleVideos.data.count);
        self.vidsFound = [[NSMutableArray alloc] initWithArray:allWinningBattleVideos.data];
        
        _BattleVidsFound = [[NSMutableArray alloc] init];
        _lostVidsFound = [[NSMutableArray alloc] init];
        _wonVidsFound = [[NSMutableArray alloc] init];
        
        for (int i=0; i<allWinningBattleVideos.data.count; i++) {
            
            Video *vid = [allWinningBattleVideos.data objectAtIndex:i];
            
            if (vid.isBattle) {
                [_BattleVidsFound addObject:vid];
                if (vid.isWin) {
                    [_wonVidsFound addObject:vid];
                }
                else{
                    [_lostVidsFound addObject:vid];
                }
            }
        }
        
        [_winNumberTxt setText:[NSString stringWithFormat:@"%lu", (unsigned long)_wonVidsFound.count]] ;
        [_lostNumberTxt setText:[NSString stringWithFormat:@"%lu", (unsigned long)_lostVidsFound.count]] ;
        [_battlesNumberTxt setText:[NSString stringWithFormat:@"%lu", (unsigned long)_BattleVidsFound.count]] ;
        
        
        
        for (int i = 0; i < _wonVidsFound.count; i++) {
            Video *vid = [_wonVidsFound objectAtIndex:i];
            
            if(i==0){
                [_WBV_1 setHidden:NO];
                [_image_WBV_1 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i==1){
                
                
                [_WBV_2 setHidden:NO];
                [_image_WBV_2 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i==2){
                [_WBV_3 setHidden:NO];
                [_image_WBV_3 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            
            else if(i==3){
                [_WBV_4 setHidden:NO];
                [_image_WBV_4 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            
            else if (i==4){
                [_WBV_5 setHidden:NO];
                [_image_WBV_5 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i>4){
                
                [_btn_WBV setHidden:NO];
                
                NSString *result = [NSString stringWithFormat:@"+%lu",(unsigned long)_wonVidsFound.count-5];
                
                [_plus_btn_WBV setTitle:result forState:UIControlStateNormal];
                
                
            }
        }
        
        
        for (int i = 0; i < _lostVidsFound.count; i++) {
            Video *vid = [_lostVidsFound objectAtIndex:i];
            
            if(i==0){
                [_LBV_1 setHidden:NO];
                [_image_LBV_1 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i==1){
                
                
                [_LBV_2 setHidden:NO];
                [_image_LBV_2 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i==2){
                [_LBV_3 setHidden:NO];
                [_image_LBV_3 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            
            else if(i==3){
                [_LBV_4 setHidden:NO];
                [_image_LBV_4 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            
            else if (i==4){
                [_LBV_5 setHidden:NO];
                [_image_LBV_5 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i>4){
                
                [_btn_LBV setHidden:NO];
                
                //                NSString *result = [NSString stringWithFormat:@"+%lu",(unsigned long)_lostVidsFound.count-5];
                //
                //                [_plus_btn_ setTitle:result forState:UIControlStateNormal];
                
                
            }
        }
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at Video Gather %@",error.detail);
    }];
    
    
    
}
- (IBAction)playVideoAction:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:0] ;
    self.selectedVideo = vid ;
    
    //[self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_2:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:1] ;
    self.selectedVideo = vid ;
    
    //[self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_3:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:2] ;
    self.selectedVideo = vid ;
    
    //[self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_4:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:3] ;
    self.selectedVideo = vid ;
    
    //[self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_5:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:4] ;
    self.selectedVideo = vid ;
    
    //[self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_6:(id)sender {
}

- (IBAction)playVideoAction_7:(id)sender {
}

- (IBAction)playVideoAction_8:(id)sender {
}

- (IBAction)playVideoAction_9:(id)sender {
}

- (IBAction)playVideoAction_10:(id)sender {
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToVideoPaybackFromProfile"]) {
        VideoPlayViewController *vc = segue.destinationViewController ;
        vc.selectedVideo = self.selectedVideo ;
    }
    if ([segue.identifier isEqualToString:@"OtherUserToChat"]) {
       
        ChatViewController *vc = segue.destinationViewController ;
        vc.otherUser = self.selectedUser ;
        vc.comingFromProfile = YES ;
        
    
        
        
        
    }
    
}

@end
