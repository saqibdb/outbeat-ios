//
//  ProfiledatenAndernViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/11/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "ProfiledatenAndernViewController.h"
#import "Backendless.h"
#import "profileInformation.h"
#import "SVProgressHUD.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "ActionSheetPicker.h"
#import "NeuerAccountViewController.h"
#import "UIImageView+WebCache.h"
#import "UIButton+WebCache.h"


@interface ProfiledatenAndernViewController (){
UIImage *selectedImage;
}
@property NSMutableArray *profileData;
@end

@implementation ProfiledatenAndernViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view layoutIfNeeded]; 
    _personalCredView.layer.cornerRadius=60;
    _personalCredView.clipsToBounds=YES;
    
    _txtFirstName.placeholder=[NSString stringWithFormat:NSLocalizedString(@"First Name", nil)];
    _txtLastName.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Last Name", nil)];
    _txtUserName.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Username", nil)];
    _txtEmail.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Email", nil)];
    _txtHome.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Home", nil)];
    _txtAboutMe.placeholder=[NSString stringWithFormat:NSLocalizedString(@"About me", nil)];
    _lblTitle.text=[NSString stringWithFormat:NSLocalizedString(@"Change Profile", nil)];
    _lblProfile.text=[NSString stringWithFormat:NSLocalizedString(@"Profile", nil)];
    
    
    [SVProgressHUD showWithStatus:@"Please wait..."];
    
    [_profileBtnImage.layer setCornerRadius:_profileBtnImage.frame.size.height/2];
    _profileBtnImage.clipsToBounds=YES;
    // Do any additional setup after loading the view.
    //[self currentUserData];
    
    
    [SVProgressHUD show];
    
    
    
    

    
    
    
    
    if ([backendless.userService.currentUser getProperty:@"firstName"] != [NSNull null]){
        self.txtFirstName.text = [backendless.userService.currentUser getProperty:@"firstName"];
    }
    if ([backendless.userService.currentUser getProperty:@"lastName"] != [NSNull null]){
        self.txtLastName.text = [backendless.userService.currentUser getProperty:@"lastName"];
    }
    if ([backendless.userService.currentUser getProperty:@"name"] != [NSNull null]){
        self.txtUserName.text = [backendless.userService.currentUser getProperty:@"name"];
    }
    if ([backendless.userService.currentUser getProperty:@"email"] != [NSNull null]){
        self.txtEmail.text = [backendless.userService.currentUser getProperty:@"email"];
    }
    if ([backendless.userService.currentUser getProperty:@"homeAddress"] != [NSNull null]){
        self.txtHome.text = [backendless.userService.currentUser getProperty:@"homeAddress"];
    }
    if ([self.txtHome.text isEqualToString:@"_"]) {
        self.txtHome.text =@"";
    }
    
    if ([backendless.userService.currentUser getProperty:@"aboutMe"] == [NSNull null]) {
        self.txtAboutMe.text=@"";
    }else{
        
        self.txtAboutMe.text = [backendless.userService.currentUser getProperty:@"aboutMe"];
    }
    
    
    if ([backendless.userService.currentUser getProperty:@"profileImage"] != [NSNull null]){
        [self.profileBtnImage sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]] forState:UIControlStateNormal placeholderImage:nil];
        
    }
    
    
    
    [SVProgressHUD dismiss];

    
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated {
    
    
 
}
- (IBAction)changeAction:(id)sender {
    
    [SVProgressHUD showWithStatus:@"updating User..."];
    
        NSLog(@"File is %@", [backendless.userService.currentUser getProperty:@"profileImage"]) ;
    
    if (!selectedImage) {
        NSLog(@"No profile image selected") ;
        BackendlessUser *updateUser = backendless.userService.currentUser ;
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [updateUser setProperty:@"name" object:self.txtUserName.text];
        [updateUser setProperty:@"firstName" object:self.txtFirstName.text];
        [updateUser setProperty:@"lastName" object:self.txtLastName.text];
        [updateUser setProperty:@"email" object:self.txtEmail.text];
        [updateUser setProperty:@"homeAddress" object:self.txtHome.text];
        [updateUser setProperty:@"aboutMe" object:self.txtAboutMe.text];
        [dataStore save:updateUser response:^(BackendlessUser *updateUser) {
            backendless.userService.currentUser = updateUser ;
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Success" andText:@"User Updated Successfully" andCancelButton:NO forAlertType:AlertSuccess];
            [alert show];
            [SVProgressHUD dismiss];
        } error:^(Fault *error) {
            [SVProgressHUD dismiss];
            NSString *errorMsg = [NSString stringWithFormat:@"Failed to Update. Details = %@", error.detail];
            NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }];
    }
    else{
        [self uploadAsyncWithImageName:backendless.userService.currentUser.objectId completionWithDB:^(NSString *fileName, BOOL Status) {
            if (Status) {
                BackendlessUser *updateUser = backendless.userService.currentUser ;
                id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
                
                [updateUser setProperty:@"name" object:self.txtUserName.text];
                [updateUser setProperty:@"firstName" object:self.txtFirstName.text];
                [updateUser setProperty:@"lastName" object:self.txtLastName.text];
                [updateUser setProperty:@"email" object:self.txtEmail.text];
                [updateUser setProperty:@"homeAddress" object:self.txtHome.text];
                [updateUser setProperty:@"aboutMe" object:self.txtAboutMe.text];
                [updateUser setProperty:@"profileImage" object:fileName];
                
                
                [dataStore save:updateUser response:^(BackendlessUser *updateUser) {
                    backendless.userService.currentUser = updateUser ;
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@" " andText:@"User Updated Successfully" andCancelButton:NO forAlertType:AlertSuccess];
                    [alert show];
                    [SVProgressHUD dismiss];
                } error:^(Fault *error) {
                    [SVProgressHUD dismiss];
                    NSString *errorMsg = [NSString stringWithFormat:@"Failed to Update. Details = %@", error.detail];
                    NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
                    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
                    [alert show];
                }];
            }
            else{
                
            }
        }];
    }
}
-(void)loginWithUser :(BackendlessUser *)loginUser
{
   
    
    [backendless.userService login:loginUser.email password:loginUser.password response:^(BackendlessUser *userLogged) {
        UIDevice *device = [UIDevice currentDevice];
        NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
        
        
        [userLogged setProperty:@"deviceId" object:currentDeviceId];
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
        [dataStore save:userLogged response:^(id result) {
            
        } error:^(Fault *error) {
            NSLog(@"Error at deviceId %@",error.description);
        }];
        
        
        
        [SVProgressHUD dismiss];
            } error:^(Fault *error) {
        
        [SVProgressHUD dismiss];
        
        /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",error] andCancelButton:NO forAlertType:AlertInfo];
         [alert show];*/
    }];
}



-(void)updateImageUrl:(BackendlessUser *)imageUpdateUser :(NSString *)newUrl
{
    
    [imageUpdateUser setProperty:@"profileImage" object:[NSString stringWithFormat:@"%@", newUrl]];
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore save:imageUpdateUser response:^(id result) {
        [SVProgressHUD dismiss];
        [self loginWithUser:imageUpdateUser];
        
    } error:^(Fault *error) {
        NSLog(@"Error At imageUrl Update %@", error.description);
        NSString *errorMsg = [NSString stringWithFormat:@"Error At imageUrl Update. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
        
        
        [SVProgressHUD dismiss];
        
    }];
}

-(void)uploadAsyncWithImageName :(NSString *)imageName completionWithDB:(void (^)(NSString *fileName, BOOL Status))completionWithDB __attribute__((nonnull(2))){
    
    NSLog(@"\n============ Uploading file  with the ASYNC API ============");
    
    
    int REQUIRED_SIZE = 100;
    selectedImage = [self getScaledToRequiredSizedImage:REQUIRED_SIZE :selectedImage];
    
    NSData *imageData = UIImageJPEGRepresentation(selectedImage, 1.0);
    
    
    [backendless.fileService saveFile:[NSString stringWithFormat:@"myfiles/%@.jpg", imageName] content:imageData overwriteIfExist:YES response:^(BackendlessFile *uploadedFile) {
        
        completionWithDB(uploadedFile.fileURL, YES) ;

    } error:^(Fault *fault) {
        completionWithDB(nil, NO) ;

        NSLog(@"Server reported an error: %@", fault);
        
        NSString *errorMsg = [NSString stringWithFormat:@"Server reported an error. Details = %@", fault.detail];
        
        NSLog(@"FAULT = %@ <%@>", fault.message, fault.description);
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
        
        [SVProgressHUD dismiss];
    }] ;
    
}

-(UIImage *)getScaledToRequiredSizedImage :(int)requiredSize :(UIImage *)sentImage{
    int scale = 1;
    
    while (sentImage.size.width / scale / 2 >= requiredSize
           && sentImage.size.height / scale / 2 >= requiredSize)
        scale *= 2;
    
    return [self imageWithImage:selectedImage scaledToSize:CGSizeMake(selectedImage.size.width/scale, selectedImage.size.height/scale)];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}



-(IBAction)imagePickerAction:(UIButton *)sender{


    NSArray *colors = [NSArray arrayWithObjects:@"Gallery" , @"Camera", nil];
    
    [ActionSheetStringPicker showPickerWithTitle:@"Select Image Source."
                                            rows:colors
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           
                                           
                                           [picker hidePickerWithCancelAction];
                                           
                                           
                                           
                                           if (selectedIndex == 1) {
                                               
                                               [self showImagePicker:UIImagePickerControllerSourceTypeCamera sender:nil];
                                               
                                           }
                                           else{
                                               [self showImagePicker:UIImagePickerControllerSourceTypePhotoLibrary sender:nil];
                                               
                                           }
                                           
                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];




}
#pragma mark Image Picker And Delegates

- (void)showImagePicker:(UIImagePickerControllerSourceType)sourceType sender:(id)sender {
    
    
    UIImagePickerController *controler = [[UIImagePickerController alloc] init];
    if (![UIImagePickerController isSourceTypeAvailable:sourceType]) {
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Permission Error" andText:@"Permission not granted"  andCancelButton:NO forAlertType:AlertFailure];
         
         [alert show];
        
        //loder remove
        return;
    }
    controler.sourceType = sourceType;
    controler.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:sourceType]) {
        
        [self presentViewController:controler animated:YES completion:nil];
        
    }
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSLog(@"Picker returned successfully.");
    
    NSURL *mediaUrl;
    mediaUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
    if (mediaUrl == nil) {
        selectedImage = (UIImage *) [info valueForKey:UIImagePickerControllerEditedImage];
        if (selectedImage == nil) {
            selectedImage= (UIImage *) [info valueForKey:UIImagePickerControllerOriginalImage];
            NSLog(@"Original image picked.");
            selectedImage = [self scaleAndRotateImage:selectedImage];
            [self.profileBtnImage setImage:selectedImage forState:UIControlStateNormal];
        }
        else {
            NSLog(@"Edited image picked.");
        }
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Image url is : %@",[mediaUrl absoluteString]);
}

- (UIImage *)scaleAndRotateImage:(UIImage *) image {
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch(orient) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}


- (IBAction)backAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"profileAndernToSettings" sender:self];
}
@end
