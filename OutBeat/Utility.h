//
//  Utility.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 8/2/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "outbeat-swift.h"
#import <UIKit/UIKit.h>
@import LiquidLoader;

@interface Utility : NSObject


+(void)showLoading:(UIView *)mainView andGreyBackground :(UIView *)grayView andLineLoader :(LiquidLoader *)lineLoader ;
+(void)hideLoading :(UIView *)grayView andLineLoader :(LiquidLoader *)lineLoader ;
+(LiquidLoader *)createAndAdjustLiquidLoader :(UIView *)mainView ;
+(UIView *)createGrayView ;

+(UIColor *)getOrangeColor ;

+(NSString *)getTimeFormatForDate :(NSDate *)commentDate ;
+(void)publishMessageAsPushNotificationAsync:(NSString *)message forDevice:(NSString *)deviceId ;
@end
