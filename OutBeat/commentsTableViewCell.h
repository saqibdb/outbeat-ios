//
//  commentsTableViewCell.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/27/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface commentsTableViewCell : SWTableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImage;
@property (weak, nonatomic) IBOutlet UITextView *commentTextView;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *lblcomment;
@property (weak, nonatomic) IBOutlet UILabel *commentTime;

@property (weak, nonatomic) IBOutlet UIButton *CommentOptionsBtn;

@end
