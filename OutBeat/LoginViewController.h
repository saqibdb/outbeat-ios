//
//  LoginViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/12/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UIGestureRecognizerDelegate, UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *passwordtext;
@property (weak, nonatomic) IBOutlet UITextField *emailtext;
- (IBAction)loginAction:(id)sender;
- (IBAction)BackToGetStartedFromLogin:(UIButton *)sender;
- (IBAction)forgetPasswordAction:(UIButton *)sender;
@property NSString *identity ;
@property (weak, nonatomic) IBOutlet UITextField *txtNewEmail;
@property (weak, nonatomic) IBOutlet UIView *forgetView;
- (IBAction)forgetDoneAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *credView;
@property (weak, nonatomic) IBOutlet UIButton *loginbtn;


@property BOOL isBackable;


@property (weak, nonatomic) IBOutlet UIView *resetView;
@property (weak, nonatomic) IBOutlet UITextField *resetEmailText;

@property (weak, nonatomic) IBOutlet UIButton *resetBtn;


@property (weak, nonatomic) IBOutlet UIButton *REGISTERBTN;
- (IBAction)registerAction:(UIButton *)sender;
- (IBAction)resetAction:(id)sender;


@end
