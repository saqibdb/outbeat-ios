//
//  FriendRequests.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/3/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendRequests : UIViewController <UITableViewDataSource ,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *confirmAction;
@property(nonatomic, strong)  NSMutableArray *FriendsRequestArray;
@property(nonatomic, strong)  NSMutableArray *FriendsUsersArray;

@end
