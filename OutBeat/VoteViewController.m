//
//  VoteViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/23/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//
#import <MessageUI/MFMailComposeViewController.h>
#import <MessageUI/MessageUI.h>
#import "Stars.h"
#import "SearchPopUp.h"
#import "TNRadioButtonGroup.h"
#import "TNRectangularRadioButton.h"
#import "VoteViewController.h"
#import "HCSStarRatingView.h"
#import "Backendless.h"
#import "Battle.h"
#import "UIImageView+WebCache.h"
#import "UIImageView+WebCache.h"
#import "commentsTableViewCell.h"
#import "Comments.h"
#import <Social/Social.h>
#import "IQKeyboardManager.h"
#import "IQUIView+IQKeyboardToolbar.h"
#import "Comments.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Utility.h"
#import "Vote.h"
#import "UserProfileViewController.h"
#import "VotersListViewController.h"
#import "VideoPlayViewController.h"
#import "UIViewController+CWPopup.h"
#import "PDKPin.h"
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "EmailPopUp.h"
#import "SendGrid.h"
#import "SendGridEmail.h"


@interface VoteViewController ()
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *starRatingView_2;

@end

@implementation VoteViewController{
    int video1wins ;
    int video2wins ;
    NSMutableArray *vid1Voters;
    NSMutableArray *vid2Voters;
    int Result;
    NSArray *allVoters;
    SearchPopUp *callUsPopUpView ;
    NSInteger indexToRemove ;
    NSString * bodypartOne;
    EmailPopUp *emailPopUpView ;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view layoutIfNeeded];
    self.verticalSpace.constant=-self.dropDownView.frame.size.height;
   
    video1wins = 0;
    video2wins = 0;
    
    [self filterVoters];
    
    [self add];
    self.commentsTableView.dataSource = self;
    self.commentsTableView.delegate= self;
   
    _lblTitleFirst.text= self.selectedBattle.videoOne.Title;
    _lblTitleSecond.text= self.selectedBattle.videoTwo.Title;
    
  
//    _userNameFirst.text=[NSString stringWithFormat:@"%@  |  %@ Wins" ,self.selectedBattle.videoOne.User.name,self.selectedBattle.videoOneWins]    ;
//    _userNameSecond.text=[NSString stringWithFormat:@"%@  |  %@ Wins" ,self.selectedBattle.videoTwo.User.name,self.selectedBattle.videoTwoWins] ;
    
    
    [_thumnailOne sd_setImageWithURL:[NSURL URLWithString:self.selectedBattle.videoOne.ThumbnailURL]];
     [_thumnailSecond sd_setImageWithURL:[NSURL URLWithString:self.selectedBattle.videoTwo.ThumbnailURL]];
    [_profileImageFirst sd_setImageWithURL:[NSURL URLWithString:[self.selectedBattle.videoOne.User getProperty:@"profileImage"]]];
    [_profileImageSecond sd_setImageWithURL:[NSURL URLWithString:[self.selectedBattle.videoTwo.User getProperty:@"profileImage"]]];
    
    NSString *viewers =[NSString stringWithFormat:NSLocalizedString(@"viewers", nil)];
    _lblViewers.text=[NSString stringWithFormat:@"%@ %@",self.selectedBattle.Views,viewers];
    
       
    
    _starRatingView.maximumValue = 5;
    _starRatingView.minimumValue = 0;
    _starRatingView.value = 0;
  
    _starRatingView.allowsHalfStars = NO;
    _starRatingView.emptyStarImage = [UIImage imageNamed:@"grayStar"];
    _starRatingView.filledStarImage = [UIImage imageNamed:@"fillStar"];
    
    
    //rating view second
    _starRatingView_2.maximumValue = 5;
    _starRatingView_2.minimumValue = 0;
    _starRatingView_2.value = 0;
    
    Vote *newVote ;

    NSString *searchString = backendless.userService.currentUser.objectId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.User.objectId contains %@", searchString];
    NSArray *filteredArray = [_selectedBattle.Video1Votes filteredArrayUsingPredicate:predicate];
    if (filteredArray.count) {
        newVote = [filteredArray objectAtIndex:0] ;
        _starRatingView.value = [newVote.Stars intValue] ;
    }
   
    NSArray *filteredArray2 = [_selectedBattle.Video2Votes filteredArrayUsingPredicate:predicate];
    if (filteredArray2.count) {
        newVote = [filteredArray2 objectAtIndex:0] ;
        _starRatingView_2.value = [newVote.Stars intValue] ;
    }
    
    
    
    
    _starRatingView_2.allowsHalfStars = NO;
    _starRatingView_2.emptyStarImage = [UIImage imageNamed:@"grayStar"];
    _starRatingView_2.filledStarImage = [UIImage imageNamed:@"fillStar"];
    
  
    
    [_starRatingView addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    [_starRatingView_2 addTarget:self action:@selector(didChangeValue2:) forControlEvents:UIControlEventValueChanged];
    
    
    
    
    
   /* if ([_selectedBattle.videoOne.User.objectId isEqualToString:backendless.userService.currentUser.objectId] || [_selectedBattle.videoTwo.User.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
        self.isSelfBattle = YES ;
        _starRatingView_2.enabled = NO ;
        _starRatingView.enabled = NO ;
    }
    else{
        self.isSelfBattle = NO ;
        _starRatingView_2.enabled = YES ;
        _starRatingView.enabled = YES ;
    }*/
    
   
    
    
    
    
    [_voter_1.layer setCornerRadius:_voter_1.frame.size.height/2];
    _voter_1.clipsToBounds=YES;
    _voter_1.layer.borderWidth=1.5;
    _voter_1.layer.borderColor=[[UIColor orangeColor] CGColor];
    [_voter_1 setHidden:YES];
    _voter_1.contentMode = UIViewContentModeScaleToFill;

    [_voter_2.layer setCornerRadius:_voter_2.frame.size.height/2];
    _voter_2.clipsToBounds=YES;
    _voter_2.layer.borderWidth=1.5;
    _voter_2.layer.borderColor=[[UIColor orangeColor] CGColor];
    [_voter_2 setHidden:YES];
    _voter_2.contentMode = UIViewContentModeScaleToFill;

    [_voter_3.layer setCornerRadius:_voter_3.frame.size.height/2];
    _voter_3.clipsToBounds=YES;
    _voter_3.layer.borderWidth=1.5;
    _voter_3.layer.borderColor=[[UIColor orangeColor] CGColor];
    [_voter_3 setHidden:YES];
    _voter_3.contentMode = UIViewContentModeScaleToFill;

    [_voter_4.layer setCornerRadius:_voter_4.frame.size.height/2];
    _voter_4.clipsToBounds=YES;
    _voter_4.layer.borderWidth=1.5;
    _voter_4.layer.borderColor=[[UIColor orangeColor] CGColor];
    [_voter_4 setHidden:YES];
    _voter_4.contentMode = UIViewContentModeScaleToFill;

    [_voter_5.layer setCornerRadius:_voter_5.frame.size.height/2];
    _voter_5.clipsToBounds=YES;
    _voter_5.layer.borderWidth=1.5;
    _voter_5.layer.borderColor=[[UIColor orangeColor] CGColor];
    [_voter_5 setHidden:YES];
    _voter_5.contentMode = UIViewContentModeScaleToFill;

    [_voter_6.layer setCornerRadius:_voter_6.frame.size.height/2];
    _voter_6.clipsToBounds=YES;
    _voter_6.layer.borderWidth=1.5;
    _voter_6.layer.borderColor=[[UIColor orangeColor] CGColor];
    [_voter_6 setHidden:YES];
    _voter_6.contentMode = UIViewContentModeScaleToFill;

    [_plusVoters.layer setCornerRadius:_voter_6.frame.size.height/2];
    _voter_6.clipsToBounds=YES;
    _voter_6.layer.borderWidth=1.5;
    _voter_6.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_plusVoters setHidden:YES];

    
 
    for (int i = 0; i < _uniqueEntries.count; i++) {
        BackendlessUser *voter = _uniqueEntries[i];
        if (i == 0) {
            [_voter_1 setHidden:NO];
            [_voter_1 sd_setImageWithURL:[NSURL URLWithString:[voter getProperty:@"profileImage"] ]];
        }
        else if (i == 1){
            [_voter_2 setHidden:NO];
            [_voter_2 sd_setImageWithURL:[NSURL URLWithString:[voter getProperty:@"profileImage"] ]];
        }
        else if (i == 2){
            [_voter_3 setHidden:NO];
            [_voter_3 sd_setImageWithURL:[NSURL URLWithString:[voter getProperty:@"profileImage"] ]];
        }
        else if (i == 3){
            [_voter_4 setHidden:NO];
            [_voter_4 sd_setImageWithURL:[NSURL URLWithString:[voter getProperty:@"profileImage"] ]];
        }
        else if (i == 4){
            [_voter_5 setHidden:NO];
            [_voter_5 sd_setImageWithURL:[NSURL URLWithString:[voter getProperty:@"profileImage"] ]];
        }
        else if (i == 5){
            [_voter_6 setHidden:NO];
            [_voter_6 sd_setImageWithURL:[NSURL URLWithString:[voter getProperty:@"profileImage"] ]];
        }
        else if (i > 5){
            [_plusVoters setHidden:NO];
            NSString *extraVoters = [NSString stringWithFormat:@"+%lu",self.selectedBattle.Voters.count - 6];
            [_plusVoters setTitle:extraVoters forState:UIControlStateNormal] ;
            break;

        }
        
    }

   


    [self.commentTextField addDoneOnKeyboardWithTarget:self action:@selector(commentSave:) shouldShowPlaceholder:YES];
    
    [SVProgressHUD showWithStatus:@"Loading Comments ..."];
    [self sortByDate:self.selectedBattle.Comments completion:^(NSArray *arraySorted) {
        
        NSArray* reversedArray = [[arraySorted reverseObjectEnumerator] allObjects];

        [SVProgressHUD dismiss];
        self.selectedBattle.Comments = [reversedArray mutableCopy] ;
        [self.commentsTableView reloadData] ;
    }];
    
    [self.view layoutIfNeeded];
    
    [_profileImageFirst.layer setCornerRadius:_profileImageFirst.frame.size.height/2];
    _profileImageFirst.clipsToBounds=YES;
    _profileImageFirst.layer.borderWidth=1.5;
    _profileImageFirst.layer.borderColor=[[UIColor orangeColor] CGColor];
    [_profileImageSecond.layer setCornerRadius:_profileImageSecond.frame.size.height/2];
    _profileImageSecond.clipsToBounds=YES;
    _profileImageSecond.layer.borderWidth=1.5;
    _profileImageSecond.layer.borderColor=[[UIColor orangeColor] CGColor];
    [_VsSprite.layer setCornerRadius:_VsSprite.frame.size.height/2];
    _VsSprite.clipsToBounds=YES;
    
    
    

    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:_VsSprite.bounds];
    _VsSprite.layer.shadowColor = [UIColor blackColor].CGColor;
    _VsSprite.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
    _VsSprite.layer.shadowOpacity = 0.3f;
    _VsSprite.layer.shadowPath = shadowPath.CGPath;
    
   
    
    [self.circulerProgressBarView setValue:[_selectedBattle.TotalVotes floatValue]];
   
    NSLog(@"Total Votes = %@", _selectedBattle.TotalVotes) ;
    
    self.selectedBattlePresistance = self.selectedBattle ;
    
    int result = 0 ;
//    int v1=(int)self.selectedBattle.Video1Votes.count ;
//    int v2=(int)self.selectedBattle.Video2Votes.count ;
    
    
//    NSArray *one =[[NSArray alloc]init];
//    NSArray *two =[[NSArray alloc] init];
//    one = [ self.selectedBattle.Video1Votes arrayByAddingObjectsFromArray:self.selectedBattle.Video2Votes];
//    two = [[NSSet setWithArray:one] allObjects];
    
     result =(int)_uniqueEntries.count;
    
    for (Vote *vote in self.selectedBattle.Video2Votes) {
        NSString *searchString = vote.User.objectId;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.User.objectId contains %@", searchString];
        NSArray *filteredArray = [_selectedBattle.Video1Votes filteredArrayUsingPredicate:predicate];
        if (!filteredArray.count) {
            result = result + 1 ;
        }
    }
    

    
    NSString *Votes = [NSString stringWithFormat:NSLocalizedString(@"Votes", nil)];
    _lblVotesOutOfTotal.text=[NSString stringWithFormat:@"%lu of 1000 %@ " , (unsigned long)result, Votes];
    
    for (Vote *vote1 in self.selectedBattle.Video1Votes) {
        
        video1wins = video1wins + [vote1.Stars intValue] ;
        
    }
    for (Vote *vote2 in self.selectedBattle.Video2Votes) {
        
        video2wins = video2wins + [vote2.Stars intValue] ;
    }
    
   
    
    
    if (result==1000){
        
        _starRatingView_2.enabled = NO ;
        _starRatingView.enabled = NO ;
         _starRatingView.hidden=YES;
        _starRatingView_2.hidden=YES;
        
        if(video1wins > video2wins){
            
            NSLog(@"video One is Winner ");
            self.frownView1.hidden=NO;
           
        }
        else if (video1wins < video2wins){
            
            NSLog(@"video two is Winner ");
            self.frownView2.hidden=NO;
            
        }
        else{
             NSLog(@"compition is equal  ");
        }
        
    }

    NSString *wins =[NSString stringWithFormat:NSLocalizedString(@"wins", nil)];
    _userNameFirst.text=[NSString stringWithFormat:@"%@  |  %i %@" ,self.selectedBattle.videoOne.User.name,video1wins,wins]    ;
    _userNameSecond.text=[NSString stringWithFormat:@"%@  |  %i %@" ,self.selectedBattle.videoTwo.User.name,video2wins,wins] ;
    
    
    _actualScore1.text=[NSString stringWithFormat:@"%i " ,video1wins];
    _actualScore2.text=[NSString stringWithFormat:@"%i" ,video2wins];
   
    
    _commentTextField.placeholder=[NSString stringWithFormat:NSLocalizedString(@"Tap to Write", nil)];
    _lblShare.text=[NSString stringWithFormat:NSLocalizedString(@"share", nil)];
    _lblComments.text=[NSString stringWithFormat:NSLocalizedString(@"Comments", nil)];
    

    
    if (filteredArray.count) {
        
        _starRatingView_2.enabled = NO ;
        _starRatingView.enabled = NO ;
       
       
    }
    
    

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];

    
}
- (void)keyboardWillHide:(NSNotification *)notification
{
    self.keyboardViewBottomConstraint.constant = 0;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    CGFloat height = [[notification.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.height;
    self.keyboardViewBottomConstraint.constant = height;
    [UIView animateWithDuration:0.2
                     animations:^{
                         [self.view layoutIfNeeded];
                     }];
}

//-(void)disableVoting{
//   
//    _starRatingView.enabled = NO ;
//    _starRatingView.hidden=YES;
//    _actualScore1.hidden=NO;
//}
//-(void)disableVoting2{
//    _starRatingView_2.enabled = NO ;
//    _starRatingView_2.hidden=YES;
//    _actualScore2.hidden=NO;
//   
//}

-(void)filterVoters{
    
    vid1Voters=[[NSMutableArray alloc]init];
    vid2Voters=[[NSMutableArray alloc]init];
    allVoters=[[NSArray alloc] init];
    _uniqueEntries=[[NSArray alloc] init];
    
    
    for (Vote *voter in self.selectedBattle.Video1Votes)
    {
        
        [vid1Voters addObject:voter.User];
    }
    
    for (Vote *voter2 in self.selectedBattle.Video2Votes)
    {
        
        [vid2Voters addObject:voter2.User];
    }
    
    allVoters = [ vid1Voters arrayByAddingObjectsFromArray:vid2Voters];
    _uniqueEntries = [[NSSet setWithArray:allVoters] allObjects];
    if (self.uniqueEntries.count==0){
    
        _openVotersListBtn.enabled=NO;
        
    }
}


- (void)commentSave:(id)sender {
    
    [self.commentTextField resignFirstResponder] ;
    NSLog(@"Comment Passed = %@" , self.commentTextField.text) ;
    if (self.commentTextField.text.length) {
        [SVProgressHUD showWithStatus:@"Saving Comment ..."];
        
        Comments *comment = [[Comments alloc] init];
        comment.Message = self.commentTextField.text ;
        comment.User =  backendless.userService.currentUser ;
        comment.created = [NSDate date] ;
        
        
        [_selectedBattle.Comments addObject:comment] ;
        
        [self sortByDate:_selectedBattle.Comments completion:^(NSArray *arraySorted) {
            
            NSArray* reversedArray = [[arraySorted reverseObjectEnumerator] allObjects];
            
            [SVProgressHUD dismiss];
            _selectedBattle.Comments = [reversedArray mutableCopy] ;
            [self.commentsTableView reloadData];
            [self.commentTextField setText:@""];
            [self.commentTextField resignFirstResponder] ;
            
            [self.commentsTableView scrollsToTop];
            
        }];
        
        
        
         //id<IDataStore> dataStore = [backendless.persistenceService of:[Comments class]];
        //[dataStore save:comment response:^(id savedComment) {
            id<IDataStore> dataStoreBattle = [backendless.persistenceService of:[Battle class]];
            //[_selectedBattle.Comments addObject:savedComment] ;
            [dataStoreBattle save:_selectedBattle response:^(id savedBattle) {
                NSLog(@"Comment Saved");
            } error:^(Fault *errorBattle) {
                [SVProgressHUD dismiss];
                NSLog(@"Error At Saving Battle = %@" , errorBattle.description) ;
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",errorBattle.detail] andCancelButton:NO forAlertType:AlertInfo];
                [alert show];
                [_selectedBattle.Comments removeObject:comment] ;
                [self.commentsTableView reloadData] ;
            }];
        /*} error:^(Fault *error) {
            [SVProgressHUD dismiss];
            NSLog(@"Error At Saving Comment = %@" , error.description) ;
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",error.description] andCancelButton:NO forAlertType:AlertInfo];
            [alert show];
            [_selectedBattle.Comments removeObject:comment] ;
            [self.commentsTableView reloadData] ;
        }];*/
    }
    else{
        [self.commentTextField resignFirstResponder] ;
    }
}


-(void)viewDidAppear:(BOOL)animated{
    
    if ([self.selectedBattle.videoOne.User.objectId isEqualToString:backendless.userService.currentUser.objectId] || [self.selectedBattle.videoTwo.User.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
        _starRatingView_2.enabled = NO ;
        _starRatingView.enabled = NO ;
        /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Warning" andText:[NSString stringWithFormat:@"You Will not be able to Vote for your own Battles"] andCancelButton:NO forAlertType:AlertInfo];
        [alert show];*/
        NSLog(@"You Will not be able to Vote for your own Battles") ;
    }
    
}
-(void)add{
    
    int vidOneVotes = self.selectedBattle.videoOneVotes.intValue;
     int vidTwoVotes = self.selectedBattle.videoTwoVotes.intValue;
    Result = vidOneVotes + vidTwoVotes;
    _lblVotesOutOfTotal.text=[NSString stringWithFormat:@"%d of 1000 Votes" , Result];
   
    [self.circulerProgressBarView setValue:Result
           animateWithDuration:1];


}
-(void)fetchingVideosAsync {
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[Battle class]];
    
    BackendlessDataQuery *query = [BackendlessDataQuery new];
    QueryOptions *queryOptions = [QueryOptions new];
    queryOptions.related = [NSMutableArray arrayWithArray:@[@"Comments"]];
    
    [queryOptions sortBy:@[@"created"]];
    
    query.queryOptions = queryOptions;
    
    
    
    [dataStore find:query response:^(BackendlessCollection *videosCollection) {
        
        _allBattles =[[NSMutableArray alloc]initWithArray:videosCollection.data];
        
       
        
        
    } error:^(Fault *error) {
        NSLog(@"error at ");
    }];
    
}
-(void)sortByDate :(NSArray *)comments completion:(void (^)(NSArray *arraySorted))completion __attribute__((nonnull(2)));
{
    NSMutableArray *commentsArray = [[NSMutableArray alloc] initWithArray:comments];
    
    NSArray *sortedLocations = [commentsArray sortedArrayUsingComparator: ^(Comments *a1, Comments *a2) {
        
        
        
        
        return [a1.created compare:a2.created];
    }];
    completion(sortedLocations);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.selectedBattle.Comments.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.commentsTableView layoutIfNeeded];
    static NSString *CellIdentifier = @"commentCell";
    commentsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[commentsTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                               reuseIdentifier:CellIdentifier];
    }
    Comments *comment = [self.selectedBattle.Comments objectAtIndex:indexPath.row];
    [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:[comment.User getProperty:@"profileImage"] ]];
    indexToRemove =indexPath.row;
    cell.lblcomment.text=comment.Message;
    cell.userName.text=comment.User.name;
    [cell.profileImage.layer setCornerRadius:cell.profileImage.frame.size.height/2];
    cell.profileImage.clipsToBounds=YES;
    cell.profileImage.layer.borderWidth=1.5;
    cell.profileImage.layer.borderColor=[[UIColor orangeColor] CGColor];
    NSString *commentDateString = [Utility getTimeFormatForDate:comment.created] ;
    
    cell.commentTime.text=[NSString stringWithFormat:@"%@",commentDateString];
           cell.rightUtilityButtons = [self rightButtonsAndComment:comment];
   cell.delegate = self;

    return cell;
    
}
- (NSArray *)rightButtonsAndComment:(Comments*)comment
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:
     [UIColor colorWithRed:0.78f green:0.78f blue:0.8f alpha:1.0]
                                                title:@"Report"];
    
    
    if ([comment.User.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
        [rightUtilityButtons sw_addUtilityButtonWithColor:
         [UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f]
                                                    title:@"Delete"];
    }
    
    
    return rightUtilityButtons;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}
- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            [self openReportPopUp ];
            break;
        case 1:
        {
            Comments *comment = self.selectedBattle.Comments [indexToRemove] ;
            id<IDataStore> dataStore = [backendless.persistenceService of:[Comments class]];
            [dataStore remove:comment response:^(NSNumber *number) {
                
                NSLog(@"comment removed");
                [self.commentsTableView reloadData];
                //[self fetchingVideosAsync ];
                
            } error:^(Fault *error) {
                NSLog(@"error at comment removed ==%@" ,error.detail);
            }];

            break;
        }
        default:
            break;
    }
}
-(void)openReportPopUp{
//asdfsadfadsfsadfef

        callUsPopUpView = [[SearchPopUp alloc] initWithNibName:@"SearchPopUp" bundle:nil];
    
        callUsPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width- 50, self.view.frame.size.height * 0.5 );
    
    
    TNImageRadioButtonData *one = [TNImageRadioButtonData new];
    one.labelText = @"its annoying or not interesting";
    one.identifier = @"its annoying or not interesting";
    one.selected = NO;
    one.unselectedImage = [UIImage imageNamed:@"unCheked"];
    one.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *two = [TNImageRadioButtonData new];
    two.labelText = @"i think it should'nt be on outBeat";
    two.identifier = @"i think it should'nt be on outBeat";
    two.selected = NO;
    two.unselectedImage = [UIImage imageNamed:@"unCheked"];
    two.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *three = [TNImageRadioButtonData new];
    three.labelText = @"its spam";
    three.identifier = @"its spam";
    three.selected = NO;
    three.unselectedImage = [UIImage imageNamed:@"unCheked"];
    three.selectedImage = [UIImage imageNamed:@"cheked"];
    
    TNImageRadioButtonData *four = [TNImageRadioButtonData new];
    four.labelText = @"its rude , vulger or uses bad language";
    four.identifier = @"its rude , vulger or uses bad language";
    four.selected = NO;
    four.unselectedImage = [UIImage imageNamed:@"unCheked"];
    four.selectedImage = [UIImage imageNamed:@"cheked"];
    

    
    
    callUsPopUpView.radioButtonGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[one, two,three,four] layout:TNRadioButtonGroupLayoutVertical];
    callUsPopUpView.radioButtonGroup.identifier = @"Temperature group";
  
    [callUsPopUpView.radioButtonGroup create];
    //callUsPopUpView.radioButtonGroup.position = CGPointMake(25, 400);
    [ callUsPopUpView.outerView addSubview : callUsPopUpView.radioButtonGroup];

     [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(temperatureGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:callUsPopUpView.radioButtonGroup];
    
    
    
        [self presentPopupViewController:callUsPopUpView animated:YES completion:^(void) {
       [callUsPopUpView.cancelBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
            [callUsPopUpView.sendBtn addTarget:self action:@selector(sendMail) forControlEvents:UIControlEventTouchUpInside];
   }];

}

-(void)sendMail{
  
    Comments *comment = self.selectedBattle.Comments [indexToRemove];
    
    
    NSString *subject = @"Report from outBeat";
    NSString *body =[NSString stringWithFormat:@" hy ! we received a report(Reason:%@) against your          commnet on Battle '%@' and your comment was '%@' .Please remove it ASAP",bodypartOne,self.selectedBattle.objectId, comment.Message];
    NSString *recipient = @"asadjavid46@gmail.com";
    NSLog(@"%@",recipient);
    [backendless.messagingService
    sendHTMLEmail:subject body:body to:@[recipient]
    response:^(id result) {
         NSLog(@"ASYNC: HTML email has been sent");
         [self dismissPopup];
     }
     error:^(Fault *fault) {
         NSLog(@"Server reported an error: %@", fault);
     }];
}


- (void)temperatureGroupUpdated:(NSNotification *)notification {
    NSLog(@"selected reason = %@", callUsPopUpView.radioButtonGroup.selectedRadioButton.data.identifier);
    bodypartOne = callUsPopUpView.radioButtonGroup.selectedRadioButton.data.identifier;
}
- (void)createHorizontalListWithImage {
    
   }

- (void)dismissPopup {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            
            NSLog(@"popup view dismissed");
        }];
    }
}

-(void)checkForVideo2Stars {
    if (_starRatingView_2.value) {
       
        Stars *newStar;
        Vote *newVote ;
        NSString *searchString = backendless.userService.currentUser.objectId;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.User.objectId contains %@", searchString];
        NSArray *filteredArray = [_selectedBattle.Video2Votes filteredArrayUsingPredicate:predicate];
        if (filteredArray.count) {
                        newVote = [filteredArray objectAtIndex:0] ;
            if ([newVote.Stars isEqualToString:[NSString stringWithFormat:@"%i",(int)_starRatingView_2.value]]) {
               
                [SVProgressHUD dismiss];
                [self performSegueWithIdentifier:@"votesToVideoFeeds" sender:self];
                return ;
            }
            
            newVote.Stars = [NSString stringWithFormat:@"%i",(int)_starRatingView_2.value] ;
            NSLog(@"Vote Updted...");
            
            
        }
        else{
            newVote = [[Vote alloc] init] ;
            newVote.User = backendless.userService.currentUser ;
            newVote.Stars = [NSString stringWithFormat:@"%i",(int)_starRatingView_2.value] ;
            
            newStar = [[Stars alloc] init] ;
            newStar.stars =newVote.Stars;
            newStar.User=self.selectedBattle.videoTwo.User;
            
        }
        
        id<IDataStore> dataStore = [backendless.persistenceService of:[Battle class]];
        
        if (!_selectedBattle.Video2Votes) {
            _selectedBattle.Video2Votes = [[NSMutableArray alloc] init] ;
        }
        [_selectedBattle.Video2Votes addObject:newVote];
       
        [dataStore save:_selectedBattle response:^(id savedBattle) {
            NSLog(@"Vote Saved...");
            _selectedBattle = savedBattle ;
            
            //saving user stars
            id<IDataStore> dataStore2 = [backendless.persistenceService of:[Stars class]];
             [dataStore2 save:newStar response:^(id saveNewStar) {
                NSLog(@"stars Saved...");
                 
                 BackendlessUser *videoTwoUser = self.selectedBattle.videoTwo.User;
                 NSString *currentStarsStr = [videoTwoUser getProperty:@"Stars"];
                 NSString *thisMonthStarsStr = [videoTwoUser getProperty:@"starsThisMonth"];
                 
                 // Total Stars
                 Stars *updatedStars = newStar;
                 NSString *newStars = updatedStars.stars;
                 NSString *totalUserStart = [NSString stringWithFormat:@"%ld",[currentStarsStr integerValue] + [newStars integerValue]];
                 NSLog(@"stars : %@",totalUserStart );
                 
                 
                 // Stars This month
                 NSString *starsThisMonth = [NSString stringWithFormat:@"%ld",[thisMonthStarsStr integerValue] + [newStars integerValue]];
                 NSLog(@"stars : %@",starsThisMonth );
                 
                 
                 
                 [videoTwoUser setProperty:@"Stars" object:totalUserStart];
                 [videoTwoUser setProperty:@"starsThisMonth" object:starsThisMonth];
                 
                 id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
                 [dataStore save:videoTwoUser responder:nil];
   
                
             } error:^(Fault *error) {
                 NSLog(@"stars not Saved...");
             }];
            
            
            [SVProgressHUD dismiss];
            [self performSegueWithIdentifier:@"votesToVideoFeeds" sender:self];
        } error:^(Fault *error) {
            NSLog(@"Error in Vote Saving...%@", error.description);
            [SVProgressHUD dismiss];
            [self performSegueWithIdentifier:@"votesToVideoFeeds" sender:self];
        }];
    }
    else{
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"votesToVideoFeeds" sender:self];
    }
}

- (IBAction)backAction:(id)sender {

    if (!self.isSelfBattle) {
        [SVProgressHUD showWithStatus:@"Saving Votes"] ;
        if (_starRatingView.value) {
            Vote *newVote ;
             Stars *newStar;
            NSString *searchString = backendless.userService.currentUser.objectId;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.User.objectId contains %@", searchString];
            NSArray *filteredArray = [_selectedBattle.Video1Votes filteredArrayUsingPredicate:predicate];
            if (filteredArray.count) {
                newVote = [filteredArray objectAtIndex:0];
                
                if ([newVote.Stars isEqualToString:[NSString stringWithFormat:@"%i",(int)_starRatingView.value]]) {
                    [self checkForVideo2Stars];
                    return;
                }
                
                newVote.Stars = [NSString stringWithFormat:@"%i",(int)_starRatingView.value] ;
                NSLog(@"Vote Updted...");
            }
            else{
                newVote = [[Vote alloc] init] ;
                newVote.User = backendless.userService.currentUser ;
                newVote.Stars = [NSString stringWithFormat:@"%i",(int)_starRatingView.value] ;
               
                newStar = [[Stars alloc] init];
                
                newStar.stars=newVote.Stars;
                newStar.User=self.selectedBattle.videoOne.User;
            }
            
            if (!_selectedBattle.Video1Votes) {
                _selectedBattle.Video1Votes = [[NSMutableArray alloc] init];
            }
            [_selectedBattle.Video1Votes addObject:newVote];
            
            
            id<IDataStore> dataStore = [backendless.persistenceService of:[Battle class]];
            [dataStore save:_selectedBattle response:^(id savedBattle) {
                NSLog(@"Vote Saved...");
                _selectedBattle = savedBattle;
                
                //saving stars
                id<IDataStore> dataStore2 = [backendless.persistenceService of:[Stars class]];

                  [dataStore2 save:newStar response:^(id newStar) {
                      
                      NSLog(@"stars Saved...");
                      
                      BackendlessUser *videoOneUser = self.selectedBattle.videoOne.User;
                      NSString *currentStarsStr = [videoOneUser getProperty:@"Stars"];
                      NSString *thisMonthStarsStr = [videoOneUser getProperty:@"starsThisMonth"];
                      
                      // Total Stars
                      Stars *updatedStars = newStar;
                      NSString *newStars = updatedStars.stars;
                      NSString *totalUserStart = [NSString stringWithFormat:@"%ld",[currentStarsStr integerValue] + [newStars integerValue]];
                      NSLog(@"stars : %@",totalUserStart );
                      
                      
                      // Stars This month
                      NSString *starsThisMonth = [NSString stringWithFormat:@"%ld",[thisMonthStarsStr integerValue] + [newStars integerValue]];
                      NSLog(@"stars : %@",starsThisMonth );
                      
                      
                      
                      [videoOneUser setProperty:@"Stars" object:totalUserStart];
                      [videoOneUser setProperty:@"starsThisMonth" object:starsThisMonth];
                      
                      id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
                      [dataStore save:videoOneUser responder:nil];
                      
                      
                      
                  } error:^(Fault *error) {
                       NSLog(@"stars not Saved...");
                  }];
                
                [self checkForVideo2Stars];
                
            } error:^(Fault *error) {
                NSLog(@"Error in Vote Saving...%@", error.description);
                [self checkForVideo2Stars];
            }];
        }
        else{
            [self checkForVideo2Stars];
        }
    }
    else{
        [SVProgressHUD dismiss];
        [self performSegueWithIdentifier:@"votesToVideoFeeds" sender:self];
    }
}
- (IBAction)videpTwoPlayAction:(id)sender {
    self.selectedVideo=self.selectedBattle.videoTwo;
    [self performSegueWithIdentifier:@"voteToVideoPlayerViewController" sender:self];
}
- (IBAction)videoOnePlayAction:(id)sender {
    self.selectedVideo=self.selectedBattle.videoOne;
    
    [self performSegueWithIdentifier:@"voteToVideoPlayerViewController" sender:self];
}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender1 {
    NSLog(@"Rating of First Video %.1f", sender1.value);
    Vote *oldVote ;
    NSString *searchString = backendless.userService.currentUser.objectId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.User.objectId contains %@", searchString];
    NSArray *filteredArray = [_selectedBattlePresistance.Video1Votes filteredArrayUsingPredicate:predicate];
    if (filteredArray.count) {
        oldVote = [filteredArray objectAtIndex:0] ;

        int newValue;
        if ([oldVote.Stars intValue] > sender1.value) {
            newValue = [_selectedBattlePresistance.TotalVotes intValue] - ([oldVote.Stars intValue] - sender1.value) ;
        }
        else if ([oldVote.Stars intValue] < sender1.value) {
            newValue = [_selectedBattlePresistance.TotalVotes intValue] + (sender1.value - [oldVote.Stars intValue]) ;
        }
        else{
            newValue = [_selectedBattlePresistance.TotalVotes intValue] ;
        }
        
        Vote *newVote = oldVote ;
        newVote.Stars = [NSString stringWithFormat:@"%f",sender1.value];
        
       /* _selectedBattle.TotalVotes = [NSString stringWithFormat:@"%i",newValue];
        [_circulerProgressBarView setValue:newValue];
        _lblVotesOutOfTotal.text=[NSString stringWithFormat:@"%d of 1000 Votes " , newValue];
        [_selectedBattlePresistance.Video1Votes replaceObjectAtIndex:[_selectedBattlePresistance.Video1Votes indexOfObject:oldVote] withObject:newVote];*/
    }
    else{
        
    }
    // [self performSelector:@selector(disableVoting) withObject:nil afterDelay:10];
}
- (IBAction)didChangeValue2:(HCSStarRatingView *)sender2 {
    NSLog(@"Rating of second Video  %.1f", sender2.value);
    Vote *oldVote ;
    NSString *searchString = backendless.userService.currentUser.objectId;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.User.objectId contains %@", searchString];
    NSArray *filteredArray = [_selectedBattlePresistance.Video2Votes filteredArrayUsingPredicate:predicate];
    if (filteredArray.count) {
        oldVote = [filteredArray objectAtIndex:0] ;
        
        int newValue;
        if ([oldVote.Stars intValue] > sender2.value) {
            newValue = [_selectedBattlePresistance.TotalVotes intValue] - ([oldVote.Stars intValue] - sender2.value) ;
        }
        else if ([oldVote.Stars intValue] < sender2.value) {
            newValue = [_selectedBattlePresistance.TotalVotes intValue] + (sender2.value - [oldVote.Stars intValue]) ;
        }
        else{
            newValue = [_selectedBattlePresistance.TotalVotes intValue] ;
        }
        Vote *newVote = oldVote ;
        newVote.Stars = [NSString stringWithFormat:@"%f",sender2.value];
       /* _selectedBattle.TotalVotes = [NSString stringWithFormat:@"%i",newValue];
        [_circulerProgressBarView setValue:newValue];
        _lblVotesOutOfTotal.text=[NSString stringWithFormat:@"%d of 1000 Votes " , newValue];
        [_selectedBattlePresistance.Video2Votes replaceObjectAtIndex:[_selectedBattlePresistance.Video2Votes indexOfObject:oldVote] withObject:newVote];*/

    }
 //[self performSelector:@selector(disableVoting2) withObject:nil afterDelay:10];//5sec
}
- (IBAction)plusVoterAction:(id)sender {
}

-(void)viewWillDisappear:(BOOL)animated{
}

- (IBAction)userClicked:(id)sender {
    
    UIButton *button =(UIButton *)sender ;
    
    if (button.tag == 1) {
        NSLog(@"User clicked is %@", self.selectedBattle.videoOne.User.name);
        self.selectedUser = self.selectedBattle.videoOne.User;
    }
    if (button.tag == 2) {
        NSLog(@"User clicked is %@", self.selectedBattle.videoTwo.User.name);
        self.selectedUser = self.selectedBattle.videoTwo.User;
    }
    [self performSegueWithIdentifier:@"VotesToUserProfile" sender:self] ;

    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"VotesToUserProfile"])
    {
        // Get reference to the destination view controller
        UserProfileViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        vc.selectedUser = self.selectedUser ;
        vc.previousController = @"VoteViewController";

    }
    
    if ([segue.identifier isEqualToString:@"voteToVotersList"]) {
        VotersListViewController *vc = segue.destinationViewController ;
        vc.selectedList = self.uniqueEntries ;
        vc.TitleString=@"Votters";
    }
    
    if ([segue.identifier isEqualToString:@"voteToVideoPlayerViewController"]) {
        VideoPlayViewController *vc = segue.destinationViewController ;
        vc.selectedVideo = self.selectedVideo ;
        vc.previousController = @"VoteViewController";
    }
}
- (IBAction)unwindToVotes:(UIStoryboardSegue*)sender
{
    
}
- (IBAction)shareFbAction:(UIButton *)sender {
    FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
    content.contentURL = [NSURL URLWithString:self.selectedBattle.videoOne.VideoURL];
    [FBSDKShareDialog showFromViewController:self withContent:content delegate:nil] ;
    
}

- (IBAction)shareTwitterAction:(UIButton *)sender {
    
    if([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        
        SLComposeViewController *twitterController=[SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        
        // set up a completion handler (optional)
        SLComposeViewControllerCompletionHandler __block completionHandler=^(SLComposeViewControllerResult result){
            
            [twitterController dismissViewControllerAnimated:YES completion:nil];
            
            switch(result){
                case SLComposeViewControllerResultCancelled:
                default:
                    break;
                case SLComposeViewControllerResultDone:
                    break;
            }};
        
        
        [twitterController setInitialText:@"Look at this awesome website for aspiring iOS Developers!"];
        [twitterController addURL:[NSURL URLWithString:self.selectedBattle.videoOne.VideoURL]];
        [twitterController setCompletionHandler:completionHandler];
        [self presentViewController:twitterController animated:YES completion:nil];
    }
    
    else{
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Cannot Share" andText:[NSString stringWithFormat:@"You need Twitter app installed and have to be logged in to it to be able to Share to Twitter."] andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
        
        NSLog(@"you are not logged in ");
    }

    
}

- (IBAction)shareGoogleAction:(id)sender {
    // Construct the Google+ share URL
    NSURLComponents* urlComponents = [[NSURLComponents alloc]
                                      initWithString:@"https://plus.google.com/share"];
    urlComponents.queryItems = @[[[NSURLQueryItem alloc]
                                  initWithName:@"url"
                                  value:[[NSURL URLWithString:self.selectedBattle.videoOne.VideoURL] absoluteString]]];
    NSURL* url = [urlComponents URL];
    
    if ([SFSafariViewController class]) {
        // Open the URL in SFSafariViewController (iOS 9+)
        SFSafariViewController* controller = [[SFSafariViewController alloc]
                                              initWithURL:url];
        controller.delegate = self;
        [self presentViewController:controller animated:YES completion:nil];
    } else {
        // Open the URL in the device's browser
        [[UIApplication sharedApplication] openURL:url];
    }
}

- (IBAction)sharePinterestAction:(id)sender {
    [PDKPin pinWithImageURL:[NSURL URLWithString:@"https://about.pinterest.com/sites/about/files/logo.jpg"]
                       link:[NSURL URLWithString:self.selectedBattle.videoOne.VideoURL]
         suggestedBoardName:@"OutBeat"
                       note:@"Check out the OutBeat from Appstore Today!"
         fromViewController:self
                withSuccess:^
     {
         //weakSelf.resultLabel.text = [NSString stringWithFormat:@"successfully pinned pin"];
     }
                 andFailure:^(NSError *error)
     {
         //weakSelf.resultLabel.text = @"pin it failed";
     }];
}

- (IBAction)shareEmailAction:(id)sender {
    emailPopUpView = [[EmailPopUp alloc] initWithNibName:@"EmailPopUp" bundle:nil];
    
    emailPopUpView.view.frame = CGRectMake(0, self.view.frame.origin.y, self.view.frame.size.width- 50, self.view.frame.size.height * 0.4 );
    
    
    [self presentPopupViewController:emailPopUpView animated:YES completion:^(void) {
        
        [emailPopUpView.shareBtn addTarget:self action:@selector(dismissPopupWithEmail) forControlEvents:UIControlEventTouchUpInside];
        [emailPopUpView.closeBtn addTarget:self action:@selector(dismissPopup) forControlEvents:UIControlEventTouchUpInside];
        
    }];
}
- (void)dismissPopupWithEmail {
    if (self.popupViewController != nil) {
        [self dismissPopupViewControllerAnimated:YES completion:^{
            SendGrid *sendgrid = [SendGrid apiUser:@"ibuildx" apiKey:@"umer7447"];
            SendGridEmail *email = [[SendGridEmail alloc] init];
            email.to = emailPopUpView.phoneTxt.text;
            email.from = @"info@ibuildx.com";
            email.subject = @"OutBeat";
            email.text = [NSString stringWithFormat:@"Checkout the OutBeat from Appstore. %@" , self.selectedBattle.videoOne.VideoURL];
            [sendgrid sendWithWeb:email successBlock:^(id responseObject) {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Sent" andText:@"Your Share has been sent." andCancelButton:NO forAlertType:AlertSuccess withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                }];
                alert.cornerRadius = 3.0f;
                [alert show];
            } failureBlock:^(NSError *error) {
                [SVProgressHUD dismiss];
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initFadeAlertWithTitle:@"Failure!" andText:@"Your Email Cannot be sent." andCancelButton:NO forAlertType:AlertFailure withCompletionHandler:^(AMSmoothAlertView *alertView, UIButton *btton) {
                }];
                alert.cornerRadius = 3.0f;
                [alert show];
            }];
            
            NSLog(@"popup view dismissed");
        }];
    }
}
- (IBAction)openVotersListAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"voteToVotersList" sender:self];
}

- (IBAction)user1ProfileClickAction:(id)sender {
    self.selectedUser = self.selectedBattle.videoOne.User;
    [self performSegueWithIdentifier:@"VotesToUserProfile" sender:self];
}

- (IBAction)user2ProfileClickAction:(id)sender {
    self.selectedUser = self.selectedBattle.videoTwo.User;
    [self performSegueWithIdentifier:@"VotesToUserProfile" sender:self];
}
@end
