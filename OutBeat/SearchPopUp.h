//
//  CallUsPopUpView.h
//  Basiligo
//
//  Created by ibuildx on 1/29/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//
#import "TNRadioButtonGroup.h"
#import <UIKit/UIKit.h>
#import "BEMCheckBox.h"

@interface SearchPopUp : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;

@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (nonatomic, strong) TNRadioButtonGroup *radioButtonGroup;
@property (weak, nonatomic) IBOutlet TNRadioButtonGroup *outerView;


@end
