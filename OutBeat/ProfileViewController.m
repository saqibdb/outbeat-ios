//
//  ProfileViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/11/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "ProfileViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
//#import "SVProgressHUD.h"
#import "UIImageView+WebCache.h"
#import "Friends.h"
#import "UIImageView+WebCache.h"
#import "VideoPlayViewController.h"
#import "Follower.h"
#import "RKNotificationHub.h"
#import "Inbox.h"
#import "VotersListViewController.h"
#import "SQBUtility.h"
#import "ClosedBattlesViewController.h"

@interface ProfileViewController (){
    
    
    NSMutableArray *UsersArray;
    NSMutableArray *allUsersArray;
    NSMutableArray *friendRequestArray;
    UIVisualEffectView *visualEffectView;
    RKNotificationHub *hub ;
    bool isFriendBtn;
    BOOL isWinningSelected;
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view layoutIfNeeded];
    
    [_circulerProfile.layer setCornerRadius:_circulerProfile.frame.size.height/2];
    _circulerProfile.clipsToBounds=YES;
    _circulerProfile.layer.borderWidth=2.0;
    _circulerProfile.layer.borderColor=[[UIColor whiteColor] CGColor];
    
    // friends circuler images
    
    [_PlusFriends.layer setCornerRadius:_PlusFriends.frame.size.height/2];
    _PlusFriends.clipsToBounds=YES;
    
    [_image_1.layer setCornerRadius:_image_1.frame.size.height/2];
    _image_1.clipsToBounds=YES;
    _image_1.layer.borderWidth=1.5;
    _image_1.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    
    [_image_2.layer setCornerRadius:_image_2.frame.size.height/2];
    _image_2.clipsToBounds=YES;
    _image_2.layer.borderWidth=1.5;
    _image_2.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_3.layer setCornerRadius:_image_3.frame.size.height/2];
    _image_3.clipsToBounds=YES;
    _image_3.layer.borderWidth=1.5;
    _image_3.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_4.layer setCornerRadius:_image_4.frame.size.height/2];
    _image_4.clipsToBounds=YES;
    _image_4.layer.borderWidth=1.5;
    _image_4.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_5.layer setCornerRadius:_image_5.frame.size.height/2];
    _image_5.clipsToBounds=YES;
    _image_5.layer.borderWidth=1.5;
    _image_5.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_6.layer setCornerRadius:_image_6.frame.size.height/2];
    _image_6.clipsToBounds=YES;
    _image_6.layer.borderWidth=1.5;
    _image_6.layer.borderColor=[[UIColor orangeColor] CGColor];
    // End of  friends circuler images
    
    // followers circuler images
    
    [_image_7.layer setCornerRadius:_image_7.frame.size.height/2];
    _image_7.clipsToBounds=YES;
    _image_7.layer.borderWidth=1.5;
    _image_7.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_8.layer setCornerRadius:_image_8.frame.size.height/2];
    _image_8.clipsToBounds=YES;
    _image_8.layer.borderWidth=1.5;
    _image_8.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_9.layer setCornerRadius:_image_9.frame.size.height/2];
    _image_9.clipsToBounds=YES;
    _image_9.layer.borderWidth=1.5;
    _image_9.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_10.layer setCornerRadius:_image_10.frame.size.height/2];
    _image_10.clipsToBounds=YES;
    _image_10.layer.borderWidth=1.5;
    _image_10.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_11.layer setCornerRadius:_image_11.frame.size.height/2];
    _image_11.clipsToBounds=YES;
    _image_11.layer.borderWidth=1.5;
    _image_11.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_image_12.layer setCornerRadius:_image_12.frame.size.height/2];
    _image_12.clipsToBounds=YES;
    _image_12.layer.borderWidth=1.5;
    _image_12.layer.borderColor=[[UIColor orangeColor] CGColor];
    
    [_plusFollowerAction.layer setCornerRadius:_PlusFriends.frame.size.height/2];
    _PlusFriends.clipsToBounds=YES;
    
    [_followerBtn.layer setCornerRadius:_followerBtn.frame.size.height/2];
    _followerBtn.clipsToBounds=YES;
    [_followerBtn setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:0.2]] ;
    
    // End of  followers circuler images
    
    
    
    
    
    
    

    
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.tabbarShadowView.bounds];
    self.tabbarShadowView.layer.masksToBounds = NO;
    self.tabbarShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabbarShadowView.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    self.tabbarShadowView.layer.shadowOpacity = 0.2f;
    self.tabbarShadowView.layer.shadowPath = shadowPath.CGPath;
    
    _lblAbout.text=[NSString stringWithFormat:NSLocalizedString(@"about", nil)];
    _lblWins.text=[NSString stringWithFormat:NSLocalizedString(@"wins", nil)];
    _lblLost.text=[NSString stringWithFormat:NSLocalizedString(@"Lost", nil)];
    _lblBattles.text=[NSString stringWithFormat:NSLocalizedString(@"Battles", nil)];
    _lblFriends.text=[NSString stringWithFormat:NSLocalizedString(@"Friends", nil)];
    _lblFollowers.text=[NSString stringWithFormat:NSLocalizedString(@"followers", nil)];
    _lblWinningBattles.text=[NSString stringWithFormat:NSLocalizedString(@"winning battles", nil)];
    _lblLostBattles.text=[NSString stringWithFormat:NSLocalizedString(@"lost Battles", nil)];
}
-(void)viewWillAppear:(BOOL)animated{
    _allFreindsCircles = [[NSMutableArray alloc] initWithObjects:_image_1,
                          _image_2,
                          _image_3,
                          _image_4,
                          _image_5,
                          _image_6, nil] ;
    
    _allFollwersCircles = [[NSMutableArray alloc] initWithObjects:_image_7,
                           _image_8,
                           _image_9,
                           _image_10,
                           _image_11,
                           _image_12, nil] ;
    _allVideoBox = [[NSMutableArray alloc] initWithObjects:
                    _image_WBV_1,
                    _image_WBV_2,
                    _image_WBV_3,
                    _image_WBV_4,
                    _image_WBV_5, nil] ;
    
    [self setCurrentUserInformation];

    
    [SQBUtility refreshCurrentUserWithresponseBlock:^(id object, BOOL status, Fault *error) {
        if (status) {
            
            [self setCurrentUserInformation];
        }
        else if(error){
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Please Try Again Later." andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
    }];
    
}

-(void)setCurrentUserInformation{
    //[self.view layoutIfNeeded];
    [_circulerProfile sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                        placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                 options:SDWebImageRefreshCached];
    
    [_blurImageView sd_setImageWithURL:[NSURL URLWithString:[backendless.userService.currentUser getProperty:@"profileImage"]]
                      placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                               options:SDWebImageRefreshCached completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                                   UIVisualEffect *blurEffect;
                                   blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
                                   [visualEffectView removeFromSuperview];
                                   visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
                                   
                                   visualEffectView.frame = _blurImageView.bounds;
                                   [_blurImageView addSubview:visualEffectView];
                               }];
    /*
    [self hideObjects];
    [self fetchVideos];
    [self fetchingChallengeVideos ];
    */
    
    
    UIBezierPath *shadowPath = [UIBezierPath bezierPathWithRect:self.tabbarShadowView.bounds];
    self.tabbarShadowView.layer.masksToBounds = NO;
    self.tabbarShadowView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.tabbarShadowView.layer.shadowOffset = CGSizeMake(0.0f, -1.0f);
    self.tabbarShadowView.layer.shadowOpacity = 0.2f;
    self.tabbarShadowView.layer.shadowPath = shadowPath.CGPath;
    
    self.userNameLabel.text = [backendless.userService.currentUser getProperty:@"firstName"];
    self.homeAddressLabel.text = [backendless.userService.currentUser getProperty:@"homeAddress"];
    self.aboutMeText.text = [backendless.userService.currentUser getProperty:@"aboutMe"];
    
    [self setupFunctionsCall];
    
    /*
    [self updateFriendsCircle];
    [self updateFollwersCircle];
    [self.view layoutIfNeeded];
    */
}
-(void)viewDidAppear:(BOOL)animated {
    
    [self.view layoutIfNeeded];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"Reciever.objectId = '%@' AND isSeen = 'NO'", backendless.userService.currentUser.objectId];
    
    NSNumber *pageSize = [NSNumber numberWithInt:1];
    [query.queryOptions setPageSize:pageSize];
    if (!hub) {
        hub = [[RKNotificationHub alloc] initWithView:self.messageBtn];
        [hub setCircleColor:[UIColor redColor] labelColor:[UIColor whiteColor]];
    }
    [hub decrementBy:0];
    [hub setCircleAtFrame:CGRectMake(self.messageBtn.frame.size.width - 7.5, -7.5, 15, 15)];
    
    [backendless.persistenceService find:[Inbox class] dataQuery:query response:^(BackendlessCollection *collection1)  {
        NSLog(@"Data Found %i", [[collection1 getTotalObjects] intValue]);
        if([[collection1 getTotalObjects] intValue] == 0)
        {
            [hub hideCount];
            [hub setCircleColor:[UIColor clearColor] labelColor:[UIColor clearColor]];
            [hub setCount:[[collection1 getTotalObjects] intValue]];
        }
        else
        {
            [hub showCount];
            [hub setCount:[[collection1 getTotalObjects] intValue]];
            
            
            int randomIndex = arc4random() % 3 ; // gives no .between 1 to 15 ..
            
            switch (randomIndex)
            {
                case 0 :
                    [hub setCircleColor:[UIColor blueColor] labelColor:[UIColor whiteColor]];
                    break;
                case 1:
                    [hub setCircleColor:[UIColor blackColor] labelColor:[UIColor whiteColor]];
                    break;
                case 2:
                    [hub setCircleColor:[UIColor redColor] labelColor:[UIColor whiteColor]];
                    break;
                    
            }
            
            
            [hub pop];
        } 
        
    } error:^(Fault *error) {
        NSLog(@"Error Found %@",error.description);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)fromProfileViewController:(NSObject*)something
{
    
}

-(void)setupFunctionsCall {
    
    [self hideObjects];
    [self fetchVideos];
    [self fetchingChallengeVideos ];
    
    [self updateFriendsCircle];
    [self updateFollwersCircle];
}

#pragma mark - Actions
- (IBAction)menueBtnAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"ToSettings" sender:self];
}

- (IBAction)PlusFriendsAction:(id)sender {
    isFriendBtn=YES;
    [self performSegueWithIdentifier:@"profileToUserList" sender:self];
}

- (IBAction)plusFollowerAction:(id)sender {
    isFriendBtn=NO;
    [self performSegueWithIdentifier:@"profileToUserList" sender:self];
}

- (IBAction)plusFollowers:(id)sender {
    
}

- (IBAction)addFriendsAction:(id)sender {
    
    [self performSegueWithIdentifier:@"profileToFriends" sender:self];
}
- (IBAction)addFollowersAction:(id)sender {
    
    [self performSegueWithIdentifier:@"profileToFollowers" sender:self];
}

- (IBAction)MessageBtnAction:(UIButton *)sender {
    /*AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Under construction" andText:@"The Page is Currently under construction" andCancelButton:NO forAlertType:AlertInfo];
     
     [alert show];*/
    [self performSegueWithIdentifier:@"ToMessages" sender:self];
}
- (IBAction)unwindToProfile:(UIStoryboardSegue*)sender
{
    
}

-(void)updateFriendsCircle {
    [SQBUtility getAllFrirendsOfUser:backendless.userService.currentUser responseBlock:^(id object, BOOL status, Fault *error) {
        if (status) {
            self.friendsRedirectsBtn.hidden = NO ;
            _friendsArray = [[NSMutableArray alloc] initWithArray:(NSArray *)object] ;
            for (int i = 0 ; i < _friendsArray.count; i++) {
                if (i < 6) {
                    UIImageView *circleImageView = _allFreindsCircles[i];
                    [circleImageView setHidden:NO] ;
                    [circleImageView sd_setImageWithURL:[NSURL URLWithString:[_friendsArray[i] valueForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"] options:SDWebImageRefreshCached];
                }
                else{
                    int plusFriends = i - 5;
                    NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;
                    [_PlusFriends setTitle:btnTitle forState:UIControlStateNormal] ;
                    [_PlusFriends setHidden:NO];
                }
            }
        }
        else{
            if (error) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Error Fetching Friends" andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
            else{
                self.friendsRedirectsBtn.hidden = YES ;
            }
        }
    }];
}

-(void)updateFollwersCircle {
    [SQBUtility getAllFollowersOfUser:backendless.userService.currentUser responseBlock:^(id object, BOOL status, Fault *error) {
        if (status) {
            self.followersRedirectsBtn.hidden = NO ;
            _followrsArray = [[NSMutableArray alloc] initWithArray:(NSArray *)object] ;
            for (int i = 0 ; i < _followrsArray.count; i++) {
                if (i < 6) {
                    UIImageView *circleImageView = _allFollwersCircles[i];
                    [circleImageView setHidden:NO] ;
                    [circleImageView sd_setImageWithURL:[NSURL URLWithString:[_followrsArray[i] valueForKey:@"profileImage"]] placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"] options:SDWebImageRefreshCached];
                    [_plusFollowerAction setHidden:YES];
                    [_plusFollowerAction setTitle:@"+0" forState:UIControlStateNormal] ;
                }
                else{
                    int plusFriends = i - 5;
                    NSString *btnTitle = [NSString stringWithFormat:@"+%i" , plusFriends] ;
                    [_plusFollowerAction setTitle:btnTitle forState:UIControlStateNormal] ;
                }
            }
        }
        else{
            if (error) {
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error" andText:@"Error Fetching Followers" andCancelButton:NO forAlertType:AlertFailure];
                [alert show];
            }
            else{
                self.followersRedirectsBtn.hidden = YES ;
            }
            
        }
    }] ;
}

-(void)hideObjects{
    
    
    [_image_1 setHidden:YES];
    [_image_2 setHidden:YES];
    [_image_3 setHidden:YES];
    [_image_4 setHidden:YES];
    [_image_5 setHidden:YES];
    [_image_6 setHidden:YES];
    [_PlusFriends setHidden:YES];
    [_image_7 setHidden:YES];
    [_image_8 setHidden:YES];
    [_image_9 setHidden:YES];
    [_image_10 setHidden:YES];
    [_image_11 setHidden:YES];
    [_image_12 setHidden:YES];
    [_plusFollowerAction setHidden:YES];
    
    [_LBV_1 setHidden:YES];
    [_LBV_2 setHidden:YES];
    [_LBV_3 setHidden:YES];
    [_LBV_4 setHidden:YES];
    [_LBV_5 setHidden:YES];
    [_btn_LBV setHidden:YES];
    
    
}
-(void)fetchVideos{
    
    
    [_WBV_1 setHidden:YES];
    [_WBV_2 setHidden:YES];
    [_WBV_3 setHidden:YES];
    [_WBV_4 setHidden:YES];
    [_WBV_5 setHidden:YES];
    [_btn_WBV setHidden:YES];
    
    
    BackendlessDataQuery *query1 = [BackendlessDataQuery query];
    query1.whereClause = [NSString stringWithFormat:@"User.objectId = '%@' ",backendless.userService.currentUser.objectId];
    
    id<IDataStore> dataStore2 = [backendless.persistenceService of:[Video class]];
    [dataStore2 find:query1 response:^(BackendlessCollection *allWinningBattleVideos) {
        NSLog(@"winning battles found = %lu",(unsigned long)allWinningBattleVideos.data.count);
        self.vidsFound = [[NSMutableArray alloc] initWithArray:allWinningBattleVideos.data];
        
        _BattleVidsFound = [[NSMutableArray alloc] init];
        _lostVidsFound = [[NSMutableArray alloc] init];
        _wonVidsFound = [[NSMutableArray alloc] init];
        
        for (int i=0; i<allWinningBattleVideos.data.count; i++) {
            
            Video *vid = [allWinningBattleVideos.data objectAtIndex:i];
            
            if (vid.isBattle) {
                [_BattleVidsFound addObject:vid];
                if (vid.isWin) {
                    [_wonVidsFound addObject:vid];
                }
                else{
                    [_lostVidsFound addObject:vid];
                }
            }
        }
        
        [_winNumberTxt setText:[NSString stringWithFormat:@"%lu", (unsigned long)_wonVidsFound.count]] ;
        [_lostNumberTxt setText:[NSString stringWithFormat:@"%lu", (unsigned long)_lostVidsFound.count]] ;
        [_battlesNumberTxt setText:[NSString stringWithFormat:@"%lu", (unsigned long)_BattleVidsFound.count]] ;
        
    } error:^(Fault *error) {
        NSLog(@"Error Found at Video Gather %@",error.detail);
    }];
}
-(void)fetchingChallengeVideos{
    
    self.ChellengeVideosArray=[[NSMutableArray alloc]init];
    self.BattleVideosArray=[[NSMutableArray alloc]init];
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    query.whereClause = [NSString stringWithFormat:@"User.objectId = '%@' ",backendless.userService.currentUser.objectId];
    id<IDataStore> dataStore = [backendless.persistenceService of:[Video class]];
    [dataStore find:query response:^(BackendlessCollection *ChellengeVideoCollection) {
        
        
        self.AllVideosArray =[[NSMutableArray alloc]initWithArray:ChellengeVideoCollection.data];
        
        for(Video *vid in _AllVideosArray){
        
            if([vid.Category isEqualToString:@"Challenge"])
            {
            
                [self.ChellengeVideosArray addObject:vid];
                NSLog(@"challenge vids found = %lu" ,(unsigned long)self.ChellengeVideosArray.count);
            
            }
            else{
            
                [self.BattleVideosArray addObject:vid];
                NSLog(@"Battle vids found = %lu" ,(unsigned long)self.BattleVideosArray.count);
            }
        }
        
            //setting video thumnials and views
        
        
        for (int i = 0; i < self.BattleVideosArray.count; i++) {
            
            self.winningBattleAllBtn.hidden = NO;
            Video *vid = [self.BattleVideosArray objectAtIndex:i];
            if(i==0){
                self.winningBattleAllBtn.hidden = YES;
                [_WBV_1 setHidden:NO];
                [_image_WBV_1 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i==1){
                
                
                [_WBV_2 setHidden:NO];
                [_image_WBV_2 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i==2){
                [_WBV_3 setHidden:NO];
                [_image_WBV_3 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            
            else if(i==3){
                [_WBV_4 setHidden:NO];
                [_image_WBV_4 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            
            else if (i==4){
                [_WBV_5 setHidden:NO];
                [_image_WBV_5 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i>4){
                
                [_btn_WBV setHidden:NO];
                
                NSString *result = [NSString stringWithFormat:@"+%lu",(unsigned long)self.BattleVideosArray.count-5];
                
                [_plus_btn_WBV setTitle:result forState:UIControlStateNormal];
                
                
            }
        }
        
        
        for (int i = 0; i < self.ChellengeVideosArray.count; i++) {
            Video *vid = [self.ChellengeVideosArray objectAtIndex:i];
            self.losingBattleAllBtn.hidden = NO;

            if(i==0){
                self.losingBattleAllBtn.hidden = YES;
                [_LBV_1 setHidden:NO];
                [_image_LBV_1 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i==1){
                
                
                [_LBV_2 setHidden:NO];
                [_image_LBV_2 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i==2){
                [_LBV_3 setHidden:NO];
                [_image_LBV_3 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            
            else if(i==3){
                [_LBV_4 setHidden:NO];
                [_image_LBV_4 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            
            else if (i==4){
                [_LBV_5 setHidden:NO];
                [_image_LBV_5 sd_setImageWithURL:[NSURL URLWithString:vid.ThumbnailURL]];
            }
            else if(i>4){
                
                [_btn_LBV setHidden:NO];
                
                               NSString *result = [NSString stringWithFormat:@"+%lu",(unsigned long)_lostVidsFound.count-5];
                
                                //[_plus_btn_ setTitle:result forState:UIControlStateNormal];
                
                
            }
        }

           } error:^(Fault *error) {
        
        
    }];
}

- (IBAction)playVideoAction:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:0] ;
    self.selectedVideo = vid ;
    
    [self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_2:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:1] ;
    self.selectedVideo = vid ;
    
    [self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_3:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:2] ;
    self.selectedVideo = vid ;
    
    [self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_4:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:3] ;
    self.selectedVideo = vid ;
    
    [self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_5:(id)sender {
    Video *vid = [self.vidsFound objectAtIndex:4] ;
    self.selectedVideo = vid ;
    
    [self performSegueWithIdentifier:@"ToVideoPaybackFromProfile" sender:self];
}

- (IBAction)playVideoAction_6:(id)sender {
}

- (IBAction)playVideoAction_7:(id)sender {
}

- (IBAction)playVideoAction_8:(id)sender {
}

- (IBAction)playVideoAction_9:(id)sender {
}

- (IBAction)playVideoAction_10:(id)sender {
}

- (void)fromChallengersViewController:(NSObject*)something {
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    if ([segue.identifier isEqualToString:@"ToVideoPaybackFromProfile"]) {
        VideoPlayViewController *vc = segue.destinationViewController ;
        vc.selectedVideo = self.selectedVideo ;
    }
    if ([segue.identifier isEqualToString:@"profileToUserList"]) {
        
        if(isFriendBtn){
        VotersListViewController *vc = segue.destinationViewController ;
            //vc.votersList = self.friendArray1 ;
            //vc.delegate=self;
            NSString *Friends = [NSString stringWithFormat:NSLocalizedString(@"Friends", nil)];
            vc.TitleString=Friends;
            vc.selectedList = _friendsArray ;
        }
        else{
            VotersListViewController *vc = segue.destinationViewController ;
            //vc.votersList = self.followrsArray1 ;
            //vc.delegate=self;
            NSString *Followers = [NSString stringWithFormat:NSLocalizedString(@"followers", nil)];
            vc.TitleString=Followers;
            vc.selectedList=_followrsArray;
        }
    }
    if ([segue.identifier isEqualToString:@"ToClosedBattles"]) {
        ClosedBattlesViewController *vc = segue.destinationViewController ;
        if (isWinningSelected) {
            vc.videos = self.BattleVideosArray;
        }
        else{
            vc.videos = self.ChellengeVideosArray;
        }
    }
}

- (IBAction)friendsRedirects:(id)sender {
    isFriendBtn=YES;
    [self performSegueWithIdentifier:@"profileToUserList" sender:self];
}

- (IBAction)followersRedirects:(id)sender {
    isFriendBtn=NO;
    [self performSegueWithIdentifier:@"profileToUserList" sender:self];
}
- (IBAction)plusWinningBattlesAction:(id)sender {
    isWinningSelected = YES;
    [self performSegueWithIdentifier:@"ToClosedBattles" sender:self];
}

- (IBAction)plusLosingBattlesAction:(id)sender {
    isWinningSelected = NO;
    [self performSegueWithIdentifier:@"ToClosedBattles" sender:self];
}
@end
