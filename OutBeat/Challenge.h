//
//  Challenge.h
//  outbeat
//
//  Created by iBuildx_Mac_Mini on 9/22/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Video.h"
#import "Backendless.h"
#import "Comments.h"


@interface Challenge : NSObject


@property (nonatomic, strong) Video *MainVideo;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSMutableArray *OtherVideos;
@property (nonatomic, strong) NSMutableArray *Comments;
@property (nonatomic, strong) NSString *Views;
@property (nonatomic, strong) NSString *ChallengeLimit;





@end
