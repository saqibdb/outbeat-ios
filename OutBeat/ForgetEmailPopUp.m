//
//  CallUsPopUpView.m
//  Basiligo
//
//  Created by ibuildx on 1/29/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "ForgetEmailPopUp.h"
#import "SVProgressHUD.h"
#define grayTextColor [UIColor colorWithRed:127.0/255.0 green:127.0/255.0 blue:127.0/255.0 alpha:0.5]


@interface ForgetEmailPopUp () <UITextViewDelegate>

@end

@implementation ForgetEmailPopUp

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
  
    
    [[self.borderView layer] setBorderWidth:2.0f];
    [[self.borderView layer] setBorderColor:grayTextColor.CGColor];
    self.borderView.layer.cornerRadius = 5;
    self.borderView.clipsToBounds = YES;
    
    
    }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma - textView Delegate



#pragma mark - Actions


- (IBAction)shareAction:(UIButton *)sender {
    //[self.shareBtn sendActionsForControlEvents:UIControlEventTouchUpInside];

}
@end
