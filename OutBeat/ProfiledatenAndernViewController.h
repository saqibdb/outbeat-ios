//
//  ProfiledatenAndernViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/11/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"
@interface ProfiledatenAndernViewController : UIViewController <UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate>


@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *roundedProfileImage;
@property (weak, nonatomic) IBOutlet UITextField *txtUserName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtHome;
@property (weak, nonatomic) IBOutlet UITextField *txtAboutMe;
@property (weak, nonatomic) IBOutlet UITextField *txtFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtLastName;
@property (weak, nonatomic) IBOutlet UIButton *profileBtnImage;
@property (strong, nonatomic, readonly) BackendlessUser *currentUser;
@property (weak, nonatomic) IBOutlet UILabel *lblProfile;

- (IBAction)imagePickerAction:(UIButton *)sender;
- (IBAction)changeAction:(id)sender;
- (IBAction)backAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *personalCredView;
@property (weak, nonatomic) IBOutlet UIView *credView;

@end
