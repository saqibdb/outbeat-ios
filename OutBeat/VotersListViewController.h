//
//  VotersListViewController.h
//  OutBeat
//
//  Created by ibuildx on 8/10/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"

//


@interface VotersListViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (strong,nonatomic) NSString *TitleString;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
- (IBAction)backAction:(UIButton *)sender;
@property (strong,nonatomic) NSArray *votersList;
@property (strong,nonatomic) NSArray *selectedList;
@property (strong,nonatomic) BackendlessUser *selectedVoter;

@end
