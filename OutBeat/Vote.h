//
//  Vote.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 8/8/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"

@interface Vote : NSObject


@property (nonatomic, strong) NSString *Stars;
@property (nonatomic, strong) BackendlessUser *User;



@end
