//
//  MessagesViewController.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 7/18/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

//#import "ViewController.h"
#import "Inbox.h"

@interface MessagesViewController : UIViewController <UITableViewDelegate , UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UIButton *backBtn;

@property (weak, nonatomic) IBOutlet UITableView *inboxTableView;



@property (strong) NSMutableArray  *uniqueMsgs;
@property (strong) NSMutableArray  *allUniqueMsgs;
@property (strong) NSMutableArray  *users;

@property (strong) Inbox  *selectedInbox;

- (IBAction)backAction:(UIButton *)sender;




@end
