//
//  BattleViewController.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 6/3/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Video.h"
#import "UploadVideoViewController.h"
#import "SWTableViewCell.h"
@interface BattleViewController : UIViewController<UITableViewDelegate , UITableViewDataSource,UploadVideoViewDelegate,SWTableViewCellDelegate>

@property (weak, nonatomic) IBOutlet UILabel *lbloutBeatTitle;
@property (weak, nonatomic) IBOutlet UIButton *menueBtn;
@property (weak, nonatomic) IBOutlet UIButton *openSearchBtn;


@property (weak, nonatomic) IBOutlet UILabel *lblNoResultsFound;

@property (weak, nonatomic) IBOutlet UITableView *videoTableView;
@property (strong) NSMutableArray  *vidsFoundOriginal;
@property (strong) NSMutableArray  *vidsFound;
@property (strong) NSMutableArray  *searchFilters;
@property  id selectedVideo;
@property (weak, nonatomic) IBOutlet UIView *triangleSprite;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn;
@property (weak, nonatomic) IBOutlet UIView *tabbarShadowView;

- (IBAction)searchAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITextField *txtToSearch;
- (IBAction)searchBackAction:(UIButton *)sender;
- (IBAction)searchOkAction:(UIButton *)sender;
- (IBAction)menuAction:(id)sender;


@end
