//
//  VideoPlayViewController.m
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 6/6/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "VideoPlayViewController.h"
#import "Utility.h"

@interface VideoPlayViewController ()

@end

@implementation VideoPlayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.view layoutIfNeeded];
    self.videoLabel.text = self.selectedVideo.Title ;

    
    
    //[addPlayerButton setEnabled:NO];
    //[removePlayerButton setEnabled:YES];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    //[SVProgressHUD showWithStatus:@"Loading Video..."] ;
    NSURL *URL = [NSURL URLWithString:self.selectedVideo.VideoURL];
    [self.playerView setVideoURL:URL];
    [self.playerView prepareAndPlayAutomatically:YES];
    [self.playerView setTintColor:[Utility getOrangeColor]];
    [self.playerView setBufferTintColor:[UIColor grayColor]];
    
    [SVProgressHUD dismiss];
}
-(void)viewWillDisappear:(BOOL)animated {
    [self.playerView clean];
    [SVProgressHUD dismiss];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)backAction:(id)sender {
    if ([_previousController isEqualToString:@"SharingAndComment"]) {
        [self performSegueWithIdentifier:@"backToSharingAndComment" sender:self];
    }
    else if ([_previousController isEqualToString:@"ClosedBattle"]){
        [self performSegueWithIdentifier:@"backToClosed" sender:self];
    }
    else if ([_previousController isEqualToString:@"VoteViewController"]) {
        
        [self performSegueWithIdentifier:@"videoPlayerToVotesVC" sender:self];
    }
    else {
        [self performSegueWithIdentifier:@"backToBattle" sender:self];
    }
}


- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeLeft; // or Right of course
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotate {
    return NO;
}
- (IBAction)vidsliderValueChanged:(UISlider *)sender {
}

- (IBAction)playPauseAction:(UIButton *)sender {
    
}
@end
