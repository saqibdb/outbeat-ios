//
//  Comments.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/27/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"

@interface Comments : NSObject
@property (nonatomic, strong) NSString *Message;
@property (nonatomic, strong) BackendlessUser *User;
@property (nonatomic, strong) NSDate *created;

@end
