//
//  VoteViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/23/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//
#import <MessageUI/MessageUI.h>
#import <UIKit/UIKit.h>
#import "Battle.h"
#import "MBCircularProgressBarView.h"
#import "Video.h"
#import "SWTableViewCell.h"
#import "TNRectangularRadioButton.h"
@interface VoteViewController : UIViewController <UITableViewDataSource ,UITableViewDelegate,SWTableViewCellDelegate,MFMailComposeViewControllerDelegate , SFSafariViewControllerDelegate>
- (IBAction)backAction:(id)sender;
//first user
@property (weak, nonatomic) IBOutlet UIImageView *thumnailOne;
- (IBAction)videoOnePlayAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalVotesFirst;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleFirst;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageFirst;
@property (weak, nonatomic) IBOutlet UILabel *userNameFirst;
@property (weak, nonatomic) IBOutlet UILabel *totalWinsFirst;
@property (strong) NSMutableArray  *searchFilters;
//second

@property (weak, nonatomic) IBOutlet UIImageView *thumnailSecond;
- (IBAction)videpTwoPlayAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalVotesSecond;
@property (weak, nonatomic) IBOutlet UILabel *lblTitleSecond;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageSecond;
@property (weak, nonatomic) IBOutlet UILabel *userNameSecond;
@property (weak, nonatomic) IBOutlet UILabel *totalWinsSecond;
@property (weak, nonatomic) IBOutlet UILabel *lblVotesOutOfTotal;

@property (weak, nonatomic) IBOutlet UILabel *VsSprite;
@property (weak, nonatomic) IBOutlet UIButton *openVotersListBtn;

@property (strong ,nonatomic) NSString *titleFirst ;
@property (strong ,nonatomic) NSString *titleSecond ;
@property (strong ,nonatomic) NSString *votesFirst ;
@property (strong ,nonatomic) NSString *votesSecond ;
@property (strong ,nonatomic) NSString *winsFirst ;
@property (strong ,nonatomic) NSString *winsSecond ;
@property (strong ,nonatomic) NSString *userName_First ;
@property (strong ,nonatomic) NSString *userName_Second ;
@property (strong,nonatomic) NSMutableArray *allBattles;
@property (strong,nonatomic) NSMutableArray *_allVotersCircles;
@property (strong,nonatomic) NSMutableArray *commentUser;
@property (weak, nonatomic) IBOutlet UITableView *commentsTableView;
@property (weak, nonatomic) IBOutlet MBCircularProgressBarView *circulerProgressBarView;
@property (strong,nonatomic) NSArray *uniqueEntries;
//voters
@property (weak, nonatomic) IBOutlet UIImageView *voter_1;
@property (weak, nonatomic) IBOutlet UIImageView *voter_2;
@property (weak, nonatomic) IBOutlet UIImageView *voter_3;
@property (weak, nonatomic) IBOutlet UIImageView *voter_4;
@property (weak, nonatomic) IBOutlet UIImageView *voter_5;
@property (weak, nonatomic) IBOutlet UIImageView *voter_6;
@property (weak, nonatomic) IBOutlet UIButton *plusVoters;
- (IBAction)plusVoterAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblViewers;
@property (strong) Video *selectedVideo;
@property (strong) Battle  *selectedBattle;
@property BOOL isSelfBattle;
@property (strong) Battle  *selectedBattlePresistance;
@property (strong) BackendlessUser  *selectedUser ;




@property (weak, nonatomic) IBOutlet UITextField *commentTextField;







- (IBAction)userClicked:(id)sender;


@property (weak, nonatomic) IBOutlet UIButton *video1Eye;
@property (weak, nonatomic) IBOutlet UIButton *video2Eye;
- (IBAction)shareFbAction:(UIButton *)sender;

- (IBAction)shareTwitterAction:(UIButton *)sender;

- (IBAction)shareGoogleAction:(id)sender;
- (IBAction)sharePinterestAction:(id)sender;
- (IBAction)shareEmailAction:(id)sender;






- (IBAction)openVotersListAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblShare;
@property (weak, nonatomic) IBOutlet UILabel *lblComments;
@property (weak, nonatomic) IBOutlet UILabel *actualScore1;
@property (weak, nonatomic) IBOutlet UILabel *actualScore2;
@property (weak, nonatomic) IBOutlet UIView *actualscoreView1;
@property (weak, nonatomic) IBOutlet UIView *actualscoreView2;
@property (weak, nonatomic) IBOutlet UIView *frownView1;
@property (weak, nonatomic) IBOutlet UIView *frownView2;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sameBottom;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalSpace;
@property (weak, nonatomic) IBOutlet UIView *dropDownView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *keyboardViewBottomConstraint;
- (IBAction)user1ProfileClickAction:(id)sender;
- (IBAction)user2ProfileClickAction:(id)sender;


@end
