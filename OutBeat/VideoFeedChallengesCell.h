//
//  VideoFeedChallengesCell.h
//  OutBeat
//
//  Created by ibuildx on 8/18/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoFeedChallengesCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *videoThumnail;
@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UIButton *playVideoBtn;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UIButton *btnSupport;
@property (weak, nonatomic) IBOutlet UIButton *btnType;
@property (weak, nonatomic) IBOutlet UILabel *lblVidTitle;

@property (strong, nonatomic) IBOutletCollection(UIView) NSArray *TileViews;

@property (strong, nonatomic) IBOutletCollection(UIImageView) NSArray *TileViewImages;

@end
