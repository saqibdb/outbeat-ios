//
//  Battle.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/23/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Video.h"
#import "Backendless.h"
#import "Vote.h"



@interface Battle : NSObject

@property (nonatomic, strong) Video *videoOne;
@property (nonatomic, strong) Video *videoTwo;
@property (nonatomic, strong) NSString *videoOneWins;
@property (nonatomic, strong) NSString *videoTwoWins;
@property (nonatomic, strong) NSString *videoOneVotes;
@property (nonatomic, strong) NSString *videoTwoVotes;
@property (nonatomic, strong) NSString *videoOneTitle;
@property (nonatomic, strong) NSString *videoTwoTitle;
@property (nonatomic, strong) NSString *Views;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSMutableArray *Voters;


@property (nonatomic, strong) NSMutableArray *Comments;

@property (nonatomic, strong) NSMutableArray *Video1Votes;
@property (nonatomic, strong) NSMutableArray *Video2Votes;

@property (nonatomic, strong) NSString *TotalVotes;




@end
