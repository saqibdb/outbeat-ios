//
//  Video.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 6/2/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"


@interface Video : NSObject
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSString *VideoURL;
@property (nonatomic, strong) BackendlessUser *User;
@property (nonatomic, strong) NSString *Title;
@property (nonatomic, strong) NSString *ThumbnailURL;
@property (nonatomic, strong) NSString *Tags;
@property (nonatomic, strong) NSString *Type;
@property (nonatomic, strong) NSString *Category;
@property(nonatomic) BOOL isBattle;
@property(nonatomic) BOOL alreadyBattle;
@property(nonatomic) BOOL isWin;
@property (nonatomic, strong) NSString *ChallengersAmount;



@end
