//
//  ViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/6/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

- (IBAction)signUpWithFbAction:(id)sender;

- (IBAction)getStartedAction:(UIButton *)sender;

- (IBAction)signInAction:(UIButton *)sender;

@property (weak, nonatomic) IBOutlet UILabel *lblAlreadHaveAccount;

@property (weak, nonatomic) IBOutlet UIButton *btnSignIn;

@property (weak, nonatomic) IBOutlet UILabel *lblPlayTheGame;

@property (weak, nonatomic) IBOutlet UILabel *lblOr;

@property (weak, nonatomic) IBOutlet UILabel *lblSignInWith;




@end

