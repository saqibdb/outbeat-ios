//
//  NSMutableAttributedString+Color.m
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 8/3/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "NSMutableAttributedString+Color.h"


@implementation NSMutableAttributedString (Color)

-(void)setColorForText:(NSString*) textToFind withColor:(UIColor*) color
{
    NSRange range = [self.mutableString rangeOfString:textToFind options:NSCaseInsensitiveSearch];
    
    if (range.location != NSNotFound) {
        [self addAttribute:NSForegroundColorAttributeName value:color range:range];
    }
}
@end