//
//  FriendsTableViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/2/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//
#import "Friends.h"
#import "FriendsTableViewController.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "FriendsCell.h"
#import "UIImageView+WebCache.h"
#import "Friend_Requests.h"
#import "Utility.h"

@interface FriendsTableViewController (){
    NSMutableArray *allUSers ;
    
}

@end

@implementation FriendsTableViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    self.friendsTableView.delegate = self ;
    self.friendsTableView.dataSource = self ;

    [self fetchUsers];
}

-(void)fetchUsers{
    [SVProgressHUD show];
    allUSers =[[NSMutableArray alloc]init];
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    _friendsArrayIds =[[NSMutableArray alloc]initWithArray:[backendless.userService.currentUser getProperty:@"friends"]] ;
    [dataStore find:nil response:^(BackendlessCollection *collection) {

        
        for(BackendlessUser *user in collection.data){
            if (![user.objectId isEqualToString:backendless.userService.currentUser.objectId]){
                [allUSers addObject:user];
            }
            
            for (int i=0 ;i<_friendsArrayIds.count; i++){
                Friends *friend = _friendsArrayIds[i] ;
                
                
                if ([user.objectId isEqualToString:friend.FriendUserId] && [friend.RequestStatus isEqualToString:@"Accepted"]){
                    [allUSers removeObject:user];
                }
            }
            
        }
        [self.friendsTableView reloadData] ;
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        
        NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];

    
    

    
    
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allUSers.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"FriendsCell";
    FriendsCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[FriendsCell alloc] initWithStyle:UITableViewCellStyleDefault
                                  reuseIdentifier:CellIdentifier];
    }
    
    BackendlessUser *user =[allUSers objectAtIndex:indexPath.row];
    cell.UserName.text=user.name;
    
    
    [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:[user getProperty:@"profileImage"]]
                         placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                  options:SDWebImageRefreshCached];
    [cell.profileImage.layer setCornerRadius:cell.profileImage.frame.size.height/2];
    cell.profileImage.clipsToBounds=YES;
    cell.addFriendAction.tag=indexPath.row;
    
    [cell.addFriendAction addTarget:self action:@selector(addFriendClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    for (Friends *friend in _friendsArrayIds) {
        if ([friend.FriendUserId isEqualToString:user.objectId]) {
            [cell.addFriendAction setTitle:@"Friend request sent." forState:UIControlStateNormal];
            cell.addFriendAction.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:10.0f];
            cell.addFriendAction.userInteractionEnabled = NO ;
            break ;
        }
        else{
            [cell.addFriendAction setTitle:@"Add Friend" forState:UIControlStateNormal];
            cell.addFriendAction.titleLabel.font = [UIFont fontWithName:@"Lato-Regular" size:15.0f];
            cell.addFriendAction.userInteractionEnabled = YES ;
        }
    }
    
    
    
    
    return cell;
    
}


-(void)addFriendClicked:(UIButton*)sender
{
    [SVProgressHUD showWithStatus:@"Sending Request..."];
    BackendlessUser *selectedUser = allUSers[sender.tag] ;
    Friends *newFriendRequest = [[Friends alloc] init];
    newFriendRequest.FriendUserId = [selectedUser getProperty:@"objectId"] ;
    newFriendRequest.RequestStatus = @"NotAccepted" ;
    newFriendRequest.SendingUserId = [backendless.userService.currentUser getProperty:@"objectId"] ;

    BackendlessUser *currentUser = backendless.userService.currentUser ;
    NSMutableArray *newFriendsList = [[NSMutableArray alloc] initWithArray:[currentUser getProperty:@"friends"]] ;
    [newFriendsList addObject:newFriendRequest] ;
    [currentUser setProperty:@"friends" object:newFriendsList] ;
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore save:currentUser response:^(id savedUser) {
        [SVProgressHUD dismiss];
        [sender setTitle:@"Friend request sent." forState:UIControlStateNormal];
        sender.titleLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:10.0f];
        sender.userInteractionEnabled = NO ;
        backendless.userService.currentUser = savedUser ;
        NSString *friendRequestMessage = [NSString stringWithFormat:@"%@ sent you a Friend Request and Waiting for you Response.", backendless.userService.currentUser.name];
        [Utility publishMessageAsPushNotificationAsync:friendRequestMessage forDevice:[selectedUser getProperty:@"deviceId"]] ;
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
    }] ;

}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (IBAction)backAction:(id)sender {
    [self performSegueWithIdentifier:@"toprofilefromfriends" sender:self];
}
@end
