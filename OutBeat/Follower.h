//
//  Follower.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/7/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
@interface Follower : NSObject

@property (nonatomic, strong) NSString *FollowerUserId;
@property (nonatomic, strong) NSString *SendingUserId;


@end
