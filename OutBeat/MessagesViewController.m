//
//  MessagesViewController.m
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 7/18/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "MessagesViewController.h"
#import "MessagesTableViewCell.h"
#import "Inbox.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "UIImageView+WebCache.h"
#import "ChatViewController.h"


@interface MessagesViewController ()

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.inboxTableView setDelegate:self];
    [self.inboxTableView setDataSource:self];
    self.uniqueMsgs = [[NSMutableArray alloc] init];
    self.allUniqueMsgs = [[NSMutableArray alloc] init];

}
- (void)viewWillAppear:(BOOL)animated {
    [self fetchingUsersInboxAsync];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - tableView Delegate methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.uniqueMsgs.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *CellIdentifier = @"MessagesTableViewCell";
    MessagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[MessagesTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                         reuseIdentifier:CellIdentifier];
    }
    [cell layoutIfNeeded] ;
    Inbox *inbox = [self.uniqueMsgs objectAtIndex:indexPath.row];
    
    if ([inbox.Sender.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
        cell.userName.text = inbox.Reciever.name;
        [cell.userAvatar sd_setImageWithURL:[NSURL URLWithString:[inbox.Reciever getProperty:@"profileImage"]]
                            placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                     options:SDWebImageRefreshCached];
    }
    else{
        cell.userName.text = inbox.Sender.name;
        [cell.userAvatar sd_setImageWithURL:[NSURL URLWithString:[inbox.Sender getProperty:@"profileImage"]]
                           placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                    options:SDWebImageRefreshCached];
    }
    [cell.userAvatar.layer setCornerRadius:cell.userAvatar.frame.size.height/2];
    cell.userAvatar.clipsToBounds=YES;
    cell.userAvatar.layer.borderWidth = 1.0;
    cell.userAvatar.layer.borderColor = [UIColor colorWithRed:251.0f/255.0f
                                                                                         green:102.0f/255.0f
                                                                                          blue:68.0f/255.0f
                                                                                         alpha:1.0f].CGColor;
    cell.userMessage.text = inbox.messageBody ;
    
    cell.chatBtn.tag = indexPath.row ;
    [cell.chatBtn addTarget:self
                     action:@selector(gotoChatScreen:)
       forControlEvents:UIControlEventTouchUpInside];
    
    NSLog(@"message is %@",inbox.messageBody);
    
    
    
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
}
-(void)gotoChatScreen :(UIButton *)btn {
    Inbox *inbox = self.uniqueMsgs[btn.tag] ;
    self.selectedInbox = inbox ;
    [self performSegueWithIdentifier:@"ToChat" sender:nil];
}
-(void)fetchingUsersInboxAsync {
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    QueryOptions *queryOption = [QueryOptions new];
    queryOption.sortBy = @[@"created DESC"];
    
    query.whereClause = [NSString stringWithFormat:@"(Reciever.objectId = '%@' OR Sender.objectId = '%@')", backendless.userService.currentUser.objectId, backendless.userService.currentUser.objectId];
    
    query.queryOptions = queryOption;
    [backendless.persistenceService find:[Inbox class] dataQuery:query response:^(BackendlessCollection *collection1)  {
        NSLog(@"Inbox Found %i", [[collection1 getTotalObjects] intValue]);
        
        
        for (Inbox *inbox in collection1.data) {
            
            
            NSPredicate *predicateReceiver = [NSPredicate predicateWithFormat:@"Reciever.objectId==%@",inbox.Reciever.objectId];
            NSArray *resultsReceiver = [self.uniqueMsgs filteredArrayUsingPredicate:predicateReceiver];
            
            if (!resultsReceiver.count) {
                
                NSPredicate *predicateSenderReciever = [NSPredicate predicateWithFormat:@"Sender.objectId==%@ AND Reciever.objectId==%@",inbox.Reciever.objectId,inbox.Sender.objectId];
                NSArray *resultsSenderReciever = [self.uniqueMsgs filteredArrayUsingPredicate:predicateSenderReciever];
                if (!resultsSenderReciever.count) {
                    [self.uniqueMsgs addObject:inbox];
                    [self.allUniqueMsgs addObject:inbox];
                }
            }
        }

        NSLog(@"Unique Senders Messages are %lu", (unsigned long)self.uniqueMsgs.count);
        
        [self.inboxTableView reloadData];
        
    } error:^(Fault *error) {
        NSLog(@"Inbox Found %@",error.description);
    }];

}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    if ([segue.identifier isEqualToString:@"ToChat"]) {
        ChatViewController *vc = segue.destinationViewController ;
        vc.selectedInbox = self.selectedInbox ;
        if ([self.selectedInbox.Reciever.objectId isEqualToString:backendless.userService.currentUser.objectId]) {
            vc.otherUser = self.selectedInbox.Sender ;
        }
        else{
            vc.otherUser = self.selectedInbox.Reciever ;

        }
    }
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

- (IBAction)unwindToInbox:(UIStoryboardSegue*)sender
{
    
}
- (IBAction)backAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"backToProfile" sender:self];
}
@end
