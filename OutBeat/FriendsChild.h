//
//  FriendsChild.h
//  outbeat
//
//  Created by iBuildx_Mac_Mini on 10/5/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "Friends.h"
#import <Backendless.h>

@interface FriendsChild : Friends
@property (nonatomic, strong) Friends *friendObj;
@property (nonatomic, strong) BackendlessUser *friendUser;

@end
