//
//  Stars.h
//  OutBeat
//
//  Created by ibuildx on 9/6/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Backendless.h"
@interface Stars : NSObject

@property (nonatomic, strong) NSString *stars;
@property (nonatomic, strong) BackendlessUser *User;

@end
