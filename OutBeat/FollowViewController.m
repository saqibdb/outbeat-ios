//
//  FollowViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 6/7/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "FollowViewController.h"
#import "FollowTableViewCell.h"
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "FriendsCell.h"
#import "UIImageView+WebCache.h"
#import "Follower.h"
#import "Utility.h"
@interface FollowViewController ()
{
    NSMutableArray *allUsers;
    NSMutableArray *followerIds;
    
}

@end

@implementation FollowViewController

- (void)viewDidLoad {
    [self.view layoutIfNeeded];
    [super viewDidLoad];
    self.uitableview.delegate = self ;
    self.uitableview.dataSource = self ;
    
}
- (void)viewDidAppear:(BOOL)animated {
    [self fetchUsers];
}

-(void)fetchUsers{
    [SVProgressHUD showWithStatus:@"Getting Followers"];
    allUsers =[[NSMutableArray alloc]init];
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    followerIds =[[NSMutableArray alloc]initWithArray:[backendless.userService.currentUser getProperty:@"following"]] ;
    
    NSString *followerIdStr ;
    
    
    
    
    
    
    
    
    
    if (followerIds.count) {
        for (int i = 0 ; i < followerIds.count; i++) {
            
            Follower *follower = followerIds[i] ;
            if (i == 0) {
                followerIdStr = [NSString stringWithFormat:@"'%@'" , follower.FollowerUserId] ;
                
            }
            else {
                followerIdStr = [NSString stringWithFormat:@"%@,'%@'",followerIdStr , follower.FollowerUserId] ;
                
            }
            
        }
    }
    else{
        [SVProgressHUD dismiss];
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Not Following" andText:@"You are currently Not Following Anyone." andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
        return ;
    }
    
    BackendlessDataQuery *query = [BackendlessDataQuery query];
    
    query.whereClause = [NSString stringWithFormat:@"objectId IN (%@)",followerIdStr];
    
    
    
    [dataStore find:query response:^(BackendlessCollection *collection) {

        for(BackendlessUser *user in collection.data){
            if (![user.objectId isEqualToString:backendless.userService.currentUser.objectId]){
                [allUsers addObject:user];
            }
        }
        [SVProgressHUD dismiss];
        [self.uitableview reloadData] ;
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:error.detail andCancelButton:NO forAlertType:AlertFailure];
        [alert show];
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return allUsers.count;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"followCell";
    FollowTableViewCell *cell  = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    if (cell == nil) {
        cell = [[FollowTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                          reuseIdentifier:CellIdentifier];
    }
    
    BackendlessUser *user =[allUsers objectAtIndex:indexPath.row];
    cell.userName.text=user.name;
    
    
    [cell.profileImage sd_setImageWithURL:[NSURL URLWithString:[user getProperty:@"profileImage"]]
                         placeholderImage:[UIImage imageNamed:@"avatar-placeholder.png"]
                                  options:SDWebImageRefreshCached];
    [cell.profileImage.layer setCornerRadius:cell.profileImage.frame.size.height/2];
    cell.profileImage.clipsToBounds=YES;
    cell.followAction.tag=indexPath.row;
    
    
    
    
    
    [cell.followAction setTitle:@"UnFollow" forState:UIControlStateNormal] ;

   
    
    [cell.followAction addTarget:self action:@selector(unFollowClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
    
}

-(void)unFollowClicked:(UIButton*)sender
{
    [SVProgressHUD showWithStatus:@"UnFollowing..."];
    BackendlessUser *selectedUser = allUsers[sender.tag] ;

    
    NSMutableArray *allFollowings =[[NSMutableArray alloc]initWithArray:[backendless.userService.currentUser getProperty:@"following"]] ;
    
    
    for (Follower *follower in allFollowings) {
        if ([follower.FollowerUserId isEqualToString:selectedUser.objectId]) {

            [allFollowings removeObject:follower] ;
            
            
            [backendless.userService.currentUser setProperty:@"following" object:allFollowings] ;
             
             
            
            
            
            id<IDataStore> dataStore = [backendless.persistenceService of:[Follower class]];
            [dataStore remove:follower response:^(NSNumber *number) {
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:sender.tag inSection:0];
                
                [allUsers removeObject:selectedUser] ;

                
                [self.uitableview deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationLeft];
                [SVProgressHUD dismiss];

                
            } error:^(Fault *error) {
                [SVProgressHUD dismiss];
                NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
                NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
                
                AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
                
                [alert show];
            }];

        }
    }


}
-(void)followClicked:(UIButton*)sender
{
    [SVProgressHUD showWithStatus:@"Saving Follower..."];
    BackendlessUser *selectedUser = allUsers[sender.tag] ;
    Follower *newFollowing = [[Follower alloc] init];
    newFollowing.FollowerUserId = [selectedUser getProperty:@"objectId"] ;
    newFollowing.SendingUserId = [backendless.userService.currentUser getProperty:@"objectId"] ;
    
    BackendlessUser *currentUser = backendless.userService.currentUser ;
    NSMutableArray *newFollowingList = [[NSMutableArray alloc] initWithArray:[currentUser getProperty:@"following"]] ;
    [newFollowingList addObject:newFollowing] ;
    [currentUser setProperty:@"following" object:newFollowingList] ;
    
    id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
    [dataStore save:currentUser response:^(id savedUser) {
        [SVProgressHUD dismiss];
        [sender setTitle:@"Following" forState:UIControlStateNormal];
        [sender setBackgroundColor:[UIColor grayColor]] ;
        sender.userInteractionEnabled = NO ;
        backendless.userService.currentUser = savedUser ;
        NSString *friendRequestMessage = [NSString stringWithFormat:@"%@ is now Following You.", backendless.userService.currentUser.name];
        [Utility publishMessageAsPushNotificationAsync:friendRequestMessage forDevice:[selectedUser getProperty:@"deviceId"]] ;
        Follower *newFollower ;
        NSMutableArray *followingList = [[NSMutableArray alloc] initWithArray:[savedUser getProperty:@"following"]] ;
        for (Follower *follower in followingList) {
            if ([follower.FollowerUserId isEqualToString:newFollowing.FollowerUserId]) {
                newFollower = follower ;
            }
        }
        if (newFollower) {
            NSMutableArray *newFollowersList = [[NSMutableArray alloc] initWithArray:[selectedUser getProperty:@"followers"]] ;
            [newFollowersList addObject:newFollower] ;
            
            [selectedUser setProperty:@"followers" object:newFollowersList] ;
            
            [dataStore save:selectedUser response:^(id savedSelectedUser) {
                NSLog(@"Follower Saved");
            } error:^(Fault *error) {
                NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
            }];
        }
        else{
            NSLog(@"issue at object nil") ;
        }
        
        
    } error:^(Fault *error) {
        [SVProgressHUD dismiss];
        NSString *errorMsg = [NSString stringWithFormat:@"something went wrong. Details = %@", error.description];
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:errorMsg andCancelButton:NO forAlertType:AlertFailure];
        
        [alert show];
    }] ;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
}

- (IBAction)backAction:(id)sender {
    
    [self performSegueWithIdentifier:@"toProfileFromFollow" sender:self];
}
@end
