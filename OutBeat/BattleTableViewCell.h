//
//  BattleTableViewCell.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 6/3/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWTableViewCell.h"
@interface BattleTableViewCell : SWTableViewCell

@property (weak, nonatomic) IBOutlet UIView *videoView;

@property (weak, nonatomic) IBOutlet UIImageView *videoThumbImage;


@property (weak, nonatomic) IBOutlet UIImageView *challengeIcon;

@property (weak, nonatomic) IBOutlet UIImageView *battleOrChallengImage;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImage;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
- (IBAction)sportAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoTitle;
@property (weak, nonatomic) IBOutlet UIButton *playVideoButtion;
- (IBAction)playVideoAction:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *battleBtn;

@end
