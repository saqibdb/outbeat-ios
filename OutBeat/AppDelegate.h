//
//  AppDelegate.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/6/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong) NSMutableArray *usersArray;

@end

