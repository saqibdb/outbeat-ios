//
//  AppDelegate.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/6/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "AppDelegate.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "Backendless.h"
#import "OutBeat-swift.h"
#import "TSMessageView.h"
#import "IQKeyboardManager.h"
#import "PinterestSDK.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    [Fabric with:@[[Crashlytics class]]];

    
     [backendless initApp:@"3A30E854-99EA-BF2D-FF42-DBEF6C6BB600" secret:@"2BA23D56-150A-BCA4-FF8D-A854796B9700" version:@"v1"];
    // Override point for customization after application launch.
    [backendless.messaging registerForRemoteNotifications];

    
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    [PDKClient configureSharedInstanceWithAppId:@"4870016470679042317"];
    
    [[UITabBar appearance] setBackgroundColor:[UIColor whiteColor]];
    
    [NSThread sleepForTimeInterval:2.0];
    [IQKeyboardManager sharedManager].enable = YES ;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self fetchingUsersAsync];
    });

    return YES;
}


-(void)fetchingUsersAsync {
    
    self.usersArray=[[NSMutableArray alloc] init];
    [[backendless.persistenceService of:[BackendlessUser class]] find:nil response:^(BackendlessCollection *userColection) {
        NSLog(@" Users Found = %lu",(unsigned long)userColection.data.count);
        
        NSMutableArray *unsortedArray =[[NSMutableArray alloc] initWithArray:userColection.data];
        
        NSSortDescriptor *sortDescriptor;
        sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"starsThisMonth"
                                                     ascending:NO];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedArray = [unsortedArray sortedArrayUsingDescriptors:sortDescriptors];
        _usersArray=[sortedArray mutableCopy];
        
        NSDate * currentDate = [NSDate date];

        for (BackendlessUser *user in _usersArray)
        {
            NSString *lastUpdatedMonth = [user getProperty:@"lastUpdatedMonth"];
            NSArray *lastUpdatedMonthArray = [lastUpdatedMonth componentsSeparatedByString:@"-"];
            int lastYear = [[lastUpdatedMonthArray objectAtIndex:0] intValue];
            int lastMonth = [[lastUpdatedMonthArray objectAtIndex:1] intValue];
            
            NSString *todayDate = [self stringFromDate:currentDate];
            NSArray *todayDateArray = [todayDate componentsSeparatedByString:@"-"];
            int currentYear = [[todayDateArray objectAtIndex:0] intValue];
            int currentMonth = [[todayDateArray objectAtIndex:1] intValue];
            
            if (currentMonth > lastMonth && currentYear == lastYear)
            {
                [user setProperty:@"lastUpdatedMonth" object:todayDate];
                [user setProperty:@"starsThisMonth" object:@"0"];
                
                id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
                [dataStore save:user responder:nil];
            }
            else if (currentMonth < lastMonth && currentYear > lastYear)
            {
                [user setProperty:@"lastUpdatedMonth" object:todayDate];
                [user setProperty:@"starsThisMonth" object:@"0"];
                
                id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
                [dataStore save:user responder:nil];
                
            }
            else
            {
                NSLog(@"No need to updated Last month");
            }
        }
    
    } error:^(Fault *error) {
        
    }];
}

-(NSString *)stringFromDate:(NSDate *)currentDate
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"GMT"]];
    [formatter setDateFormat:@"yyyy-MM"];
    NSString *todayDate = [formatter stringFromDate:currentDate];
    return todayDate;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    // Add any custom logic here.
    return handled;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken {
    
    NSString *deviceTokenStr = [backendless.messagingService deviceTokenAsString:deviceToken];
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        @try {
            NSString *deviceRegistrationId = [backendless.messagingService registerDeviceToken:(NSData *)deviceTokenStr ];
            NSLog(@"deviceToken = %@, deviceRegistrationId = %@", deviceTokenStr, deviceRegistrationId);
        }
        @catch (Fault *fault) {
            NSLog(@"deviceToken = %@, FAULT = %@ <%@>", deviceTokenStr, fault.message, fault.detail);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            
        });
    });

    
    
}

- (void)application:(UIApplication *)app didFailToRegisterForRemoteNotificationsWithError:(NSError *)err {
    // handle error
}
- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if ( application.applicationState == UIApplicationStateInactive || application.applicationState == UIApplicationStateBackground  )
    {
        NSLog(@"Push Recieved In Background");

        //opened from a push notification when the app was on background
        
        //TODO Open the chat view on message recieved
    }
    else{
        NSLog(@"Push Recieved");
        
       
        NSString *alertMessage = [[userInfo objectForKey:@"aps"] objectForKey:@"alert"];

        
        [TSMessage showNotificationInViewController:[self topViewController]
                                              title:@"Message Recieved"
                                           subtitle:alertMessage
                                              image:nil
                                               type:TSMessageNotificationTypeMessage
                                           duration:TSMessageNotificationDurationAutomatic
                                           callback:nil
                                        buttonTitle:@"OK"
                                     buttonCallback:^{
                                         NSLog(@"User tapped the button");
                                     }
                                         atPosition:TSMessageNotificationPositionTop
                               canBeDismissedByUser:YES];
        
        


    }
    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;

    
}

- (UIViewController *)topViewController{
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if (rootViewController.presentedViewController == nil) {
        return rootViewController;
    }
    
    if ([rootViewController.presentedViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController.presentedViewController;
        UIViewController *lastViewController = [[navigationController viewControllers] lastObject];
        return [self topViewController:lastViewController];
    }
    
    UIViewController *presentedViewController = (UIViewController *)rootViewController.presentedViewController;
    return [self topViewController:presentedViewController];
}

@end
