//
//  AccountLoschenViewController.h
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/11/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountLoschenViewController : UIViewController
- (IBAction)backAction:(UIButton *)sender;
- (IBAction)deleteAccountAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnAccountLoschen;
@property (weak, nonatomic) IBOutlet UITextView *viewText;

@end
