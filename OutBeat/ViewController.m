//
//  ViewController.m
//  OutBeat
//
//  Created by Muhammad Saqib Yaqeen on 5/6/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"

#import "Utility.h"
#import <UIKit/UIKit.h>
#import "OutBeat-swift.h"


@interface ViewController (){
    //LiquidLoader *liquidLoader ;
    UIView *grayView ;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    _lblSignInWith.text= [NSString stringWithFormat:NSLocalizedString(@"Sign In with", nil)];
    _lblOr.text=[NSString stringWithFormat:NSLocalizedString(@"or", nil)];
    //_lblPlayTheGame.text=[NSString stringWithFormat:NSLocalizedString(@"P L A Y   T H E   G A M E", nil)];
    //_btnSignIn.titleLabel.text=[NSString stringWithFormat:NSLocalizedString(@"Sign in", nil)];
    _lblAlreadHaveAccount.text=[NSString stringWithFormat:NSLocalizedString(@"already have an account?", nil)];
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)viewDidAppear:(BOOL)animated{
    /*if (!liquidLoader) {
     liquidLoader = [Utility createAndAdjustLiquidLoader:self.view];
     grayView = [Utility createGrayView] ;
     }
     [Utility hideLoading:grayView andLineLoader:liquidLoader];*/
    if (backendless.userService.currentUser) {
        [backendless.userService isValidUserToken:
         ^(NSNumber *result) {
             NSLog(@"isValidUserToken (ASYNC): %@", [result boolValue]?@"YES":@"NO");
             [self performSegueWithIdentifier:@"getStartedToTabBar" sender:nil];
         }
                                            error:^(Fault *fault) {
                                                NSLog(@"login FAULT (ASYNC): %@", fault);
                                                if ([fault.faultCode isEqualToString:@"3064"] ) {
                                                    BackendlessUser *user = backendless.userService.currentUser ;
                                                    user.password = [user getProperty:@"copyPassword"] ;
                                                    
                                                    [self loginWithUser:user] ;
                                                }
                                                else {
                                                    [self showActioSheetWithMessage:@"Please Login Again..."];
                                                }
                                            }];
        
        
    }
    
    NSLog(@"______%@_______" , backendless.userService.currentUser.name) ;
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)signUpWithFbAction:(id)sender {
    
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    [login logOut];
    
    [login
     logInWithReadPermissions: @[@"user_about_me",
                                 @"user_birthday",
                                 @"email",
                                 @"user_photos",
                                 @"public_profile"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         //[Utility showLoading:self.view andGreyBackground:grayView andLineLoader:liquidLoader];
         
         [SVProgressHUD show] ;
         
         
         
         if (error) {
             NSLog(@"Process error");
             
             [self showActioSheetWithMessage:error.description];
             
             
         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             NSString *messageError= [NSString stringWithFormat:NSLocalizedString(@"Cancelled By User", nil)];
             
             
             [self showActioSheetWithMessage:messageError];
             
         } else {
             NSLog(@"Logged in");
             if ([result.grantedPermissions containsObject:@"email"]) {
                 NSLog(@"Granted all permission");
                 
                 if ([FBSDKAccessToken currentAccessToken]) {
                     
                     [[[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:@{@"fields": @"picture, email, name, gender, birthday"}] startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                         if (!error) {
                             NSLog(@"Private Date = %@",result);
                             
                             [self compileAndRegisterUser:result];
                         }
                         else{
                             [self showActioSheetWithMessage:error.description];
                         }
                     }];
                 }
             } else {
                 NSLog(@"Not granted");
                 
                 NSString *messageError= [NSString stringWithFormat:NSLocalizedString(@"Access Not Granted", nil)];
                 
                 [self showActioSheetWithMessage:messageError];
             }
         }
     }];
    
    
}



-(void)showActioSheetWithMessage :(NSString *)msg {
    //[Utility hideLoading:grayView andLineLoader:liquidLoader];
    
    [SVProgressHUD dismiss] ;
    
    
    
    AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:[NSString stringWithFormat:NSLocalizedString(@"Warning", nil)] andText:msg andCancelButton:NO forAlertType:AlertFailure];
    
    [alert show];
    
    
    
}



-(void)compileAndRegisterUser :(id) result {
    
    
    BackendlessUser *user = [BackendlessUser new];
    [user setProperty:@"password" object:@"1223"];
    
    NSString *userEmail = [result valueForKey:@"email"];
    if (!userEmail) {
        userEmail = [NSString stringWithFormat:@"%@@facebook.com",[result valueForKey:@"id"]];
    }
    
    [user setProperty:@"name" object:[result valueForKey:@"name"]];
    [user setProperty:@"email" object:userEmail];
    
    
    
    NSString *userProfilePicStr = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [result valueForKey:@"id"]];
    
    
    [user setProperty:@"profileImage" object:userProfilePicStr];
    
    
    
    
    [backendless.userService registering:user response:^(BackendlessUser *newRegisteredUser) {
        NSLog(@"user = %@", newRegisteredUser.name);
        [newRegisteredUser setProperty:@"password" object:@"1223"];
        
        [self loginWithUser:newRegisteredUser];
        
        
        
    } error:^(Fault *error) {
        NSLog(@"FAULT = %@ <%@>", error.message, error.detail);
        if ([error.message containsString:@"User already exists"]) {
            [self loginWithUser:user];
        }
        else{
            //[Utility hideLoading:grayView andLineLoader:liquidLoader];
            [SVProgressHUD dismiss] ;
            
            AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error Registering." andText:error.description andCancelButton:NO forAlertType:AlertFailure];
            [alert show];
        }
    }];
    
}

-(void)loginWithUser :(BackendlessUser *)loginUser
{
    [backendless.userService login:loginUser.email password:loginUser.password response:^(BackendlessUser *userLogged) {
        UIDevice *device = [UIDevice currentDevice];
        //NSString  *currentDeviceId = [[device identifierForVendor]UUIDString];
        [backendless.userService setStayLoggedIn:YES] ;
        
        
        [backendless.messagingService getRegistrationAsync:^(DeviceRegistration *deviceRegistration) {
            NSLog(@"The new device Id for push is %@ and token %@",deviceRegistration.deviceId , deviceRegistration.deviceToken);
            [userLogged setProperty:@"deviceId" object:deviceRegistration.deviceId];
            
            id<IDataStore> dataStore = [backendless.persistenceService of:[BackendlessUser class]];
            [dataStore save:userLogged response:^(id result) {
                NSLog(@"New Device Id Saved %@",deviceRegistration.deviceId);
                
            } error:^(Fault *error) {
                NSLog(@"Error at deviceId %@",error.description);
            }];
            
            
            
        } error:^(Fault *error) {
            NSLog(@"Error at get Device Registration %@",error.description);
            
        }];
        
        
        
        
        
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        [self performSegueWithIdentifier:@"getStartedToTabBar" sender:nil];
    } error:^(Fault *error) {
        
        //[Utility hideLoading:grayView andLineLoader:liquidLoader];
        [SVProgressHUD dismiss] ;
        
        
        AMSmoothAlertView *alert = [[AMSmoothAlertView alloc] initDropAlertWithTitle:@"Error!" andText:[NSString stringWithFormat:@"%@",error.detail] andCancelButton:NO forAlertType:AlertInfo];
        [alert show];
    }];
}

- (IBAction)getStartedAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"getStartedToNewAccount" sender:self];
    
}

- (IBAction)signInAction:(UIButton *)sender {
    [self performSegueWithIdentifier:@"getStartedToLogin" sender:self];
    
}
- (IBAction)unwindToGetStarted:(UIStoryboardSegue*)sender
{
    
}


@end
