//
//  VideoPlayViewController.h
//  OutBeat
//
//  Created by iBuildx_Mac_Mini on 6/6/16.
//  Copyright © 2016 Muhammad Saqib Yaqeen. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Backendless.h"
#import "AMSmoothAlertView.h"
#import "SVProgressHUD.h"
#import "Video.h"
#import "GUIPlayerView.h"




@interface VideoPlayViewController : UIViewController


@property (weak, nonatomic) IBOutlet UILabel *videoLabel;

@property (strong,nonatomic) NSString *previousController;


@property (weak, nonatomic) IBOutlet GUIPlayerView *playerView;



- (IBAction)backAction:(id)sender;

@property BOOL comingFromClosed;


@property (strong) Video *selectedVideo;
@property (weak, nonatomic) IBOutlet UIButton *playPauseBtn;

@property (weak, nonatomic) IBOutlet UISlider *vidSlider;
- (IBAction)vidsliderValueChanged:(UISlider *)sender;
- (IBAction)playPauseAction:(UIButton *)sender;

@end
